$Id$

Eos Source Installation
=======================

Although Eos itself is written in interpreted Python, requiring no compilation
itself, it has many dependencies that will need to be installed first:

- Python 2.5
- pygame 1.7 or better (Requires SDL)
- pyode 1.2 (requires ODE 0.8 and pyrex)
- python memcached 1.34 (for multiplayer, requires memcached)

Here are links to the above for convenience. I have found that using a package
management system is the easiest way to get up and running.

Python 2.5 -- http://www.python.org/download/releases/2.5/
pygame 1.7 -- http://www.pygame.org/download.shtml
SDL -- http://www.libsdl.org/download-1.2.php
pyode -- http://pyode.sourceforge.net/
ODE 0.8 -- http://sourceforge.net/project/showfiles.php?group_id=24884
pyrex -- http://www.cosc.canterbury.ac.nz/greg.ewing/python/Pyrex/
python memcached 1.34 -- ftp://ftp.tummy.com/pub/python-memcached/
memcached -- http://www.danga.com/memcached/download.bml

Verifying installation
----------------------

After getting all of the above installed, you can confirm successful
installation from the python command line:

% python
Python 2.5 (r25:51908, Feb  9 2007, 16:23:18) 
[GCC 4.0.1 (Apple Computer, Inc. build 5250)] on darwin
Type "help", "copyright", "credits" or "license" for more information.
>>> import pygame
>>> import ode
>>> import memcache
>>> 

if all three of those imports work without complaints you are good to go!

Fixing pyode on MacOS X
-----------------------

If importing ode from python complains about not being able to load
libode.dylib, you can fix it using the install_name_tool utility from
the command line. First find out where the ode.so file is (in your python
site-packages dir). Then find out where your libode.dylib file is (or
libode.0.dylib, whatever the case may be). Then run install_name_tool to
correct the path to libode.dylib in ode.so. 

Below is an example with ODE and Python installed using the fink package
manager (note you will need to be logged in as an admin):

% sudo install_name_tool -change libode.dylib /sw/lib/libode.0.dylib /sw/lib/python2.5/site-packages/ode.so

Here is another example with ODE and Python installed using
MacPorts/DarwinPorts:

% sudo install_name_tool -change libode.dylib /opt/local/lib/libode.dylib /opt/local/lib/python2.4/site-packages/ode.so

I have reported this issue to the pyode folks, hopefully it can be corrected
in their package.

Running Eos
-----------

Once you have all of the dependencies installed, you can run Eos directly from
the source directory. First cd into the eos source dir then run:

% python eos.py

There are several command line options that may be useful for debugging. Use
the --help option to list them.
