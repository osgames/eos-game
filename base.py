## Eos, Dawn of Light -- A Space Opera
## Copyright (c) 2007 Casey Duncan and contributors
## See LICENSE.txt for licensing details

# Planetary bases
# $Id$

import ConfigParser
import random
import game
import ai
import vessel
import vector
import map
import message
import panel
import sprite

class PlanetaryBase(sprite.Sprite):
	"""Planetary bases are owned by a player and can build using the planet's
	resources. Bases have a level which determines which tings can be built.
	In general bases can build three types of things:

	- Vessels (Includes ships, turrets and stations)
	- Research (Enables improved vessel systems)
	- Base Improvement (increases base level)

	Bases are established on a planet by landing an assault ship.

	A planet with a base also provides support for the fleet. This support
	is automatically taken from the planet's resources before they can
	be used for building. A maximum 66% of a planet's resource production may 
	be used for support
	"""

	builder = None # What we are building
	# base level required to build certain vessels
	vessel_level = {
		'fighter': 1,
		'assault transport': 1,
		'warship': 2,
	}
	_owner2number = {} # Map of owner => last base number
	max_build_queue = 6 # Maximum number of queued builders

	def __init__(self, planet, owner, level=1, max_level=5):
		sprite.Sprite.__init__(self)
		if isinstance(planet, str):
			self.planet = game.map.get_planet(planet_name)
			if self.planet is None:
				raise map.MapConfigError('Cannot create base, planet "%s" not found in map')
		else:
			self.planet = planet
		if self.planet.base is not None:
			self.planet.base.kill()
		self.planet.base = self
		self.owner = owner
		owner.bases.add(self)
		self.rally_point = self.planet
		if max_level >= level:
			self.level = level
			self.max_level = max_level
		else:
			raise map.MapConfigError('Expected base level (%s) <= max_level (%s)' 
				% (level, max_level))
		self.number = self._owner2number[owner] = self._owner2number.get(owner, 0) + 1
		self.build_queue = []
		if owner is game.local_player:
			panel.BasePanel(planet)
	
	def update(self):
		if self.builder is None and self.build_queue:
			self.builder = self.build_queue.pop(0)
		if self.builder is not None:
			self.builder.work(self.planet.resources / game.fps)
			if self.builder.is_finished():
				self.builder = None
	
	def can_build_vessel(self, config_file):
		"""Return True if the base can build the vessel from the config file,
		False if not
		"""
		path, config = vessel.get_config(config_file)
		vtype = config.get('general', 'vessel_type')
		return (vtype in self.vessel_level and self.vessel_level[vtype] <= self.level
			and config.has_option('general', 'cost'))
	
	def build_vessel(self, config_file):
		"""Start building the vessel if we can build it and nothing else
		is being built. Return True if the vessel will be built.
		"""
		if len(self.build_queue) < self.max_build_queue and self.can_build_vessel(config_file):
			self.build_queue.append(VesselBuilder(self, config_file))
			return True
		return False
	
	def cancel_builder(self, builder_idx=None):
		"""Remove the builder from the build queue. If no index is given,
		cancel the current thing being built (discarding any progess). Otherwise
		remove the builder specified by builder index
		"""
		if builder_idx is None:
			self.builder = None
		elif len(self.build_queue) > builder_idx:
			del self.build_queue[builder_idx]


class Builder:
	"""Base class for building things"""

	def __init__(self, base, cost):
		"""
		>>> class TestBuilder(Builder):
		...   done_called = False
		...   def done(self):
		...      self.done_called = True
		...
		>>> tb = TestBuilder(None, 2)
		>>> tb.is_finished()
		False
		>>> tb.fraction_complete()
		0.0
		>>> tb.work(1)
		0
		>>> tb.is_finished()
		False
		>>> tb.fraction_complete()
		0.5
		>>> tb.done_called
		False
		>>> tb.work(2)
		1.0
		>>> tb.is_finished()
		True
		>>> tb.fraction_complete()
		1.0
		>>> tb.done_called
		True
		"""
		self.base = base
		self.work_needed = float(cost)
		self.work_done = 0.0
	
	def work(self, resources):
		"""Add the resources to the project return any unused resources"""
		if self.work_needed - self.work_done < resources:
			extra = resources - (self.work_needed - self.work_done)
			self.work_done = self.work_needed
		else:
			extra = 0
			self.work_done += resources
		if self.is_finished():
			self.done()
		return extra
	
	def is_finished(self):
		"""Return true if the builder has finished its work"""
		return self.work_done >= self.work_needed
	
	def fraction_complete(self):
		"""Return the fraction from 0 to 1.0 that the builder has completed"""
		return min(self.work_done, self.work_needed) / self.work_needed
	
	def done(self):
		"""Complete the thing we are building"""
		raise NotImplementedError


class VesselBuilder(Builder):
	"""Builds a vessel at a base given a config path"""
	
	def __init__(self, base, config_file):
		self.config_file = config_file
		file, self.vessel_config = vessel.get_config(config_file)
		cost = float(self.vessel_config.get('general', 'cost'))
		Builder.__init__(self, base, cost)
		self.vessel_built = False
	
	def done(self):
		"""Construct vessel"""
		if not self.vessel_built:
			vessel = ai.AIVessel.load(
				self.config_file, target=self.base.rally_point, objective=self.base.rally_point,
				category=self.base.owner.category)
			vessel.set_position(self.base.planet.position)
			vessel.set_heading(random.random() * vector.fullcircle)
			self.vessel_built = True
			self.base.owner.vessel_built(self.base, vessel)

if __name__ == '__main__':
	"""Run tests if executed directly"""
	import sys, doctest
	failed, count = doctest.testmod()
	print 'Ran', count, 'test cases with', failed, 'failures'
	sys.exit(failed)
