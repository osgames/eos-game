## Eos, Dawn of Light -- A Space Opera
## Copyright (c) 2007 Casey Duncan and contributors
## See LICENSE.txt for licensing details

# Carrier bays
# $Id$

import sys
import pygame
import game
import vessel
import ai
import body
import media
import vector
from vector import halfcircle, diagonal

class FighterBay(vessel.Weapon):

	attr_name = 'fighter_bay'

	priority = 1

	def __init__(self, vessel, fighter_config, capacity, 
		launch_delay, launch_distance, launch_force=1550,
		launch_salvo=1, launch_sound=None, dock_sound=None, regeneration_time=0):
		"""Bay that holds a specific number of fighters"""
		self.vessel = vessel
		self.fighter_config = fighter_config
		self.capacity = int(capacity)
		self.fighters = pygame.sprite.Group()
		self.docked = pygame.sprite.Group()
		self.launch_delay = float(launch_delay) * 1000
		self.launch_force = int(launch_force)
		self.launch_distance = int(launch_distance)
		self.launch_salvo = int(launch_salvo)
		self.regeneration_time = float(regeneration_time)
		self.regeneration_end = None
		self.last_launch = 0
		self.launch_sound = launch_sound
		self.dock_sound = dock_sound
		self.sensor = None
		# Load the bay with fighters
		while len(self.fighters) < self.capacity:
			self.create_fighter()

	def update_system(self):
		for vessel in self.docked.sprites():
			# Docked vessels are not drawn, but systems are still updated
			vessel.update_systems()
		if self.enabled:
			if self.firing and game.time > self.last_launch + self.launch_delay:
				self.launch()
			if self.regeneration_end is not None:
				if game.time < self.regeneration_end:
					# Constructing a fighter
					energy = self.fighter_energy * 4 / (self.regeneration_time * game.fps)
					self.vessel.use_energy(energy)
				else:
					# New fighter is finished
					self.create_fighter()
					self.regeneration_end = None
			elif self.regeneration_time and len(self.fighters) < self.capacity:
				# Start building a new fighter
				self.regeneration_end = game.time + self.regeneration_time * 1000
		else:
			self.regeneration_end = None
	
	def dock(self, vessel):
		"""Dock a fighter vessel in the bay. Only fighters launched from the bay can dock"""
		if vessel in self.fighters:
			self.docked.add(vessel)
			vessel.hide()
			if self.dock_sound is not None:
				media.play_sound(self.dock_sound, position=self.vessel.position)
	
	def create_fighter(self):
		"""Create a new fighter and add it to the bay as docked"""
		assert len(self.fighters) < self.capacity, "Cannot create fighter, bay full"
		if self.sensor is None:
			self.sensor = ai.SharedSensor(
				self.vessel, 1000, body.everything & ~self.vessel.category)
		fighter = ai.AIVessel.load(self.fighter_config,
			target=self.vessel.control.target, 
			objective=self.vessel,
			sensor=self.sensor,
			category=self.vessel.category)
		self.fighters.add(fighter)
		self.dock(fighter)
		self.fighter_energy = fighter.max_energy
	
	def launch(self):
		"""Launch fighters"""
		# Healthy fighters get launched first 
		fighters = sorted(self.docked.sprites(), key=lambda v: v.health)
		for i in range(self.launch_salvo):
			if fighters:
				fighter = fighters.pop()
				self.docked.remove(fighter)
				fighter.set_position(self.vessel.position)
				fighter.set_velocity(self.vessel.velocity)
				fighter.set_heading(self.vessel.heading)
				fighter.setup_collision(self.vessel.category, body.everything & ~body.shot)
				fighter.push(vector.unit(self.vessel.heading + halfcircle) * self.launch_force)
				fighter.show()
				fighter.update_systems()
				self.last_launch = game.time
				if self.launch_sound is not None:
					media.play_sound(self.launch_sound, position=self.vessel.position)

	@property
	def targeted(self):
		target = self.vessel.control.target.sprite
		if (not target or not target.category & body.everything & ~self.vessel.category):
			return False
		return (self.docked 
			and vector.distance(self.vessel.position, target.position) < self.launch_distance)
	
	def kill(self):
		for fighter in self.docked.sprites():
			fighter.kill()
	
	def __contains__(self, vessel):
		"""Return true if vessel is docked in the fighter bay"""
		return vessel in self.docked


class BombBay(vessel.Weapon):

	priority = 1
	mass = 5
	name = 'bomb bay'

	def __init__(self, vessel_mount, bomb_factory, launch_velocity, max_ttl, reload_time,
		energy_use, name=None):
		self.vessel = vessel_mount
		self.bomb_factory = vessel.resolve(bomb_factory)
		self.launch_velocity = float(launch_velocity)
		self.reload_time = float(reload_time) * 1000
		self.energy_use = int(energy_use)
		self.max_ttl = float(max_ttl) * 1000
		self.range = self.launch_velocity * float(max_ttl)
		self.last_launch = 0
		if name is not None:
			self.name = name
	
	def update_system(self):
		if (self.enabled and self.firing
			and game.time > self.last_launch + self.reload_time
			and self.vessel.use_energy(self.energy_use)):
			bomb = self.bomb_factory(self.vessel, self,
				vector.unit(self.vessel.heading) * self.launch_velocity,
				self.max_ttl)
			self.last_launch = game.time
	
	@property
	def targeted(self):
		target = self.vessel.control.target.sprite
		if (not target or not target.alive() or target.explosion is not None
			or not target.category & body.everything & ~self.vessel.category):
			return False
		return (abs(self.vessel.bearing(target)) < diagonal / 2
			and vector.distance(self.vessel.position, target.position) <= self.range)
