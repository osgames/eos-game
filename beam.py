## Eos, Dawn of Light -- A Space Opera
## Copyright (c) 2007 Casey Duncan and contributors
## See LICENSE.txt for licensing details

# Beam weapons
# $Id$

import ode
import game
import display
import pygame
import body
import random
import vessel
import vector
import media
import sprite

_empty_rect = pygame.rect.Rect(0, 0, 0, 0)


class BeamWeapon(vessel.Weapon, sprite.Sprite):

	mass_factor = 0.1 # Mass per unit damage
	sound_interval = 750 # time between sound plays

	def __init__(self, gunmount, range, damage, efficiency, arc_degrees=0, 
		width=1, color='255,255,255', sound=None, name=None):
		sprite.Sprite.__init__(self, sprite.layers.effects)
		self.gunmount = gunmount
		self.range = float(range)
		self.length = self.range
		self.beam_damage = float(damage)
		self.efficiency = float(efficiency)
		self.arc = float(arc_degrees) / 360.0 * vector.fullcircle
		self.mass = self.beam_damage * self.mass_factor
		self.width = int(width)
		self.color = tuple(int(c) for c in color.split(','))
		self.sound = sound
		self.last_sound_time = 0
		assert len(self.color) == 3, 'Invalid color value "%s"' % color
		self.ray = None
		if display.surface.get_bitsize() == 32:
			self.draw_line = pygame.draw.aaline
		else:
			self.draw_line = pygame.draw.line
		if name is not None:
			self.name = name
	
	def update_system(self):
		if self.ray is None:
			# Create a ray geom to determine beam collision
			self.ray = ode.GeomRay(game.collision_space, self.range)
			self.ray.setCategoryBits(body.nothing)
			self.ray.parent = self
		self.ray.setCollideBits(body.everything & ~self.gunmount.category)
		if self.arc > 0 and self.gunmount.control.target:
			# Track the selected target
			track_angle = self.gunmount.bearing(self.gunmount.control.target.sprite)
			if abs(track_angle) > self.arc:
				# limit angle to maximum arc
				track_angle /= track_angle / self.arc
		else:
			track_angle = 0
		self.ray.set(vector.to_tuple3(self.gunmount.position), 
			vector.to_tuple3(vector.unit(self.gunmount.heading + track_angle)))
		self.firing = self.firing and self.gunmount.use_energy(
			self.beam_damage / self.efficiency / game.fps)
	
	def draw(self, surface):
		if self.firing and self.enabled and self.gunmount.enabled:
			camera = game.camera
			(sx, sy, ignored), (dx, dy, ignored) = self.ray.get()
			start = vector.vector2(sx, sy)
			end = start + vector.vector2(dx, dy) * self.length
			apparent_size1, start_pos = vector.to_screen(start)
			apparent_size2, end_pos = vector.to_screen(end)
			self.targeted = False
			c = random.randint(1, 4)
			self.rect = self.draw_line(surface, 
				(self.color[0]/c, self.color[1]/c, self.color[2]/c), 
				vector.to_tuple(start_pos), vector.to_tuple(end_pos), 
				int(max(self.width * max(apparent_size1, apparent_size2), 1)))
			self.length = self.range
			if self.sound is not None and game.time - self.last_sound_time > self.sound_interval:
				media.play_sound(self.sound, position=self.gunmount.position)
				self.last_sound_time = game.time
		else:
			self.rect = _empty_rect
		return self.rect
	
	def collide(self, other, contact):
		self.length = max(contact[0].getContactGeomParams()[2], 30)
		self.targeted = True
		if self.firing and self.enabled:
			other.damage(self.beam_damage / game.fps)
		
