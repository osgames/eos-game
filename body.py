## Eos, Dawn of Light -- A Space Opera
## Copyright (c) 2007 Casey Duncan and contributors
## See LICENSE.txt for licensing details

# Basic massive objects in the game universe
# $Id$

import math
import ode
import game
import display
import pygame
from pygame.locals import *
import media
import random
import sys
import vector
import sprite

# Collision category bit values, used for collision detection.
# Each body assigned a category and optionally a collide
# bitmap to determine whether it interacts with another body
# Objects only interact when the category bitmap of one
# intersects the collide bitmap of the other.
shot = 1
nothing = 0
everything = 0x7fffffff

body_count = 0 # Counter for debugging

class RoundBody(sprite.Sprite):
	"""A basic round object with mass that can be drawn and moved in
	the universe
	"""

	mass = 0.1 # Mass in tons
	radius = 0.1 # Radius in meters, affects rotation
	collision_radius = None # Optional collision radius if different from above
	max_visible_dist = 10000 # Maximum distance pointy is shown
	scale = 1.0 # Onscreen scale adjustment
	incidental = False # Incidental bodies cannot be selected, do not show on maps, etc

	category = nothing # category for collision interaction
	collides_with = nothing # category bitmap this body collides with

	body = None # ode dynamics body
	geom = None # ode collision geom

	offscreen_img = None # image to point to offscreen sprite
	xplode_animation = media.LargeExplosion
	explosion = None

	layer = () # Sprite group(s) to add body to
	rect = None
	src_image = None

	def __init__(self, position=None, velocity=None, heading=None, 
		category=nothing, collides_with=nothing, net_id=None, image_name=None):
		"""Create the body in the given position vector which
		can be either a vector or a tuple. An optional
		initial velocity may be specified if omitted the object
		is stationary.

		category -- category bitmap this body belongs to
		collides_with = category bitmap this body collides with
		"""
		if net_id is None:
			net_id = random.randint(0, sys.maxint)
		global body_count
		body_count += 1
		sprite.Sprite.__init__(self, self.layer)
		self.body = ode.Body(game.universe)
		mass = ode.Mass()
		mass.setSphereTotal(self.mass, self.radius)
		mass.adjust(self.mass)
		self.body.setMass(mass)
		if position is not None:
			self.set_position(position)
		else:
			self.position = vector.vector2()
		if velocity is not None:
			self.set_velocity(velocity)
		else:
			self.velocity = vector.vector2()
		if heading is not None:
			self.set_heading(heading)
		if category != nothing or collides_with != nothing:
			self.setup_collision(category, collides_with)
		# on screen size and positioning
		if self.rect is None:
			self.rect = pygame.Rect((0, 0, self.radius*2, self.radius*2))
		self.enabled = True
		self.on_screen = True
		self.apparent_size = 1.0
		self.net_id = net_id
		self.image_name = image_name
		game.objects.add(self)

	def get_state(self):
		return {
			'classname' : self.__class__.__name__,
			'create_kw' : {'net_id' : self.net_id},
			'position' : self.get_position(),
			'velocity' : self.get_velocity(),
			'heading' : self.get_heading(),
		}

	def set_local_state(self, state):
		self.set_position(state['position'])
		self.set_velocity(state['velocity'])
		self.set_heading(state['heading'])
	
	def get_position(self):
		return vector.vector2(*self.body.getPosition()[:2])
	
	def set_position(self, position):
		if not isinstance(position, tuple):
			position = vector.to_tuple(position)
		self.body.setPosition((position[0], position[1], 0))
		self.position = vector.vector2(*position)
	
	def get_velocity(self):
		return vector.vector2(*self.body.getLinearVel()[:2])
	
	def set_velocity(self, velocity):
		if not isinstance(velocity, tuple):
			velocity = vector.to_tuple(velocity)
		self.body.setLinearVel((velocity[0], velocity[1], 0))
		self.velocity = vector.vector2(*velocity)

	def setup_collision(self, category, collides_with, radius=None):
		"""Configure the collision geom to determine what objects we collide
		with (collides_with) and what objects collide with us (category)
		"""
		if self.geom is None:
			self.geom = ode.GeomSphere(
				game.collision_space, radius or self.collision_radius or self.radius)
			self.geom.setBody(self.body) # Move with the ode body
			self.geom.parent = self # attach ourself for callback purposes
		self.geom.setCategoryBits(category)
		self.geom.setCollideBits(collides_with)
		self.category = category
		self.collides_with = collides_with
		if game.local_player and self.category & game.local_player.category:
			self.offscreen_img = 'pointy-green'
		elif self.category and not self.incidental:
			self.offscreen_img = 'pointy-red'
		else:
			self.offscreen_img = None
	
	def __del__(self):
		if self.geom is not None and game.collision_space.query(self.geom):
			game.collision_space.remove(self.geom)
	
	@property
	def force(self):
		return vector.vector2(*self.body.getForce()[:2])
	
	@property
	def torque(self):
		return self.body.getTorque()[2]

	def get_heading(self):
		mat = self.body.getRotation()
		# Derive z-axis rotation. Assumes no rotation in other axis
		assert mat[6:] == (0, 0, 1), "Object rotated off axis!"
		return math.atan2(-mat[1], mat[0])

	def set_heading(self, h):
		sinh = math.sin(h)
		cosh = math.cos(h)
		# create rotation matrix in the Z-axis
		self.body.setRotation((cosh, -sinh, 0, sinh, cosh, 0, 0, 0, 1))
		self.heading = h

	def _turn_rate_fget(self):
		ignored, ignored, turn_rate = self.body.getAngularVel()
		return turn_rate
	def _turn_rate_fset(self, turn_rate):
		self.body.setAngularVel((0, 0, turn_rate))
	turn_rate = property(fget=_turn_rate_fget, fset=_turn_rate_fset)

	def update(self):
		"""Calculate onscreen position and size"""
		# Position, velocity and heading are cached because they are frequently accessed
		self.position = vector.vector2(*self.body.getPosition()[:2])
		self.velocity = vector.vector2(*self.body.getLinearVel()[:2])
		self.heading = self.get_heading()
		# place self relative to the camera at proper zoom level
		self.apparent_size, screen_pos = vector.to_screen(self.position)
		if self.explosion is None:
			self.image = media.image(self.image_name, self.apparent_size * self.scale, self.heading)
		else:
			image = self.explosion.next_image(self.apparent_size * self.scale)
			if image is not None:
				self.image = image
			else:
				self.kill()
		self.rect = self.image.get_rect(center=vector.to_tuple(screen_pos))
		self.on_screen = self.rect.colliderect(display.rect)

	def draw(self, surface):
		"""Draw either sprite image or offscreen pointer"""
		if self.on_screen:
			surface.blit(self.image, self.rect)
			return self.rect
		elif self.offscreen_img is not None:
			camera_rect = game.camera.rect
			camera_pos = game.camera.position
			camera_x, camera_y = vector.to_tuple(camera_pos)
			distance = vector.distance(self.position, camera_pos)
			if distance < self.max_visible_dist and self.position != camera_pos:
				angle = vector.radians(self.position - camera_pos)
				image = media.image(self.offscreen_img, game.camera.base_scale, angle) 
				rect = image.get_rect(center=self.rect.center)
				x, y = vector.to_tuple(self.position)
				if self.rect.top < display.rect.top:
					rect.top = display.rect.top
					rect.left = camera_rect.centerx + (
						(x - camera_x) * (rect.centery - camera_rect.centery) / (y - camera_y))
				elif self.rect.bottom > display.rect.bottom:
					rect.bottom = display.rect.bottom
					rect.left = camera_rect.centerx + (
						(x - camera_x) * (rect.centery - camera_rect.centery) / (y - camera_y))
				if rect.left < display.rect.left:
					rect.left = display.rect.left
					if (rect.top < self.rect.top < display.rect.top 
						or rect.bottom > display.rect.bottom):
						rect.top = camera_rect.centery + (
							(y - camera_y) * (rect.centerx - camera_rect.centerx) / (x - camera_x))
				elif rect.right > display.rect.right:
					rect.right = display.rect.right
					if (rect.top < self.rect.top < display.rect.top 
						or rect.bottom > display.rect.bottom):
						rect.top = camera_rect.centery + (
							(y - camera_y) * (rect.centerx - camera_rect.centerx) / (x - camera_x))
				return surface.blit(image, rect)
		return self.rect
	
	def hide(self):
		"""Hide a body so that is is no longer considered in ode, updated or drawn"""
		if self.geom is not None:
			self.geom.disable() # don't participate in collisions anymore
		if self in self.layer:
			self.layer.remove(self)
	
	def show(self):
		"""Show a body previously hidden"""
		if self.geom is not None:
			self.geom.enable() # Participate in collisions now
		self.layer.add(self)
	
	def disable(self):
		"""Physically disable a body"""
		if self.geom is not None:
			self.geom.disable() # don't participate in collisions anymore
		self.enabled = False
	
	def explode(self, sound='explode.wav'):
		"""Flaming death"""
		if self.explosion is None:
			self.explosion = self.xplode_animation()
			media.play_sound(sound, position=self.position, min_spacing=0.33)
			self.disable()

	def kill(self):
		"""Out damned sprite"""
		global body_count
		if self.alive():
			body_count -= 1
		sprite.Sprite.kill(self)
		# Disable the body and remove the geom objects from the
		# collision space immediately so they
		# are no longer considered by ode.
		if self.geom is not None:
			# Remove circ-refs to allow proper destruction of the ode body
			self.geom.setBody(None)
			self.geom.parent = None
			if game.collision_space.query(self.geom):
				game.collision_space.remove(self.geom)
		self.body.disable()
	
	def push(self, force):
		"""Apply the force vector to the body for this frame"""
		x, y = vector.to_tuple(force)
		self.body.addForce((x, y, 0))
	
	def is_friendly(self, other):
		"""Return true if the other body is considered friendly"""
		return self.category & other.category
	
	def collide(self, other, contacts):
		"""Called when this object collides with another. The contacts
		argument contains a list of ode.Contact objects from the collision
		"""
		# Subclasses to provide specific behavior
		pass

