## Eos, Dawn of Light -- A Space Opera
## Copyright (c) 2007 Casey Duncan and contributors
## See LICENSE.txt for licensing details

# Camera
# $Id$

import pygame
import game
import display
import vector
import sprite

class Camera:

	centering = 0.98 # Lower values == more centering force
	adhesion = 6.0 # Higher values == closer camera tracking under acceleration
	virtual_width = 1600 # distance across screen at 1.0 zoom regardless of resolution
	zoom_factor = 1.25 # Zoom change for each zoom in/out
	zoom_speed = 1.0

	def __init__(self, target):
		self.target = target
		self.center()
		self.offset = vector.vector2()
		self.last_velocity = target.velocity
		self.zoom = self.dest_zoom = self.base_scale
		self.zoom_size_factor = 300.0 / self.zoom # For perspective
	
	def center(self):
		"""Setup camera center"""
		self.rect = pygame.Rect(display.rect.width / 2, display.rect.height / 2, 0, 0)
		self.screen_pos = vector.vector2(*self.rect.center)
		self.base_scale = (float(display.rect.width) / float(self.virtual_width))**0.5
	
	@property
	def position(self):
		# Use a property to track target position precisely, avoiding lag and jitters
		return self.target.position + self.offset
	
	def follow(self, target):
		self.last_velocity = target.velocity
		self.offset = self.target.position - target.position
		self.target = target
	
	def update(self):
		if not self.target.alive():
			self.acquire_target()
		self.offset *= self.centering
		accel = self.target.velocity - self.last_velocity
		self.offset += -vector.normal(accel) * (vector.length(accel) / self.adhesion)**2
		self.last_velocity = self.target.velocity
		if self.zoom < self.dest_zoom:
			self.zoom = min(self.zoom * (1.0 + self.zoom_speed / game.fps), self.dest_zoom)
			self.zoom_size_factor = 300.0 / self.zoom # For perspective
		elif self.zoom > self.dest_zoom:
			self.zoom = max(self.zoom * (1.0 - self.zoom_speed / game.fps), self.dest_zoom)
			self.zoom_size_factor = 300.0 / self.zoom # For perspective
	
	def acquire_target(self):
		# Select a new target
		for ship in sprite.layers.vessels:
			if ship.alive() and not ship.incidental:
				self.follow(ship)
				break
	
	def zoom_in(self):
		self.dest_zoom = min(self.dest_zoom * self.zoom_factor, self.base_scale * self.zoom_factor)
	
	def zoom_out(self):
		self.dest_zoom = max(self.dest_zoom / self.zoom_factor, self.base_scale / 1.5)

