## Eos, Dawn of Light -- A Space Opera
## Copyright (c) 2007 Casey Duncan and contributors
## See LICENSE.txt for licensing details

# Pygame display state
# $Id$

import pygame
from pygame.locals import *
import media

surface = None # Main display surface
rect = None # Display rect, same as display.surface.get_rect()

resolution = None # Current display resolution setting 'low', 'med', 'high' or 'max'
pixeldepth = None # Desired pixel depth for fullscreen mode
fullscreen = None # Whether or not we're in fullscreen mode

# Screen sizes cooresponding to resolution settings
# name => fullscreen res, windowed res
screen_sizes = {
	'max': ((0, 100000), (1280, 1024)),
	'high': ((650, 800), (1024, 768)),
	'med': ((500, 649), (800, 600)),
	'low': ((400, 499), (640, 480))
}

def setup(new_resolution=None, new_fullscreen=None, new_pixeldepth=None):
	"""Setup the display, omitted arguments are unchanged from the last call to setup"""
	global resolution, fullscreen, pixeldepth, surface, rect
	if new_resolution is not None:
		resolution = new_resolution
	if new_fullscreen is not None:
		fullscreen = new_fullscreen
	if new_pixeldepth is not None:
		pixeldepth = new_pixeldepth

	assert resolution in screen_sizes, 'Unsuppported display resolution setting: %s' % resolution
	(min_height, max_height), window_size = screen_sizes[resolution]
	if fullscreen:
		modes = pygame.display.list_modes()
		# Choose the first mode with a height in the desired range
		for mode in modes:
			if min_height <= mode[1] <= max_height and mode[0] > mode[1]:
				break
		else:
			mode = modes[0]
			print "Could not find ideal display mode, using", mode
		surface = pygame.display.set_mode(mode, FULLSCREEN | HWSURFACE | DOUBLEBUF, pixeldepth)
		pygame.mouse.set_visible(False)
	else:
		surface = pygame.display.set_mode(window_size)
		pygame.display.set_icon(media.load_image('eos-icon.png'))

	rect = surface.get_rect()
	pygame.display.set_caption('EOS [ctrl+f full screen]', 'EOS')

def toggle_fullscreen():
	"""Toggle between fullscreen and windowed mode. Return True if in fullscreen"""
	global fullscreen
	setup(new_fullscreen=not fullscreen)
	return fullscreen

