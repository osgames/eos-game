## Eos, Dawn of Light -- A Space Opera
## Copyright (c) 2007 Casey Duncan and contributors
## See LICENSE.txt for licensing details

# Main program script
# $Id$

import game
import optparse
import pygame

try:
	import psyco
except ImportError:
	pass
else:
	psyco.full()

def run():
	parser = optparse.OptionParser()
	parser.add_option('-s', '--server',
		dest='is_server', default=False, action='store_true',
		help='launch a multiplayer EOS server')
	parser.add_option('-j', '--join',
		dest='is_client', default=False, action='store_true',
		help='join an EOS server')
	parser.add_option('-i', '--host',
		dest='host', default='127.0.0.1',
		help='server hostname [default %default]')
	parser.add_option('-p', '--port',
		dest='port', type='int', default=252525,
		help='server port [default %default]')
	parser.add_option('-r', '--race',
		dest='race', default=None,
		help='player race')
	parser.add_option('-z', '--ship',
		dest='ship', default=None,
		help='player ship')
	parser.add_option('-w', '--window', 
		dest='fullscreen', default=True, action='store_false',
		help='run in a window')
	parser.add_option('-o', '--profile', 
		dest='do_profile', default=False, action='store_true',
		help='enable profiling')
	parser.add_option('-d', '--debug',
		dest='debug', default=False, action='store_true',
		help='Drop into the debugger on error')
	parser.add_option('--stats',
		dest='stats', default=False, action='store_true',
		help='Output object stats during game')
	parser.add_option('--fake-latency',
		dest='latency', type='float', default=0.0,
		help='artificial latency [default %default secs]')
	parser.add_option('--fake-packet-loss',
		dest='packet_loss', type='float', default=0.0,
		help='artificial packet loss [default %default percent]')
	parser.add_option('-g', '--resolution',
		dest='resolution', choices=('low', 'med', 'high', 'max'),
		help='Graphics quality: low, med, high or max. Default: high')
	parser.add_option('--mute',
		dest='mute', default=False, action='store_true',
		help='Mute volumne')
	opts, args = parser.parse_args()
	if args: raise optparse.OptParseError('Unrecognized args: %s' % args)

	game.debug = opts.debug
	game.stats = opts.stats
	game.init(opts.is_server, opts.is_client, opts.host, opts.port,
		opts.fullscreen, opts.race, opts.ship, opts.resolution, 
		opts.latency, opts.packet_loss, opts.mute)
	start = pygame.time.get_ticks()
	try:
		if opts.do_profile:
			try:
				import cProfile as profile
			except ImportError:
				import profile
			import pstats
			# Monkey patch the pstats class to combine cum time and callees
			def print_func_and_callees(self, func):
				self.print_func_only(func)
				self.print_call_line(self.max_name_len, func, self.all_callees[func])
			pstats.Stats.print_func_only = pstats.Stats.print_line
			pstats.Stats.print_line = print_func_and_callees
			# Run the profiler and print the report
			profile.run('game.play()', 'profile.out')
			stats = pstats.Stats('profile.out')
			stats.calc_callees()
			stats.sort_stats('cum').print_stats(20)
		else:
			game.play()
	except SystemExit:
		raise
	except:
		if opts.debug:
			import pdb, sys
			pdb.post_mortem(sys.exc_info()[-1])
		else:
			raise
	finally:
		print 'Average fps:', game.frame_no * 1000.0/(game.time - start)

if __name__ == '__main__':
	run()
