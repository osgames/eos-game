## Eos, Dawn of Light -- A Space Opera
## Copyright (c) 2007 Casey Duncan and contributors
## See LICENSE.txt for licensing details

# Event Handling
# $Id$

import time
import pygame
from pygame.locals import *
import game
import display
import vector
import panel
import selection
import sprite
from vessel import Vessel
from staticbody import Planet


class Handler:
	"""Event handler base class"""

	def mouse_move(self, event):
		"""Handle mouse movement"""
	
	def mouse_up(self, event):
		"""Handle mouse button release"""
	
	def mouse_down(self, event):
		"""Handle mouse button press"""
	
	def mouse_click(self, event, pos):
		"""Single mouse click at pos, event contains mouse button info"""
	
	def mouse_double_click(self, event, pos):
		"""Double mouse click at pos, event contains mouse button info"""
	
	def mouse_drag(self, event, start_pos, end_pos):
		"""Mouse drag in progress from start_pos to end_pos, event contains mouse button info"""
	
	def mouse_drag_end(self, event, start_pos, end_pos):
		"""Mouse drag completed from tart_pos to end_pos, event contains mouse button info"""

	def key_down(self, event):
		"""Handle key press"""


class MainHandler(Handler):
	"""Top-level event handler"""

	mouse_timeout = 3000 # Time to hide mouse if not moved or clicked
	double_click_time = 750 # max millis between click to be considered a double
	mouse_slop = 5 # Amount mouse is allowed to move to differential between clicks and drags

	def __init__(self, handlers=()):
		"""Dispatches pygame events to the handler's methods"""
		self.event_handlers = {
			MOUSEMOTION: self.mouse_move,
			MOUSEBUTTONUP: self.mouse_up,
			MOUSEBUTTONDOWN: self.mouse_down,
			KEYDOWN: self.key_down,
			QUIT: self.quit
		}
		self.handlers = list(handlers)
		self.mouse_visible = pygame.mouse.set_visible(not display.fullscreen)
		self.mouse_hide_time = 0
		self.last_click_time = 0
		self.last_click_pos = None
		self.mouse_down_pos = None
		self.mouse_handler = None
		self.drag = False
		self.running = True
	
	def handle_events(self):
		"""Dispatch pending events to the appropriate handler methods"""
		for event in pygame.event.get():
			if event.type in self.event_handlers:
				self.event_handlers[event.type](event)
		if display.fullscreen and self.mouse_visible and game.time > self.mouse_hide_time:
			self.mouse_visible = pygame.mouse.set_visible(False)
		return self.running
	
	def show_mouse(self):
		"""Keep mouse visible until timeout"""
		if display.fullscreen and not self.mouse_visible:
			self.mouse_hide_time = game.time + self.mouse_timeout
			self.mouse_visible = pygame.mouse.set_visible(True)

	def mouse_move(self, event):
		"""Handle mouse movement"""
		self.show_mouse()
		if event.buttons and not self.drag and self.mouse_down_pos is not None:
			# See if the user has moved enough to begin a drag
			self.drag = (vector.distance(vector.vector2(*event.pos), self.mouse_down_pos)
				> self.mouse_slop)
		if self.drag and self.mouse_down_pos is not None:
			if self.mouse_handler is not None and self.mouse_handler.mouse_drag(
				event, vector.to_tuple(self.mouse_down_pos), event.pos):
				return
			elif self.mouse_handler is None:
				for handler in self.handlers:
					if handler.mouse_drag(event, vector.to_tuple(self.mouse_down_pos), event.pos):
						self.mouse_handler = handler
						return
		for handler in self.handlers:
			handler.mouse_move(event)
	
	def mouse_up(self, event):
		"""Handle mouse button release"""
		self.show_mouse()
		click_pos = self.mouse_down_pos and vector.to_tuple(self.mouse_down_pos) or event.pos
		self.mouse_down_pos = None
		if self.mouse_handler is not None:
			if self.drag and self.mouse_handler.mouse_drag_end(event, click_pos, event.pos):
				self.drag = False
				self.last_click_time = 0
				return
			if game.time - self.last_click_time < self.double_click_time:
				was_double_click = not self.drag
				self.last_click_time = 0
			else:
				was_double_click = False
				self.last_click_time = game.time
			if was_double_click and self.mouse_handler.mouse_double_click(event, click_pos):
				return
			if not self.drag and self.mouse_handler.mouse_click(event, click_pos):
				return
		for handler in self.handlers:
			if not self.drag and handler.mouse_click(event, click_pos):
				break
			if handler.mouse_up(event):
				break
	
	def mouse_down(self, event):
		"""Handle mouse button press"""
		self.show_mouse()
		self.mouse_down_pos = vector.vector2(*event.pos)
		self.drag = False
		for handler in self.handlers:
			if handler.mouse_down(event):
				self.mouse_handler = handler
				break
		self.mouse_handler = None
	
	def quit(self, event=None):
		self.running = False

	def key_down(self, event):
		"""Handle key press"""
		if event.key == K_ESCAPE:
			self.quit()
		elif event.key == K_f and event.mod in (KMOD_LCTRL, KMOD_RCTRL):
			display.toggle_fullscreen()
			game.camera.center()
			game.starfield.set_rect(display.rect)
			panel.handler.hide_all(force=True)
		else:			
			for handler in self.handlers:
				if handler.key_down(event):
					break
	
	def quit(self, event=None):
		game.exit()


class PlayingFieldHandler(Handler, sprite.Sprite):
	"""Game playing field event handler"""

	select_rect = None
	rect = pygame.Rect(0, 0, 0, 0)

	def zoom_in(self):
		"""Zoom in"""
		game.camera.zoom_in()
	
	def zoom_out(self):
		"""Zoom out"""
		game.camera.zoom_out()

	def select_nearest_friends(self):
		"""Select friends nearest to player"""
		selection.group.select_near_vessel(game.local_player.vessel, game.local_player.category)
	
	def constrain_selected_friends(self):
		"""Constrain friend selection by vessel class"""
		selection.group.constrain_by_class()
	
	def set_selected_friends_target(self):
		"""Command selected friends to the current target"""
		if game.target.selected:
			selection.group.set_target(game.target.selected.sprite)

	def target_nearest_enemy(self):
		"""Target nearest enemy"""
		game.target.select_nearest(~game.local_player.category)
	
	def target_nearest_planet(self):
		"""Target nearest planet"""
		game.target.select_next_planet()
	
	def target_nearest_friend(self):
		"""Target nearest friend"""
		game.target.select_nearest(game.local_player.category)
	
	def target_local_player(self):
		"""Target your ship"""
		game.target.select(game.local_player.vessel)
	
	def switch_local_player_vessel(self):
		"""Change ships"""
		target = game.target.selected.sprite
		if target:
			game.local_player.switch_vessel(target)
	
	def show_vessel_status(self):
		if isinstance(game.local_player.vessel, Vessel):
			game.local_player.vessel.show_status()
	
	def __init__(self):
		sprite.Sprite.__init__(self, sprite.layers.ui)
		key_map = {
			'=': self.zoom_in,
			'-': self.zoom_out,
			'f': self.select_nearest_friends,
			'\t': self.constrain_selected_friends,
			'\r': self.set_selected_friends_target,
			'e': self.target_nearest_enemy,
			'p': self.target_nearest_planet,
			't': self.target_nearest_friend,
			'y': self.target_local_player,
			'\b': self.switch_local_player_vessel,
		}
		self.key_map = dict((ord(key), method) for key, method in key_map.items())
		self.key_map[K_RSHIFT] = self.show_vessel_status

	def key_down(self, event):
		if event.key in self.key_map:
			self.key_map[event.key]()
			return True
	
	def mouse_click(self, event, pos):
		if event.button not in (1, 3):
			return False # We only handle left and right clicks
		# First check for vessel click
		click_rect = pygame.Rect(0, 0, 10, 10)
		click_rect.center = pos
		vessels = [s for s in sprite.layers.vessels if isinstance(s, Vessel)]
		vessels.reverse()
		for vessel in vessels:
			if (isinstance(vessel, Vessel) and vessel.rect.colliderect(click_rect) 
				and not vessel.incidental):
				if event.button == 1:
					# Left-click, select
					if vessel is not game.local_player.vessel and vessel.is_friendly(
						game.local_player.vessel):
						selection.group.select(vessel)
					else:
						game.target.select(vessel)
					return True
				elif event.button == 3:
					# Right-click, direct
					selection.group.set_target(vessel)
					return True
		# No vessel was there, see about planets
		planets = [s for s in sprite.layers.background if isinstance(s, Planet)]
		planets.reverse()
		for planet in planets:
			if planet.rect.colliderect(click_rect):
				if event.button == 1:
					# Left-click, select
					game.target.select(planet)
					return True
				elif event.button == 3:
					# Right-click, direct
					selection.group.set_target(planet)
					return True
		# Nothing specific was clicked
		if event.button == 1:
			# left-click in space, just clear the current selected group
			selection.group.select_none()
		elif event.button == 3:
			# right-click in space, make a target marker and direct ships there
			marker = selection.Marker(vector.to_map(vector.vector2(*event.pos)))
			selection.group.set_target(marker)
		return True
	
	def mouse_drag(self, event, start_pos, end_pos):
		self.select_rect = pygame.Rect(
			min(start_pos[0], end_pos[0]), min(start_pos[1], end_pos[1]),
			abs(start_pos[0] - end_pos[0]), abs(start_pos[1] - end_pos[1]))
		return True

	def mouse_drag_end(self, event, start_pos, end_pos):
		selection.group.select_none()
		selection.group.select_screen_rect(game.local_player.category, self.select_rect)
		self.select_rect = None

	def draw(self, surface):
		if self.select_rect is not None:
			pygame.draw.rect(surface, (100, 100, 200), self.select_rect, 1)
			return self.select_rect
		return self.rect

playing_field_handler = PlayingFieldHandler()

