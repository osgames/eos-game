## Eos, Dawn of Light -- A Space Opera
## Copyright (c) 2007 Casey Duncan and contributors
## See LICENSE.txt for licensing details

# Frontend user interface
# $Id$

import time
import pygame
from pygame.locals import *
import game
import sprite
from media import load_image

class Frontend:

	fps = 50

	def __init__(self, screen):
		bkgnd_img = load_image('nebula.jpg')
		self.screen = screen
		self.clock = pygame.time.Clock()
		screen_rect = screen.get_rect()
		self.background = pygame.Surface(screen_rect.size)
		self.background.fill((0, 0, 0))
		self.rect = bkgnd_img.get_rect(center=screen_rect.center)
		self.background.blit(bkgnd_img, self.rect)
		self.sprites = sprite.RenderedGroup()

		# Pages are initialized lazily on as needed, that save in both
		# code and runtime complexity. After pages are initialized we
		# keep them in the dict below. That way page state is retained
		# as long as needed.
		# Since the pages are all singletons, they are keyed by class
		self.pages = {} # Map PageClass => Page instance
		self.current_page = None
	
	def enter_masthead(self):
		header_img = load_image('eos-mast.png')
		tagline_img = load_image('dawn-of-light.png')
		flare_img = load_image('flare.png')
		header_rect = header_img.get_rect(centerx=self.rect.centerx)
		tagline_rect = tagline_img.get_rect(
			right=header_rect.right - 40, bottom=header_rect.bottom - 25)
		flare_rect = flare_img.get_rect(
			centerx=tagline_rect.right - 15, centery=tagline_rect.top + 10)
		self.sprites.empty()
		sprites = [
			Slide(header_img, header_rect.move(0, -header_rect.height), header_rect, 
				fx_time=0.25, delay=0.75),
			Wipe(tagline_img, tagline_rect, fx_time=1.0, delay=1.55),
			Slide(flare_img, flare_rect.move(-tagline_rect.width, 0), flare_rect,
				fx_time=1.0, delay=1.5, show_immediate=False),
		]
		self.sprites.add(sprites)
		self.animate(3.0)
		# Add completed masthead to background
		self.background.blit(self.screen, header_rect, header_rect)
	
	def enter_page(self, page_class):
		if page_class in self.pages:
			# Previously loaded page
			page = self.pages[page_class]
		else:
			# New up the page
			page = self.pages[page_class] = page_class(self)
		self.sprites.empty()
		# Reset residual highlighting
		for control in page.controls:
			control.highlighted = False
		if self.current_page is not None:
			self.sprites.add(self.current_page.exit_sprites())
			self.sprites.add(page.entrance_sprites(len(self.sprites)))
		else:
			self.sprites.add(page.entrance_sprites())
		self.animate(page.entrance_time)
		self.sprites.empty()
		self.sprites.add(page.controls)
		self.repaint()
		self.current_page = page
		# Clear motion events to avoid delayed highlighting after page animation is finished
		pygame.event.clear(MOUSEMOTION)
		# post a single MOUSEMOTION event to the current mouse position
		# so things are highlighted correctly under the mouse
		pygame.event.post(pygame.event.Event(MOUSEMOTION, 
			pos=pygame.mouse.get_pos(), 
			rel=pygame.mouse.get_pos(),
			buttons=pygame.mouse.get_pressed()))

	
	def animate(self, run_time, pump=True):
		"""Animate sprites for the given time, at least one frame is always
		drawn even if time is 0
		"""
		frames = run_time * self.fps or 1
		while frames > 0:
			self.sprites.clear(self.screen, self.background)
			self.sprites.update()
			dirty = self.sprites.draw(self.screen)
			pygame.display.update(dirty)
			self.clock.tick(self.fps)
			if pump:
				pygame.event.pump()
			frames -= 1
	
	def repaint(self):
		self.animate(0, pump=False)

	def run(self):
		self.screen.blit(self.background, (0, 0))
		pygame.display.flip()
		if self.current_page is None:
			# Show animated masthead entrance the first time through
			self.enter_masthead()
		self.enter_page(RootPage)
		self.event_loop()
	
	def control_at(self, pos):
		"""Return the control object at the postion specified or None if none"""
		for control in self.current_page.controls:
			if control.collides_with(pos):
				return control
	
	## Event Handling ##
	
	def event_loop(self):
		"""Wait for events and dispatch handlers"""
		self.finished = False
		self.clicked_control = None
		event_handlers = {
			MOUSEMOTION: self.mouse_move,
			MOUSEBUTTONUP: self.mouse_up,
			MOUSEBUTTONDOWN: self.mouse_down,
			KEYDOWN: self.key_down,
			QUIT: self.quit
		}
		while not self.finished:
			event = pygame.event.wait()
			if event.type in event_handlers:
				event_handlers[event.type](event)
	
	def mouse_move(self, event):
		"""Handle mouse movement"""
		pos_before = (event.pos[0] - event.rel[0], event.pos[1] - event.rel[1])
		control_before = self.control_at(pos_before)
		control_now = self.control_at(event.pos)
		if control_now != control_before:
			if control_before is not None:
				control_before.mouse_leave(self, event)
			if control_now is not None:
				control_now.mouse_enter(self, event)
	
	def mouse_up(self, event):
		"""Handle mouse button release"""
		if (self.clicked_control is not None 
			and self.clicked_control is self.control_at(event.pos)):
			self.clicked_control.mouse_up(self, event)
			self.clicked_control = None
	
	def mouse_down(self, event):
		"""Handle mouse button press"""
		control = self.control_at(event.pos)
		self.clicked_control = control
		if control is not None:
			control.mouse_down(self, event)

	def key_down(self, event):
		"""Handle key press"""
		if event.key == K_ESCAPE:
			self.quit()
	
	def quit(self, event=None):
		self.finished = True


class Page:
	"""Abstract Frontend page containing controls and handling control
	actions and holding page state
	"""

	controls = () # Sequence of control objects in the page
	entrance_time = 1.0 # Time to animate page entrance

	def __init__(self, frontend):
		self.frontend = frontend
		self.rect = frontend.screen.get_rect()
	
	def control_entrance_sprite(self, control, index):
		"""Return a sprite to animate a specific control's entrance
		on the page
		"""
		return Slide(control.render(), 
			control.rect.move(-self.rect.width, 0), control.rect, 
			fx_time=0.2, delay=0.1 * index)
	
	def control_exit_sprite(self, control, index):
		"""Return a sprite to animate a specific controls exit
		from the page
		"""
		return Slide(control.render(), 
			control.rect, control.rect.move(self.rect.width, 0), 
			fx_time=0.2, delay=0.1 * index)
	
	def entrance_sprites(self, start_index=0):
		"""Return the sprites to animate the page's entrance"""
		return [self.control_entrance_sprite(control, index + start_index) 
			for index, control in enumerate(self.controls)]
	
	def exit_sprites(self, start_index=0):
		"""Return the sprites to animate the page's exit"""
		return [self.control_exit_sprite(control, index + start_index) 
			for index, control in enumerate(self.controls)]


class RootPage(Page):
	"""Top level frontend page"""

	def __init__(self, frontend):
		Page.__init__(self, frontend)
		self.controls = [
			Button((530, 200), 'Single Player', frontend.enter_page, SinglePlayerPage),
			Button((530, 290), 'Multiplayer'),
			Button((530, 380), 'Options'),
			Button((530, 470), 'Credits'),
			Button((530, 560), 'Exit', frontend.quit),
		]

class SinglePlayerPage(Page):
	"""Single player setup frontend page"""

	def __init__(self, frontend):
		Page.__init__(self, frontend)
		self.controls = [
			Button((530, 560), 'Cancel', frontend.enter_page, RootPage),
		]


class Button(pygame.sprite.Sprite):

	def __init__(self, position, text, action=None, *action_args):
		"""Frontend push button"""
		pygame.sprite.Sprite.__init__(self)
		self.glow_img = load_image('button-lg-glow.png')
		self.select_img = load_image('button-lg-select-glow.png')
		font = pygame.font.Font('fonts/forgottenfuturist/Forgotbi.ttf', 48)
		button_text = font.render(text, True, (255, 255, 255))
		self.face_img = load_image('button-lg.png').copy()
		self.face_img.blit(
			button_text, button_text.get_rect(center=self.face_img.get_rect().center))
		self.pressed_img = load_image('button-lg-pressed.png').copy()
		self.pressed_img.blit(
			button_text, button_text.get_rect(center=self.face_img.get_rect().center).move(2, 2))
		self.rect = self.glow_img.get_rect(topleft=position[:2])
		self.action = action
		self.action_args = action_args
		self.highlighted = False
		self.pressed = False
	
	def draw(self, surface, pos=None):
		glow_img = self.highlighted and self.select_img or self.glow_img
		button_rect = surface.blit(glow_img, pos or glow_img.get_rect(center=self.rect.center))
		face_img = self.pressed and self.pressed_img or self.face_img
		surface.blit(face_img, face_img.get_rect(center=button_rect.center))
		return button_rect
	
	def render(self):
		"""Return the full rendered button in a new surface"""
		surface = pygame.Surface(self.rect.size, SRCALPHA, self.face_img)
		self.draw(surface, (0,0))
		return surface
	
	def collides_with(self, pos):
		"""Return true if pos is inside the button"""
		face_rect = self.face_img.get_rect(center=self.rect.center)
		return face_rect.collidepoint(*pos)
	
	## Event Handlers ##

	def mouse_enter(self, ui, event):
		if not event.buttons[0]:
			self.highlighted = True
			ui.repaint()
		elif ui.clicked_control is self:
			self.highlighted = True
			self.pressed = True
			ui.repaint()
	
	def mouse_leave(self, ui, event):
		self.highlighted = False
		self.pressed = False
		ui.repaint()
	
	def mouse_down(self, ui, event):
		self.highlighted = True
		self.pressed = True
		ui.repaint()
	
	def mouse_up(self, ui, event):
		self.pressed = False
		ui.repaint()
		if self.action is not None:
			self.action(*self.action_args)


class Slide(pygame.sprite.Sprite):
	
	def __init__(self, image, startpos, endpos, fx_time, delay=0, show_immediate=True):
		"""Slides the image from startpos to endpos in fx_time seconds"""
		pygame.sprite.Sprite.__init__(self)
		self.image = image
		rect = image.get_rect()
		self.rect = [float(startpos[0]), float(startpos[1]), rect.width, rect.height]
		self.delay_frames = int(delay * Frontend.fps)
		self.frames = int(fx_time * Frontend.fps)
		self.show_immediate = show_immediate
		self.vx = float(endpos[0] - startpos[0]) / self.frames
		self.vy = float(endpos[1] - startpos[1]) / self.frames
		
	def update(self):
		if self.delay_frames > 0:
			self.delay_frames -= 1
		elif self.moving:
			self.rect[0] += self.vx
			self.rect[1] += self.vy
			self.frames -= 1
	
	def draw(self, surface):
		if self.show_immediate or self.delay_frames == 0:
			return surface.blit(self.image, self.rect)
		else:
			# Don't display during delay without show_immediate set
			return Rect(self.rect)
	
	@property
	def moving(self):
		return self.frames > 0


class Wipe(pygame.sprite.Sprite):
	
	def __init__(self, image, pos, fx_time, delay=0):
		"""Wipes the image at pos in fx_time seconds"""
		pygame.sprite.Sprite.__init__(self)
		self.image = image
		self.rect = image.get_rect(topleft=pos[:2], width=0)
		self.source_rect = self.image.get_rect(width=0)
		self.max_width = self.image.get_rect().width
		self.delay_frames = int(delay * Frontend.fps)
		self.frames = int(fx_time * Frontend.fps)
		self.speed = self.image.get_rect().width / self.frames
		
	def update(self):
		if self.delay_frames > 0:
			self.delay_frames -= 1
		elif self.moving:
			self.source_rect.width = min(self.rect.width + self.speed, self.max_width)
			self.rect.width = self.source_rect.width
			self.frames -= 1
	
	def draw(self, surface):
		if self.rect.width:
			return surface.blit(self.image, self.rect, self.source_rect)
		else:
			return self.rect
	
	@property
	def moving(self):
		return self.frames > 0


if __name__ == '__main__':
	# Run the frontend as a standalone app for testing
	import sys
	import eos
	pygame.init()
	if '--window' in sys.argv:
		screen = pygame.display.set_mode((1024, 768))
	else:
		screen = pygame.display.set_mode((1024, 768), FULLSCREEN)
	Frontend(screen).run()
	#eos.run()
