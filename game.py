## Eos, Dawn of Light -- A Space Opera
## Copyright (c) 2007 Casey Duncan and contributors
## See LICENSE.txt for licensing details

# Global game elements
# $Id$

import sys
import math
from time import sleep
import platform
import display
import media
import net
import ode
import pygame
from pygame.locals import *
import random
import vector
import vessel
import message
import particle
import sprite
import scheduler

fps = 40 # Framerate, averaged over time
ai_interval = 70 # Release AI every N seconds
max_fleet_size = 10 # Maximum # of vessels for each side
universe = None # ode world
collision_space = None # ode collision space

# Other globally accessible things
clock = None
time = 0 # milliseconds since game began
frame_no = 0 # current frame number
server = None
client = None
objects = sprite.NetGroup()
local_player = None
map = None
starfield = None
camera = None
target = None
windowed = False
debug = False
stats = False

def init(is_server=False, is_client=False, host='127.0.0.1', port=252525,
		fullscreen=False, race=None, ship=None, resolution=None, latency=None,
		packet_loss=None, mute=True):
	import body
	import selection
	from sprite import RenderedGroup
	from pygame.sprite import Group
	from stars import StarField
	from staticbody import Planet
	from vessel import Vessel, KeyboardControl
	from media import load_image
	from map import Map
	from camera import Camera
	import player
	global universe, collision_space, map, starfield
	global fps, clock, time, local_player, camera
	global target, windowed
	windowed = not fullscreen

	# Initialize pygame and setup main screen
	pygame.mixer.pre_init(11025)
	pygame.init()

	# Get the available display modes and use the best one
	if platform.system() == 'Darwin' and platform.processor() == 'powerpc':
		# Use lower quality setting by default on powerpc macs for performance
		pixel_depth = 16
		if resolution is None:
			resolution = 'med'
	else:
		pixel_depth = 32
		if resolution is None:
			resolution = 'high'
	display.setup(resolution, fullscreen and not debug, pixel_depth)

	# Create ode world
	universe = ode.World()
	collision_space = ode.HashSpace()
	clock = pygame.time.Clock()

	# Load resources
	media.preload_images('data/*.png')
	media.preload_images('data/*.gif')
	media.create_images()
	media.load_animations()

	if ship is None and race is None:
		stars = StarField(display.rect)
		stars.draw(display.surface)
		pygame.display.flip()
		# Select ship
		ships = [
			Vessel.load('vessels/rone/draken'),
			Vessel.load('vessels/rone/drach'),
			Vessel.load('vessels/rone/kraken'),
			Vessel.load('vessels/naree/lotus'),
			Vessel.load('vessels/naree/cress'),
			Vessel.load('vessels/naree/corde'),
			Vessel.load('vessels/sc/pegasus'),
			Vessel.load('vessels/sc/striker'),
			Vessel.load('vessels/sc/aurora'),
		]
		import math
		font = pygame.font.Font('fonts/forgottenfuturist/Forgotbi.ttf', 24)
		title_image = font.render('Select Ship', True, (255, 255, 255))
		left_arrow = media.image('pointy-green', rotation=math.pi, colorkey=True)
		right_arrow = media.image('pointy-green', rotation=0, colorkey=True)
		choice = 0
		rot = -math.pi / 2
		while ship is None:
			display.surface.fill((0, 0, 0))
			stars.move((1, 1))
			stars.draw(display.surface)
			display.surface.blit(title_image, title_image.get_rect(
				centerx=display.rect.centerx, centery=display.rect.centery - 75))
			chosen_ship = ships[choice]
			ship_img = media.image(chosen_ship.image_name, 1.0, rot)
			display.surface.blit(ship_img, ship_img.get_rect(center=display.rect.center))
			ship_name = font.render(
				('%s %s' % (chosen_ship.vessel_class, chosen_ship.vessel_type)).title(),
				True, (255, 255, 255))
			display.surface.blit(ship_name, ship_name.get_rect(
				centerx=display.rect.centerx, centery=display.rect.centery + 75))
			display.surface.blit(left_arrow, left_arrow.get_rect(
				centerx=display.rect.centerx - 75, centery=display.rect.centery))
			display.surface.blit(right_arrow, right_arrow.get_rect(
				centerx=display.rect.centerx + 75, centery=display.rect.centery))
			pygame.display.flip()
			sleep(0.022)
			rot += math.pi / 100
			time += 22
			for event in pygame.event.get():
				if event.type == QUIT or (
					event.type == KEYDOWN and event.key == K_ESCAPE):
					raise SystemExit
				elif event.type == KEYDOWN and event.key == K_RIGHT:
					choice = (choice + 1) % len(ships)
				elif event.type == KEYDOWN and event.key == K_LEFT:
					choice = (choice - 1) % len(ships)
				elif event.type == KEYDOWN and event.key == K_RETURN:
					ship = ships[choice].vessel_class
		stars.kill()
		for ship_choice in ships:
			ship_choice.kill()
		time = 0

	# Establish player and ship, for now randomly select player number 1 or 2
	local_player = player.HumanPlayer(random.choice([1, 2]), race, ship) 
	camera = Camera(local_player.vessel)
	target = selection.Target(local_player.vessel)

	# Setup background and map
	starfield = StarField(display.rect)
	map = Map('sol')
	map.vessels.add(local_player.vessel)

	# Find the base and put the player next to it
	for planet in map.planets:
		if planet.base is not None and planet.base.owner is local_player:
			offset = vector.vector2(planet.collision_radius * 1.25, planet.collision_radius * 1.25)
			local_player.vessel.set_position(planet.position + offset)

	# Setup local messaging
	message.init(local_player)

	# Kickstart the soundtrack
	if not mute:
		media.Soundtrack(local_player.race).start()
	media.mute = mute

	# Establish multiplayer
	if is_server:
		global server
		server = net.Server(host, port, latency, packet_loss)
	elif is_client:
		global client
		client = net.Client(host, port, latency, packet_loss)

def exit():
	sys.exit()

def check_collision(collision_set, geom1, geom2):
	"""Check two objects for collision and if they collide, dispatch to the
	correct collision business logic on the affected objects for further 
	processing
	"""
	try:
		contacts = ode.collide(geom1, geom2)
		if contacts:
			collision_set.add((geom1.parent, geom2.parent, tuple(contacts)))
	except:
		# Output traceback hidden by ode
		import traceback
		traceback.print_exc()
		raise

def handle_collisions():
	global collision_space
	collided = set()
	# Collect all of the colliding bodies via our callback
	collision_space.collide(collided, check_collision)
	# Dispatch to collision handling logic. We don't do this
	# directly from our callback because ode holds a lock on the
	# collision space inside collide(). That prevents our code
	# from modifying the collision space (e.g., adding or removing
	# objects). Dispatching outside removes that limitation
	for body1, body2, contacts in collided:
		body1.collide(body2, contacts)
		body2.collide(body1, contacts)

def play():
	from ai import AIVessel
	import body
	import media
	import event
	import panel
	global universe, background, stats
	global fps, clock, time, frame_no
	global ai_interval, camera, map
	friends = pygame.sprite.Group()
	enemies = pygame.sprite.Group()
	next_wave = 0
	event_handler = event.MainHandler([panel.handler, event.playing_field_handler])
	clock.tick()
	while 1:
		event_handler.handle_events()
		keystate = pygame.key.get_pressed()
		local_player.control.set_keystate(keystate)
		if client:
			client.step(keystate)
		handle_collisions()
		display.surface.fill((0, 0, 0))
		sprite.layers.update()
		scheduler.dispatch(time)
		camera.update()
		sprite.layers.draw(display.surface)
		pygame.display.flip()
		universe.quickStep(min(clock.get_time() / 1000.0, 0.05))
		if server:
			server.step()
		time += clock.tick()
		sleep(0)
		fps = clock.get_fps() or fps
		if stats and frame_no % 30 == 0:
			cache = media._scaled_image_cache
			print 'b:', body.body_count,
			print 's:', len(sprite.layers),
			print 'i:', len(cache), (cache.accesses - cache.misses) * 100 / cache.accesses, '%',
			print 'p:', particle.particle_count,
			print 't:', len(scheduler._taskq),
			print 'fps:', fps

		## Temporary wave-based game play ##
		if not client:
			if time > next_wave:
				target = local_player.vessel
				wave_race = random.choice([r for r in ['naree', 'rone', 'sc'] 
					if r != local_player.race])
				wave_category = local_player.category<<2
				if not target.alive():
					target = random.choice(map.planets)
				position = vector.unit(random.random() * vector.fullcircle) * 1000
				warship = random.random() * frame_no > 800
				if warship and len(enemies) < max_fleet_size:
					if wave_race == 'rone':
						ai = AIVessel.load('vessels/rone/draken', category=wave_category)
					elif wave_race == 'naree':
						ai = AIVessel.load('vessels/naree/lotus', category=wave_category)
					elif wave_race == 'sc':
						ai = AIVessel.load('vessels/sc/pegasus', category=wave_category)
					ai.set_position(target.position + position)
					enemies.add(ai)
				else:
					warship = False
				barrier = 300
				friendly = warship
				while ((friendly or random.random() * frame_no > barrier) 
					and len(friends) < max_fleet_size):
					if random.random() * frame_no < barrier * 6:
						if local_player.race == 'rone':
							ship = 'vessels/rone/drach'
						elif local_player.race == 'naree':
							ship = 'vessels/naree/cress'
						elif local_player.race == 'sc':
							ship = 'vessels/sc/striker'
					else:
						if local_player.race == 'rone':
							ship = 'vessels/rone/draken'
						elif local_player.race == 'naree':
							ship = 'vessels/naree/lotus'
						elif local_player.race == 'sc':
							ship = 'vessels/sc/pegasus'
					friend = AIVessel.load(ship, objective=target, category=local_player.category)
					friend.set_position(target.position - position)
					friends.add(friend)
					barrier *= 2
					friendly = False
				barrier = 300 + max(50 * len(enemies) - 50 * len(friends), -250)
				while len(enemies) < max_fleet_size:
					if warship and random.random() * frame_no < barrier:
						break
					if wave_race == 'rone':
						ai = AIVessel.load('vessels/rone/drach', category=wave_category)
					elif wave_race == 'naree':
						ai = AIVessel.load('vessels/naree/cress', category=wave_category)
					elif wave_race == 'sc':
						ai = AIVessel.load('vessels/sc/striker', category=wave_category)
					ai.set_position(target.position + position)
					enemies.add(ai)
					warship = True
					barrier *= 2
				next_wave = time + ai_interval * 1000
				ai_interval = max(ai_interval * .9, 5)
			elif not enemies:
				next_wave = min(time + 5000, next_wave)
		frame_no += 1
