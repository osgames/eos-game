Main Engines
============

Impulse Engine
--------------
The mainstay of sub-light propulsion technology for nearly a century, the venerable impulse engine is ubiquitous throughout known space. Requiring virtually no maintenance, they propel everything from courier craft to hulking battle frigates. Impulse engines provide equally adequate thrust and velocity, making them some of the most popular all-around engines. At low speeds, impulse engines are fairly economical to run, though their efficiency drops dramatically as speed increases. Even though periodic maintenance is not required, it can greatly enhance the longevity of the engine. Many a freighter captain have been stranded when their impulse drives failed prematurely due to neglect.

Stationary Thrust: 1
Max Thrust: 1@0
Speed Threshold: 400
Power Drain: 1 (max)
Mass: 4 tons
Cost: 50,000cr

Ion Engine
----------
Research into improving the high-speed efficiency of impulse drive lead to the development of ion engine technology. Using a beam of highly charged molecules, these engines can deliver an order of magnitude better efficiency at high speed over their impulse counterparts. This high-speed efficiency comes at the cost of a much lower power/weight ratio, hampering accelleration. Erosion of the electrostatic grid used to accellerate the charged particles requires periodic maintenance, but the manufacturer guarantees 5000 hours of operating time between services.

Stationary Thrust: 0.5
Max Thrust: 1@600
Speed Threshold: 1200
Power Drain: 0.5 (max)
Mass: 10 tons
Cost: 80,000cr

MPD Thruster
------------
Gerundive enterprises took a big risk developing the magneto-plasma-dynamic thruster technology. Early failures resulted in them becoming a near laughing-stock of the industry, as many startups before them had tried and failed. After hiring the brilliant physicist Just Van Russo, however, the project breathed new life and in just 18 short months a working prototype was ready for trials. This first generation product is nearly unchanged from the prototype, save fitment of industry standard mounts and interfaces. Even in this early stage the technology shows tremendous promise supplying both high accellerative thrust and terminal velocity. Their tremendous energy consumption mandates onboard power generation, therefore limiting their usefulness on smaller ships. The cathode which generates the tremendous magnetic field will also degrade over time, though it should remain viable for around 8000 operating hours.

Stationary Thrust: 1.5
Max Thrust: 2@300
Speed Threshold: 800
Power Drain: 3 (max)
Mass: 5 tons
Cost: 200,000cr

M2P2
----
Science-fiction has long envisioned craft sailing effortlessly on the solar wind. Efforts to create solar sails made of reflective materials were of limited success and suffered from low efficiency, high maintenance and poor lifespan. Mini-magnetospheric plasma propulsion or M2P2 was designed to harness the power of stars using a conductive plasma contained in a magnetic field. This plasma creates a synthetic magnetosphere whose size varies with the density of the prevailing solar wind thereby maintaining the same thrust regardless of the distance from the star. These magnetic sails can also maneuver using the magnetic fields of nearby planets or the plasma inside a nebula. Only a small energy field is required to contain the inert plasma used to catch the charged particles, thus energy consumption is extremely low, though never zero, even at rest. The theoretical top-speed of M2P2 is close to the speed of light itself inside star systems, however accelleration falls off quickly as speed increases. At lower speeds, however accelleration is quite adequate, and is aided by the low mass of the drive mechanism. Over time the inert plasma can become poisoned by stellar particles, reducing efficiency. The manufacturer recommends inert gas replacement every 2000 operating hours, though many space-farers are known to go much longer between services. M2P2 is incompatible with other drive systems, thus must be fitted exclusively on a given ship.

Stationary Thrust: 0.75
Max Thrust: 1@100
Speed Threshold: 800
Power Drain: 0.33 (constant)
Mass: 2 tons
Cost: 150,000cr

Graviton Diametric Drive
------------------------
The GDD harnesses an artificial asymmetric gravitational field to induce motion into a stationary mass. The ship literally falls toward the field center, which is kept a fixed distance away in the desired direction of motion. The accelleration is proportional to the field strength, though doubling the field strength increases energy consumption four-fold. The terminal velocity is quite high, though still firmly in the sublight range due to decreased field stability at speed. The power requirements to maintain the gravitational field are staggering and mass of the drive unit is substantial, but the resulting field may be projected in any desired direction, thus eliminating the need for a separate maneuvering system other than directional thrusters. GDD requires relativistic navigational instruments, which make it incompatible with other drive units. The powerful field generated requires expert care and handling to avoid compromising the structural intregity of the ship. Only a helmsman first-class or higher is licensed to operate GDDs safely. It is assumed, of course, that under such professional supervision, that no regular maintenance will be required.

Stationary Thrust: 10
Max Thrust: 10@0
Speed Threshold: 800
Power Drain: 8 (max)
Mass: 25 tons
Cost: 2,000,000cr

Quantum Differential Drive
--------------------------
Harnessing the energy of the cosmic background radiation itself, QDDs are a true marvel of engineering achievement. By creating a difference in radiation pressure via a special isotropic medium, thrust is generated with nearly 100% energy efficiency. Unfortunately, dense concentrations of matter or energy near the drive unit interfere significantly with the quantum fluctuations, thus QDDs are unsuitable for massive vessels and cannot be used in conjunction with other types of main engines. On the bright side, motion through space actually stabilizes the quantum flux, making these some of the best performing engines at high speed. Their high performance, small mass and energy efficiency makes them ideal for light and medium vessels of all types. The isotropic medium does erode over time, wearing fastest at high power levels. It is assured, however, that with regular service every 10,000 operating hours, the drive system should never require overhaul.

Stationary Thrust: 1
Max Thrust: 1.5@1000
Speed Threshold: 1500
Power Drain: 0.75 (max)
Mass: 3 tons
Cost: 750,000cr

Spacial Dilation Engine
-----------------------
All other sub-light engines operate under the principle of Newton's second law of motion, where an unbalanced force generates momentum through space. The spacial dialation engine works instead by inducing a local spacial distortion which actually moves the fabric of space-time itself relative to the vessel. Outside the ship, normal motion is observed, but the ship has not been inparted with any momentum whatsoever. What has in fact happened is a realignment of the crystalline geometry of space-time at a quantum level surrounding the vessel. As a result the ship travels with no inertia and experiences no accelleration. The energy required to perform the spacial dialation is immense, and increases geometrically with the physical size of the vessel and the velocity of travel. Since there is no accelleration, the mass of the vessel is insignificant (unless the vessel is massive enough to generate a sizeable gravitational field itself) and the time to achieve terminal velocity is limited only by the electrical discharge capacity of the ship. The maximum speed is nearly the speed of light, limited not by the ship itself which is not in motion in the traditional sense, but by the drag of particles at the periphery of the dilation field. Because there is no inertia, a constant supply of power must be administered to maintain speed, however, changes in course can be executed instantaneously regardless of speed or vessel mass. The spacial dialation field can be finely controlled to execute any desired maneuvers, thus rendering separate maneuvering thrusters unnecessary. Spacecraft must be designed around the SDE, since its principles of operation are so unlike other engines.

Stationary Thrust: 10
Speed Threshold: None
Power Drain: 20 (max)
Mass: 20 tons
Cost: 10,000,000cr

Engine Enhancements and Spares
==============================

Max Burn
--------
Impulse engines lost their pulse? Ion erosion got you down? Max burn is clinically proven by 4 out of 5 space jockeys to reduce engine wear and maximize your maintenance dollar. Nothing else comes close to these claims, and remember no other product is clinically proven! Use with impulse or ion engines only, void where prohibited, results not typical, past performance no guarantee of future sucess, your mileage may vary.

Cost: 5,000cr

Engine Overhaul
---------------
Even with regular maintenance, engine performance degrades over the life of the drive system. An overhaul will restore your drive system to its full potential. Depending on the age and maintenance history of the engine, an overhaul will restore the system to within 70-90% of its performance when new. Each successive overhaul is less effective than the last, but given the cost of engine replacement, its an economical way to get the most from your propulsion system.

Cost: 25% of engine cost

Main Engine Tune-up
-------------------
Impulse, ion and MPD thrusters all lose performance over time due to contaminent build-up and misalignment of internal core components. A tuneup will remove contaminents and adjust the components to within factory specifications. Not only will this restore lost performance, but can greatly increase the longevity of your engines, especially for impulse drives. A tune-up is no substitute for an overhaul, but if performed often enough can greatly increase the time between major services.

Cost: 10,000cr per engine

Engine Inspection
-----------------
Worried that your engine performance is not all it could be? Don't know whether you need an overhaul or if just a simple tune-up will do? Our expert technicians will pour over your engines and give their assessment of performance, comparing it with new engines of the same type. They'll also estimate the performance gained by a tune-up or overhaul as well as the remaining lifespan of your propulsion system.

Cost: 2,000cr per engine

Propulsion Benchmark
--------------------
Want to see how your vessel really performs? Benchmarking your main engines will show you exactly what their capabilities are in terms of accelleration throughout a wide range of speeds and effective top speed, as well as time taken to reach it. You can also compare your vessel against others to see how its performance stacks up.

Cost: 5,000cr

Afterburner
-----------
Coupled to the exhaust of a main engine, an afterburner provides a burst of accelleration and speed on demand. A staple in racing circles and law enforcement, afterburners are also used by freighters to improve of-the-line accelleration and maneuverability. Afterburners are also popular with smugglers and other miscreants who occasionally find themselves persued. The power drain is substantial when in use, thus they are best activated in short bursts.  The mass of the unit trades some maneuverability for top end speed, but this is compensated for by the boost in accelleration provided. Compatible with impulse, ion, MPD and QDD drives.

Mass: 2 tons per engine

Maneuvering Fitments
====================

Thrust Vectoring
----------------
A simple manuvering system that simply redirects up to 20% of the thrust of your main engines to impart rotational momentum on your ship. Though lightweight, the vectoring vanes do reduce the particle velocity of the main engines, thereby reducing top-speed by 10-15%. The thrust vectoring system only functions under main engine power. Compatible with impulse, ion and MPD thrusters.

Mass: 0.5 ton per engine.

Maneuvering Thrust Conduits
---------------------------
These simple manuevering thrusters siphon thrust from your main engines and redirect it to various ports on the periphery of your ship. Up to 20% of your main engine thrust can be directed through the maneuvering conduits. Though lighter than PPM thrusters, the conduits and valves increase turbulence in the main engine plumbing, resulting in about a 15% reduction in accellerative thrust. Compatible with impulse, ion, MPD and QDD drives.

Mass: 1 tons

Pulsed Plasma Maneuvering Thrusters
-----------------------------------
A popular stand-alone maneuvering system, PPM thrusters utilize an arc of electric current to produce quick bursts of directed impulse energy. Though heavier than thrust conduits, they require no performance compromising main engine modifications. Accelleration is excellent at low speeds, though they are notoriously inefficient, draining power at a prodigious rate.

Mass: 4 tons

Pulsed Plasma Directional Thrusters
-----------------------------------
Similar to the pulsed plasma maneuvering system, this simplified design includes thrusters only for directional control, thus is only half the mass of the full system. The same caveats of accelleration and operational efficiency apply as with PPN thrusters. Spacers with main engine systems that provide their own manueverig capabilities often add on directional thrusters for additional maneuverability.

Mass: 2 tons

Company Names
=============
Gerundive Enterprises
Futurity Industries


