General
=======

Naree
=====
Naree ships tend to be light and fast, but relatively expensive. They have powerful energy-based weapons that require precise tactics. Their ships are protected by energy shields that dissipate attacks around the ship. The have high rates of energy regeneration but relatively low energy capacity. 

Rone
====
Rone ships tend to be heavier and slower but very cheap to manufacture. They utilize primarily plasma and hybrid matter-energy weapons that deliver damage in proximity, but with less damage at range. They are protected by heavy regenerative armor the dissipates damaging attacks as heat. They have relatively slow rates of energy regeneration but high energy capacity.

Human
=====
Human ships have slow cruising speeds, but can sprint at very high speeds for short periods. They utilize matter projectile and guided missile weapons that draw from on-board ammunition stores which regenerate slowly over time. The multi-level defense consists of deflector screens and ablative armor, which although rugged does not regenerate. Their small energy stores are used exclusively to power their deflector screens and sprint afterburners.

Vessel Classes
~~~~~~~~~~~~~~

Turret
======
- Extremely slow, fast turn, slow accel
- Medium offense, heavy defense
- Short sensor range
- Support: 1

Weapons
-------
Human: Blastsphere rockets
Naree: Plasmoid Bloom
Rone: Plasma chain gun

Scout
=====
- Fastest but not maneuverable, medium accel
- No offensive armament, light defense
- Double sensor range, can read enemy ship stats at close range, can read planet stats at medium range
- Support: 1

Special abiliies
----------------
All: Guidance system jamming
Rone: cloak
Naree: Teleport to nearest friendly planet
Human: Enemy sensor blackout

Assault Craft
=============
- slow, medium manuverability, slow accel
- No offensive armament, medium defense
- Short-ranged sensors, can read planet stats at close range
- Support: 5

Special abilities
-----------------
All: Planetary assault (consumes ship)

Weapons
-------
Human Typhoon: Commandos (can capture enemy ships, one time use only)
Rone Kraken: Swarm droids (disable ship systems ignoring shields and armor)
Naree Corde: Stasis temporarily removes ships from the battle

Fighters
========
- Very fast and most maneuverable, fast accel
- Light offensive armament, light defense
- Short sensor range
- Support: 1

Primary Weapon (forward fire):
------------------------------
Rone Drach: Proton grenade cannon
Naree Cress: Lepton beam
Human Striker: Burst-fire rockets

Secondary Weapon/Ability
------------------------
Rone Drach: Phase disruptors reduce shield regeneration and deflector screen efficiency
Naree Cress: Rush ability allows them to approach unhindered with immediacy
Human Striker: Plasmonic mines are invisible when not in motion, cause severe damage in an area upon contact.

Warships
========
- Medium speed and maneuverability, medium accel
- Medium offensive armament, medium defense
- Medium sensor range
- Support: 3

Primary Weapon (forward fire):
----------------------------------
Rone Draken: Plasma Cannon
Naree Lotus: Fullerene Pulse Cannon
Human Pegasus: Neutronium Bolt Cannon

Secondary Weapon:
-----------------
Rone Draken: Flak Bomb
Naree Lotus: Gnats
Human Pegasus: Dagger missiles

Destroyers
==========
- Medium speed, low maneuverability, medium accel
- Heavy offensive armament, medium defense
- Medium sensor range
- Support: 5

Primary Weapon (forward fire):
------------------------------
Rone Gotha: Glaive Blade
Naree Lark: Gravitometric Torpedo
Human Maurader: Incendiary bomb

Secondary Weapon (off-axis fire):
---------------------------------
Rone Gotha: Richochet bolt
Naree Lark: Polaron chain
Human Maurader: Storm multi-warhead missles

Capitol Ships
=============
- Medium speed, low maneuverability, low accel
- Heavy armament
- Short sensor range
- Support: 8

Primary Weapon (turreted):
-------------------------------
Rone Tarkus: Anti-matter shard cannon
Naree Orchis: Ion cannon
Human Blackstar: Magneto pulse cannon

Secondary Weapon
----------------
Rone Tarkus: Rapier fighter bay
Naree Orchis: Nova damages enemies in a radius surrounding the ship
Human Blackstar: Broadside unleases a tremendous volley of fire from the port or starboard side of the ship.

Special Attack
--------------
Rone Tarkus: Battering Ram (uses ship momentum as an attack)
Naree Orchis: Energy recoil (uses enemy energy against ship)
Human Blackstar: Force-field repels enemy projectiles and reflects them back on the attackers

Battle Stations
===============
- Low speed, low maneuverability, low accel
- Long sensor range
- Support: 12

Defensive Weapon (turreted)
-------------------------------
Rone: Repulsion volley, repels attacking ships at great speed
Naree: Sensor shock temporarily overloads a ships sensors and visual scanners
confusing it
Human: EM disruptor temporarily disables ships and drains their energy

Passive Buff
------------
Rone: Increased Energy regeneration
Naree: Increased shield regeneration
Human: Armor repair droids

Special Ability:
----------------
Rone: Project a cloaking field that hides surrounding friendly ships
Naree: Teleport a group of friendly ships to the station position
Human: Projects impenetrable defense screens that stops both ships and weapons

Tarkus
Orchis
