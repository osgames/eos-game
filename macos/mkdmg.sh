#!/bin/bash
#
# $Id$
set -e

if [ "$1" == "--help" ]; then
	echo >&2 "
Build the Eos .app bundle and create a disk image file suitable for release.
Run from the top-level eos directory

USAGE

  macos/mkdmg.sh VERSION
"
	exit 3
fi

if [ -z "$1" ]; then
	echo "Version number argument not supplied"
	exit 1
fi

DMG_FILE="macos/Eos-$1.dmg"

rm -rf macos/dist macos/build ${DMG_FILE}
python2.5 macos/setup.py py2app --bdist-base=macos/build --dist-dir=macos/dist
mv macos/dist/eos.app macos/dist/Eos.app
rm -rf macos/dist/Eos.app/Contents/Resources/lib/python2.5/OpenGL
cp LICENSE.txt macos/dist/
cp RELEASE.html "macos/dist/readme.html"
hdiutil create -fs HFS+ -srcfolder macos/dist -volname "Eos - Dawn of Light $1" "${DMG_FILE}"
hdiutil attach "${DMG_FILE}"


