"""
Script for generating standalone Mac OS X application
bundle for Eos using py2app

Usage:
    python setup.py py2app

$Id$
"""

from setuptools import setup

APP = ['eos.py']
DATA_FILES = ['data', 'music', 'sounds', 'content', 'vessels', 'fonts']
OPTIONS = {'argv_emulation': True,
           'iconfile': 'data/eos.icns'}

setup(
    app=APP,
    data_files=DATA_FILES,
    options={'py2app': OPTIONS},
    setup_requires=['py2app'],
)
