## Eos, Dawn of Light -- A Space Opera
## Copyright (c) 2007 Casey Duncan and contributors
## See LICENSE.txt for licensing details

# System maps
# $Id$

import sys
import pygame
import ConfigParser
import game
import staticbody
import panel
import vector

class Map:

	name = ''
	description = ''

	margin = 3000
	boundary_aspect = 3.0/2.0
	
	def __init__(self, config_file, planet_factory=staticbody.Planet):
		"""Load a map from file, either a file-like object or a file path
		string

		>>> from StringIO import StringIO
		>>> game.init()
		>>> class TestPlanet:
		...  def __init__(self, name, **kw):
		...    self.name = name
		...    self.params = kw
		...    self.rect = pygame.Rect(kw['x'] - 500, kw['y] - 500, 1000, 1000)
		>>> config = StringIO('''
		... [general]
		... name: test map
		... description: This is only a test
		...
		... [planet:Floom]
		... x: 0
		... y: 1
		... type: watery
		...
		... [planet:Flam]
		... x: 1000
		... y: 2000
		... type: custardy
		... ''')
		>>> m = Map(config, planet_factory=TestPlanet)
		>>> m.name
		'test map'
		>>> m.description
		'This is only a test'
		>>> len(m.planets)
		2
		>>> m.planets[0].name
		'Floom'
		>>> m.planets[0].params['x']
		'0'
		>>> m.planets[0].params['y']
		'1'
		>>> m.planets[0].params['type']
		'watery'
		>>> m.planets[1].name
		'Flam'
		>>> m.planets[1].params['x']
		'1000'
		>>> m.planets[1].params['y']
		'2000'
		>>> m.planets[1].params['type']
		'custardy'
		"""
		if isinstance(config_file, str):
			config_file = open('content/maps/%s' % config_file, 'rb')
		file_name = getattr(config_file, 'name', repr(config_file))
		parser = ConfigParser.SafeConfigParser()
		parser.readfp(config_file)
		self.rect = None
		self.planets = []
		self.vessels = pygame.sprite.Group()
		for section in parser.sections():
			if section == 'general':
				for option, value in parser.items(section):
					if option == 'name':
						self.name = value
					elif option == 'description':
						self.description = value
					else:
						raise MapConfigError, (
							'Invalid option %s in section [general] of map config file %s'
							% (option, file_name))
			elif section.startswith('planet:'):	
				nil, planet_name = section.split(':')
				planet_params = dict(parser.items(section))
				try:
					planet = planet_factory(planet_name, **planet_params)
				except Exception, err:
					raise MapConfigError, (
						'Exception creating planet from section [%s] of map config file %s: %s'
						% (section, file_name, err)), sys.exc_info()[-1]
				planet_rect = pygame.Rect(0, 0, 
					planet.radius + self.margin, planet.radius + self.margin)
				planet_rect.center = vector.to_tuple(planet.position)
				if self.rect is not None:
					self.rect.union_ip(planet_rect)
				else:
					self.rect = pygame.Rect(planet_rect)
				self.planets.append(planet)
			else:
				raise MapConfigError, (
					'Invalid section [%s] of map config file %s'
					% (section, file_name))
		if not self.planets:
			raise MapConfigError, (
				'Map config %s defined no planets' 
				% (section, file_name))
		# Ensure the map fits into the desired aspect ratio
		desired_width = self.rect.height * self.boundary_aspect
		desired_height = self.rect.width / self.boundary_aspect
		if desired_width > self.rect.width:
			self.rect.inflate_ip(desired_width - self.rect.width, 0)
		if desired_height > self.rect.height:
			self.rect.inflate_ip(0, desired_height - self.rect.height)
		self.minimap = panel.MiniMap(self)
	
	def get_planet(self, name):
		"""Return the planet with the name specified or None if no planet exists"""
		for planet in self.planets:
			if planet.name == name:
				return planet


class MapConfigError(Exception):
	"""Map configuration file error"""


if __name__ == '__main__':
	"""Run tests if executed directly"""
	import sys, doctest
	failed, count = doctest.testmod()
	print 'Ran', count, 'test cases with', failed, 'failures'
	sys.exit(failed)
