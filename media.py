## Eos, Dawn of Light -- A Space Opera
## Copyright (c) 2007 Casey Duncan and contributors
## See LICENSE.txt for licensing details

# Graphics and sound utilities
# $Id$

import os.path
import ConfigParser
import math
from math import degrees
import glob
import random
import threading
import time
import pygame
from pygame.constants import *
import game
import vector
from vector import fullcircle
import display

mute = False

_images = {} # image name => image, width

def load_image(name):
	"""Loads a single image from disk"""
	return optimize_image(pygame.image.load('data/%s' % name))

def preload_images(pattern):
	"""Preloads all images matching the specified glob pattern.
	These images are optimized and cached
	"""
	global _images
	for path in glob.glob(pattern):
		image_name = os.path.basename(path)
		image = optimize_image(pygame.image.load(path))
		_images[image_name] = (image, image.get_rect().width)

def optimize_image(image):
	if image.get_bitsize() == 32:
		# Image has alpha channel, note this prevents
		# applying alpha on blit
		image = image.convert_alpha()
		image.set_alpha(0, pygame.RLEACCEL)
		return image
	else:
		return image.convert()
		if not display.surface.get_flags() & HWSURFACE:
			transparent = image.get_colorkey()
			if transparent:
				image.set_colorkey(transparent, RLEACCEL)
		return image

def set_image(name, image):
	global _images
	_images[name] = (image, image.get_rect().width)


class Cache:
	"""Simple, fast, bounded cache that gives approximate MRU behavior"""

	def __init__(self, max_size, load_factor=0.85):
		self.max_size = max_size
		self.max_recent_size = int(max_size * load_factor)
		self._recent = {} # Recently accessed bucket
		self._aged = {} # Less recently accessed bucket
		self.accesses = 0
		self.misses = 0
		self.adds = 0
		self.flips = 0
		self.purged = 0
	
	def __getitem__(self, key):
		self.accesses += 1
		try:
			try:
				return self._recent[key]
			except KeyError:
				# Promote aged element to "recent"
				value = self._aged.pop(key)
				self._recent[key] = value
				return value
		except KeyError:
			self.misses += 1
			raise
	
	def __len__(self):
		return len(self._recent) + len(self._aged)
	
	def __contains__(self, key):
		return key in self._recent or key in self._aged
	
	def __setitem__(self, key, value):
		assert value is not None
		self.adds += 1
		if key in self._aged:
			del self._aged[key]
		if len(self._recent) >= self.max_recent_size:
			# Flip the cache discarding aged entries
			#self.flips += 1
			#print self.flips, 'cache flips in', self.adds, ' adds. ',
			#print self.misses, 'misses in', self.accesses, 'accesses (',
			#print (self.accesses - self.misses) * 100 / self.accesses, '% hit rate) ',
			#print 'with', self.purged, 'purged'
			self._aged = self._recent
			self._recent = {}
		self._recent[key] = value
		while self._aged and len(self) > self.max_size:
			# Over max size, purge aged entries
			self.purged += 1
			self._aged.popitem()

_scaled_image_cache = Cache(6000)
_rotation_step_angle = fullcircle / 250.0
_empty_image = pygame.Surface((0, 0))

def image(name, scale=1.0, rotation=0, colorkey=False):
	"""Return a preloaded image by name at the specified scale and rotation"""
	global _images, _scaled_image_cache, _rotation_step_angle, _empty_image
	orig_image, orig_width = _images[name]
	scaled_width = int(orig_width * scale)
	if scaled_width:
		rotation_step = int(rotation / _rotation_step_angle) % 250
		try:
			return _scaled_image_cache[name, scaled_width, rotation_step]
		except KeyError:
			image = pygame.transform.rotozoom(orig_image, 270 - degrees(rotation), scale)
			if colorkey:
				image = image.convert()
				image.set_colorkey(image.get_at((0, 0)), pygame.RLEACCEL)
			else:
				image.set_alpha(0, pygame.RLEACCEL)
			_scaled_image_cache[name, scaled_width, rotation_step] = image
			return image
	else:
		return _empty_image

def fit_image(name, rect):
	"""Return a preloaded image scaled to fit inside rect"""
	global _images
	orig_image, width = _images[name]
	width, height = orig_image.get_rect().size
	if width > rect.width or height > rect.height:
		scale = min(float(rect.width) / float(width), float(rect.height) / float(height))
	else:
		scale = 1.0
	return image(name, scale)


class Animation:

	image_glob = ''
	images = None
	colorkey = (0, 0, 0)
	initial_alpha = 255
	fade_alpha = 0
	fps = 30
	cycle = False

	@classmethod
	def preload_images(cls):
		"""Load the animation images from disk, must be executed before the
		animation class can be used
		"""
		alpha = cls.initial_alpha
		cls.images = []
		for path in glob.glob(cls.image_glob):
			image = pygame.image.load(path).convert()
			image.set_colorkey(cls.colorkey)
			image.set_alpha(max(alpha, 0), RLEACCEL)
			alpha -= cls.fade_alpha
			cls.images.append(optimize_image(image))
			
	def __init__(self):
		assert self.images is not None, 'Animation not loaded, did you call preload_images()?'
		self.start_time = game.time
	
	def next_image(self, apparent_size=1.0):
		"""Return the next animation frame or None if the animation is complete"""
		frame = (game.time - self.start_time) / (1000 / self.fps)
		if self.cycle:
			frame = frame % len(self.images)
		if frame < len(self.images):
			global _scaled_image_cache
			apparent_size = round(apparent_size, 1)
			try:
				return _scaled_image_cache[self.images[frame], apparent_size]
			except KeyError:
				scaled_image = pygame.transform.rotozoom(self.images[frame], 0, apparent_size)
				#image.set_colorkey(colorkey, RLEACCEL)
				_scaled_image_cache[self.images[frame], apparent_size] = scaled_image
				return scaled_image


class LargeExplosion(Animation):
	image_glob = 'data/splode/*.jpg'


class SmallExplosion(Animation):
	image_glob = 'data/proton-grenade/*.png'


class FullereneAnimation(Animation):
	cycle = True

	rpm = 450
	fps = 45
	growth = 1.5
	size = 31
	max_shadows = 2
	particles = 3
	particle_radius = 5

	@classmethod
	def preload_images(cls):
		"""Render images for animation"""
		shadows = []
		cls.images = []
		frames = cls.fps * 60 / cls.rpm 
		partsize = cls.particle_radius * 2
		rot_per_frame = vector.fullcircle / frames
		image = pygame.Surface((cls.size, cls.size))
		image.fill((0,0,0))
		for run in range(2):
			for rot in range(frames):
				if len(shadows) > cls.max_shadows:
					shadows = shadows[1:]
				shadows = [pygame.transform.rotozoom(image, 0, cls.growth) 
					for image in shadows + [image]]
				image = pygame.Surface((cls.size, cls.size))
				image.fill((0,0,0))
				image.set_colorkey((0,0,0))
				rect = image.get_rect()
				for i in range(cls.particles):
					direction_x, direction_y = vector.to_tuple(
						vector.unit(rot * rot_per_frame + i * vector.fullcircle / cls.particles))
					part = (int(rect.centerx + direction_x * cls.particle_radius),
							int(rect.centery + direction_y * cls.particle_radius))
					bg = random.randint(0, 255)
					pygame.draw.circle(image, (255, bg, 255-bg), part, cls.particle_radius)
				bg = random.randint(0, 255)
				pygame.draw.circle(image, (255, bg, 255-bg), rect.center, cls.particle_radius)
				if run:
					rect = shadows[0].get_rect()
					frame_img = pygame.Surface(rect.size, SRCALPHA, 32)
					shade = 7
					for shadow in shadows:
						shade = shade * 11 / 6
						shadow.set_colorkey(shadow.get_at((0, 0)))
						shadow.set_alpha(shade)
						frame_img.blit(shadow, shadow.get_rect(center=rect.center))
					frame_img.blit(image, image.get_rect(center=rect.center))
					frame_img.convert_alpha()
					frame_img.set_alpha(255, RLEACCEL)
					cls.images.append(frame_img)


class FullereneExplosion(FullereneAnimation):
	cycle = False

	@classmethod
	def preload_images(cls):
		"""Render images for animation"""
		shadows = []
		cls.images = []
		frames = cls.fps * 60 / cls.rpm
		rot_per_frame = vector.fullcircle / frames
		image = pygame.Surface((cls.size, cls.size))
		image.fill((0,0,0))
		fade = 255 / frames
		for run in range(2):
			for rot in range(frames):
				if len(shadows) > cls.max_shadows:
					shadows = shadows[1:]
				shadows = [pygame.transform.rotozoom(image, 0, cls.growth) 
					for image in shadows + [image]]
				image = pygame.Surface((cls.size, cls.size))
				image.fill((0,0,0))
				image.set_colorkey((0,0,0))
				rect = image.get_rect()
				bright = 255 - rot * fade
				for i in range(cls.particles):
					direction_x, direction_y = vector.to_tuple(
						vector.unit(rot * rot_per_frame + i * vector.fullcircle / cls.particles))
					part = (int(rect.centerx + direction_x * cls.particle_radius),
							int(rect.centery + direction_y * cls.particle_radius))
					bg = random.randint(0, bright)
					pygame.draw.circle(image, (bright, bg, bright-bg), part, cls.particle_radius)
				bg = random.randint(0, bright)
				pygame.draw.circle(image, (bright, bg, bright-bg), rect.center, cls.particle_radius)
				if run:
					rect = shadows[0].get_rect()
					frame_img = pygame.Surface(rect.size, SRCALPHA, 32)
					shade = 7 + rot * 2
					for shadow in shadows:
						shade = shade * 11 / 6
						shadow.set_colorkey(shadow.get_at((0, 0)))
						shadow.set_alpha(shade)
						frame_img.blit(shadow, shadow.get_rect(center=rect.center))
					frame_img.blit(image, image.get_rect(center=rect.center))
					frame_img.convert_alpha()
					frame_img.set_alpha(255, RLEACCEL)
					cls.images.append(frame_img)


def load_animations():
	LargeExplosion.preload_images()
	SmallExplosion.preload_images()
	FullereneAnimation.preload_images()
	FullereneExplosion.preload_images()

def create_images():
	def pointy_image(color):
		pointy = pygame.Surface((160, 190), SRCALPHA, 32)
		pygame.draw.polygon(pointy, color, [(0, 190), (80, 0), (160, 190)])
		color = pygame.color.subtract(color, 60)
		pygame.draw.polygon(pointy, color, [(0, 190), (80, 0), (160, 190)], 15)
		pygame.draw.circle(pointy, color, (80, 215), 85)
		pygame.draw.circle(pointy, (0, 0, 0, 0), (80, 230), 85)
		return pygame.transform.rotozoom(pointy, 0, 0.1)
	set_image('pointy-blue', pointy_image((55, 80, 216)))
	set_image('pointy-green', pointy_image((0, 155, 38)))
	set_image('pointy-red', pointy_image((216, 36, 30)))
	set_image('pointy-white', pointy_image((255, 255, 255)))
	def planet_pointy_image(color):
		pointy = pygame.Surface((160, 290), SRCALPHA, 32)
		pygame.draw.polygon(pointy, color, [(0, 130), (80, 0), (160, 130)])
		color2 = pygame.color.subtract(color, 60)
		pygame.draw.polygon(pointy, color2, [(0, 130), (80, 0), (160, 130)], 15)
		pygame.draw.circle(pointy, color2, (80, 175), 85)
		pygame.draw.circle(pointy, (0, 0, 0, 0), (80, 190), 85)
		pygame.draw.circle(pointy, color2, (80, 185), 60)
		pygame.draw.circle(pointy, color, (80, 185), 45)
		return pygame.transform.rotozoom(pointy, 0, 0.1)
	set_image('pointy-planet-blue', planet_pointy_image((55, 80, 216)))
	set_image('pointy-planet-green', planet_pointy_image((0, 155, 38)))
	set_image('pointy-planet-red', planet_pointy_image((216, 36, 30)))
	set_image('pointy-planet-white', planet_pointy_image((255, 255, 255)))

_sound_cache = {}
_sound_last_played = {}

def load_sound(name):
	global _sound_cache
	if name not in _sound_cache:
		_sound_cache[name] = pygame.mixer.Sound('sounds/%s' % name)
	return _sound_cache[name]

def play_sound(name, volume=1.0, position=None, min_spacing=.15):
	"""Play a sound at a particular volume. Return channel where sound will
	play, None if sound does not actually play.

	min_spacing is the minimum spacing between playing this sound
	in seconds. This should be longer for longer sounds to avoid
	using all of the sound channels.
	"""
	if volume > 0 and not mute:
		global _sound_last_played
		last_played = _sound_last_played.get(name, 0)
		if game.time - last_played < min_spacing * 1000:
			# Don't play same sound too close together
			return
		if position is not None:
			distance = vector.distance(game.camera.position, position)
			if distance > 0:
				volume = volume * (700 - distance) / 700
				if volume < 0.1:
					return
		sound = load_sound(name)
		sound.set_volume(volume)
		sound.play() # avoids the reserved soundtrack channel
		_sound_last_played[name] = game.time

class SoundtrackAudio():
	"""Audio track for soundtrack"""

	def __init__(self, filename, volume, min_loops, max_loops):
		self.filename = filename
		self.volume = volume
		self.min_loops = min_loops
		self.max_loops = max_loops

class Soundtrack(threading.Thread):
	"""Background thread to play the soundtrack"""

	def __init__(self, race):
		threading.Thread.__init__(self)
		self.setDaemon(True)
		self.race = race
		self.tracks = []
		self.last_track = None

	def load(self, config_file):
		"""Create a soundtrack from a config file. config_file is either a 
		readable file-like object or a file name.

		The config file is in ConfigParser format and contains a [*.ogg]
		section for each individual audio file which makes up the soundtrack.
		The values under each section describe the properties that should
		be used for that audio file (e.g. volume, how many loops, etc). See
		below for an example.

		>>> config = '''
		... [totally/awesome.ogg]
		... volume: 0.4
		... min_loops: 1
		... max_loops: 3
		... '''
		>>> pygame.mixer.init()
		>>> import StringIO
		>>> s = Soundtrack(None)
		>>> s.load(StringIO.StringIO(config))
		>>> len(s.tracks)
		1
		>>> track = s.tracks[0]
		>>> track.filename
		'totally/awesome.ogg'
		>>> round(track.volume, 1) == 0.4
		True
		>>> track.min_loops
		1
		>>> track.max_loops
		3
		"""
		if isinstance(config_file, str):
			config_file = open(config_file, 'rt')
		parser = ConfigParser.SafeConfigParser()
		parser.readfp(config_file)
		for filename in parser.sections():
			volume = parser.getfloat(filename, 'volume')
			min_loops = parser.getint(filename, 'min_loops')
			max_loops = parser.getint(filename, 'max_loops')
			track = SoundtrackAudio(filename, volume, min_loops, max_loops)
			self.tracks.append(track)
	
	def run(self):
		self.load('music/%s/soundtrack' % self.race)
		while True:
			while pygame.mixer.music.get_busy():
				time.sleep(1)
			while True:
				track = random.choice(self.tracks)
				if track is not self.last_track or len(self.tracks) <= 1:
					break # avoid consecutive repeats
			loops = random.randint(track.min_loops, track.max_loops)
			pygame.mixer.music.load(track.filename)
			pygame.mixer.music.set_volume(track.volume)
			pygame.mixer.music.play(loops)
			self.last_track = track

if __name__ == '__main__':
	"""Run tests if executed directly"""
	import sys, doctest
	failed, count = doctest.testmod()
	print 'Ran', count, 'test cases with', failed, 'failures'
	sys.exit(failed)
