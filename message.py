## Eos, Dawn of Light -- A Space Opera
## Copyright (c) 2007 Casey Duncan and contributors
## See LICENSE.txt for licensing details

# User messaging system
# $Id$

import pygame
import game
import sprite

# Message priorities
panic = 0
critical = 1
important = 2
notice = 3
info = 4
trivial = 5

class Message:

	def __init__(self, sender, recipient, subject, body=None, priority=info):
		"""Create an object encapsulating a message

		sender -- Object or user that created the message
		recipient -- Object or user the message is targeted to.
		subject -- One line summary of message, or the entire message if short.
		body -- Detail of message, if any.
		priority -- Message priority, lower values designate a higher priority.
		"""
		self.time = game.time
		self.sender = sender
		self.recipient = recipient
		self.subject = subject
		self.body = body
		self.priority = priority


class StatusMessage(Message):
	pass


all_players = object() # all player recipient marker


class Messenger:
	"""Delivers messages to their recipients, currently ignores all messages except
	those directed to the local user
	"""

	def __init__(self, local_player):
		self.local_queue = []
		self.local_player = local_player
	
	@staticmethod
	def _queue_msg(queue, msg):
		"""Add a message to a queue
		
		>>> class Msg:
		...  def __init__(self, time, priority):
		...    self.time = time
		...    self.priority = priority
		>>> qmsg = Messenger._queue_msg
		>>> myqueue = []
		>>> msgs = [Msg(100, 99), Msg(99, 99), Msg(101, 99), Msg(200, 0)]
        >>> qmsg(myqueue, msgs[0])
		>>> len(myqueue)
		1
        >>> qmsg(myqueue, msgs[1])
		>>> len(myqueue)
		2
        >>> qmsg(myqueue, msgs[2])
		>>> len(myqueue)
		3
        >>> qmsg(myqueue, msgs[3])
		>>> len(myqueue)
		4
		>>> myqueue == [msgs[3], msgs[1], msgs[0], msgs[2]]
		True
		"""
		queue.append(msg)
		queue.sort(key=lambda msg: (msg.priority, msg.time))
	
	def send(self, msg):
		"""Send the message to its recipient queue"""
		if msg.recipient in (self.local_player.vessel, self.local_player, all_players):
			self._queue_msg(self.local_queue, msg)
	
	def get_next_msg(self, recipient, msg_type=None, current_msg=None):
		"""Get the next queued message for the specified recipient and remove
		it from the queue. If msg_type is specified, return only instances of
		this message type. If current_msg is specified, return only messages
		of greater priority. If there is no such message, return None

		>>> class Msg:
		...  def __init__(self, time, priority, recipient):
		...    self.time = time
		...    self.priority = priority
		...    self.recipient = recipient
		>>> class Gram(Msg): pass
		>>> player = object()
		>>> other = object()
		>>> msgr = Messenger(player)
		>>> msgr.get_next_msg(player)
		>>> msgs = [Msg(1, 1, player), Gram(2, 1, other), 
		... Msg(3, 1, all_players), Msg(4, 0, player), Gram(5, 1, player)]
		>>> for m in msgs: msgr.send(m)
		>>> msgr.get_next_msg(player) is msgs[3]
		True
		>>> msgr.get_next_msg(player, Gram) is msgs[4]
		True
		>>> msgr.get_next_msg(player) is msgs[0]
		True
		>>> msgr.get_next_msg(player) is msgs[2]
		True
		>>> msgr.get_next_msg(player)
		>>> msgr.get_next_msg(other)
		"""
		for msg in self.local_queue:
			if msg.recipient in (recipient.vessel, recipient, all_players) and (
				msg_type is None or isinstance(msg, msg_type)) and (
				current_msg is None or msg.priority > current_msg.priority):
				self.local_queue.remove(msg)
				if isinstance(msg.sender, pygame.sprite.Sprite) and not msg.sender.alive():
					continue # Cull msgs from dead senders
				return msg


class StatusView(sprite.Sprite):
	"""Status message view"""

	font_name = 'fonts/forgottenfuturist/Forgotti.ttf'
	font_size = 18
	color = (255, 255, 255)
	message_ttl = 5000

	def __init__(self, messenger, player):
		sprite.Sprite.__init__(self, sprite.layers.ui)
		self.messenger = messenger
		self.player = player
		self.message = None
		self.message_timeout = None
		self.image = None
		self.rect = pygame.Rect(0, 0, 0, 0)
		self.font = pygame.font.Font(self.font_name, int(self.font_size * game.camera.base_scale))
	
	def update(self):
		if game.time > self.message_timeout:
			self.message = None
			self.rect = pygame.Rect(0, 0, 0, 0)
		next_msg = self.messenger.get_next_msg(self.player, StatusMessage, self.message)
		if next_msg is not None:
			self.message = next_msg
			self.message_timeout = game.time + self.message_ttl
			self._render_msg()

	def draw(self, surface):
		if self.message is not None:
			return surface.blit(self.image, self.rect)
		else:
			return self.rect
	
	def _render_msg(self):
		"""Create an image from the message"""
		self.image = self.font.render(self.message.subject, 1, self.color)
		self.rect = self.image.get_rect(topleft=(3, 3))


def init(local_player):
	"""Initialize a messenger and status view for the local player"""
	global messenger
	messenger = Messenger(local_player)
	StatusView(messenger, local_player)

def send_status(sender, recipient, text, priority=info):
	"""Convenience function to send status messages using the global messenger"""
	global messenger
	messenger.send(StatusMessage(sender, recipient, text, priority=priority))


if __name__ == '__main__':
	"""Run tests if executed directly"""
	import sys, doctest
	failed, count = doctest.testmod()
	print 'Ran', count, 'test cases with', failed, 'failures'
	sys.exit(failed)
