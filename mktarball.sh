#!/bin/bash
#
# $Id$
set -e

if [ "$1" == "--help" ]; then
	echo >&2 "
Create a source release tarball from the subversion repository.

USAGE

  mktarball.sh [REVISION] VERSION

REVISION specifies the subversion revision to be released. It defaults to
HEAD (latest). VERSION is the release version number, which is put into 
the name of the tarball and into a VERSION.txt file inside.
"
	exit 3
fi

if [ $# == 2 ]; then
	REVISION="$1"
	shift
else
	REVISION="HEAD"
fi

if [ -z "$1" ]; then
	echo "Version number argument not supplied"
	exit 1
fi

DIRNAME="eos-$1"
TARFILE="eos-$1.tgz"

rm -rf "${DIRNAME}" ${TARFILE}
trap "rm -rf ${DIRNAME}" 0 # cleanup on exit
mkdir "${DIRNAME}"
svn checkout --revision=${REVISION} https://eos-game.svn.sourceforge.net/svnroot/eos-game "${DIRNAME}"
rm -rf "${DIRNAME}/art" "${DIRNAME}/ideas" # Remove unneeded files
find "${DIRNAME}" -type d -name ".svn" | xargs rm -rf
tar zf "${TARFILE}" -c "${DIRNAME}"
echo "${TARFILE} created"

