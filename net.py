## Eos, Dawn of Light -- A Space Opera
## Copyright (c) 2007 Casey Duncan and contributors
## See LICENSE.txt for licensing details

# Multiplayer networking
# $Id$

import ai
import body
import game
import marshal
import projectile
import pygame
import random
import select
import socket
import SocketServer
import staticbody
import struct
import threading
import time
import vessel


class SocketCommunicator:

	control_fmt = '!l'
	control_size = struct.calcsize(control_fmt)
	buffer_size = 4096

	def __init__(self, latency, packet_loss):
		self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		self.socket.setblocking(False)
		self.received = []
		self.latency = latency
		self.packet_loss = packet_loss
		thread = threading.Thread(target=self._start)
		thread.setDaemon(True)
		thread.start()

	def send(self, address, *args):
		try:
			payload = marshal.dumps(args)
			data = struct.pack(self.control_fmt, len(payload)) + payload
			self.socket.sendto(data, address)
		except socket.error:
			pass

	def _start(self):
		try:
			while True:
				try:
					select.select([self.socket], [], [])
					data, address = self.socket.recvfrom(self.buffer_size)
					if random.random() < self.packet_loss:
						continue # drop a packet
					payload_size, = struct.unpack(self.control_fmt, data[:self.control_size])
					payload = marshal.loads(data[self.control_size:])
					if self.latency:
						# artificial latency
						until = time.time() + random.expovariate(1.0 / self.latency)
					else:
						until = None
					self.received.append((until, address, payload))
				except (socket.error, struct.error, EOFError, ValueError, TypeError):
					pass
		finally:
			self.socket.close()

class Server(SocketCommunicator):

	def __init__(self, host, port, latency, packet_loss):
		SocketCommunicator.__init__(self, latency, packet_loss)
		self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		self.socket.bind((host, port))
		self.server_generation = 0
		self.client_generation = {}
		self.addresses = set()

	def get_game_state(self):
		game_state = {}
		for object in game.objects:
			game_state[object.net_id] = object.get_state()
		return game_state

	def set_client_state(self, client_state):
		net_id, ship, keystate = client_state
		player = game.objects.get(net_id)
		if player is None:
			player = Vessel.load(ship, net_id=net_id)
			player.control = vessel.KeyboardControl()
			player.setup_collision(game.local_player.category<<2, body.nothing)
		player.control.set_keystate(keystate)

	def step(self):
		for address in self.addresses:
			self.send(address, self.server_generation, self.get_game_state())
		delayed = []
		while self.received:
			next = self.received.pop()
			until, address, (server_generation, client_generation, client_state) = next
			if until < time.time():
				self.addresses.add(address)
				if client_generation > self.client_generation.get(client_state[0]):
					self.set_client_state(client_state)
					self.client_generation[client_state[0]] = client_generation
			else:
				delayed.append(next)
		self.received.extend(delayed)
		self.server_generation += 1

class Client(SocketCommunicator):

	def __init__(self, host, port, latency, packet_loss):
		SocketCommunicator.__init__(self, latency, packet_loss)
		self.server = (host, port)
		self.client_generation = 0
		self.server_generation = None

	def create_object(self, object_state):
		if object_state['classname'] == 'Shot':
			create_func = projectile.Shot
		elif object_state['classname'] == 'ExplodingShot':
			create_func = projectile.ExplodingShot
		elif object_state['classname'] == 'PhaseDisruptorShot':
			create_func = projectile.PhaseDisruptorShot
		elif object_state['classname'] == 'Rocket':
			create_func = projectile.Rocket
		elif object_state['classname'] == 'Missile':
			create_func = projectile.Missile
		elif object_state['classname'] == 'Fullerene':
			create_func = projectile.Fullerene
		elif object_state['classname'] == 'FlakBomb':
			return None # TODO
			create_func = projectile.FlakBomb
		elif object_state['classname'] == 'AIVessel':
			create_func = ai.AIVessel.load
		elif object_state['classname'] == 'Vessel':
			create_func = vessel.Vessel.load
		elif object_state['classname'] == 'Planet':
			create_func = staticbody.Planet
		else:
			raise ValueError, `object_state['classname']`
		args = object_state.get('create_args', tuple())
		kw = object_state.get('create_kw', dict())
		return create_func(*args, **kw)

	def get_client_state(self, keystate):
		return game.local_player.vessel.net_id, game.local_player.vessel.vessel_class, keystate

	def set_game_state(self, game_state):
		# update objects
		for net_id, object_state in game_state.items():
			object = game.objects.get(net_id)
			if object is None:
				# create objects
				try:
					object = self.create_object(object_state)
				except AssertionError, e:
					# FIXME: flak shards reference their shooter, but there
					# shooter doesn't exist (since that's the original FlakBomb)
					print e
					object = None
			if object is not None:
				object.set_local_state(object_state)
		# destroy objects
		for object in game.objects:
			if object.net_id not in game_state:
				object.kill()

	def step(self, keystate):
		self.send(self.server, self.server_generation, self.client_generation, self.get_client_state(keystate))
		delayed = []
		while self.received:
			next = self.received.pop()
			until, address, (server_generation, game_state) = next
			if until < time.time():
				if server_generation > self.server_generation:
					self.set_game_state(game_state)
					self.server_generation = server_generation
			else:
				delayed.append(next)
		self.received.extend(delayed)
		self.client_generation += 1
