## Eos, Dawn of Light -- A Space Opera
## Copyright (c) 2007 Casey Duncan and contributors
## See LICENSE.txt for licensing details

# User interface panels
# $Id$

import glob
import pygame
from pygame.locals import *
import game
import display
import sprite
import vector
import camera
import event
import vessel
import widget
import sprite
import media
import selection
import scheduler

class Panel(sprite.Sprite, event.Handler):
	"""User interface panel. Panels contain controls. Enabled panels receive 
	events that occur inside them and dispatch the appropriate control actions.
	"""

	hotkey = None # Key to show/hide panel
	slide_time = 0.25
	border_color = (40, 40, 60, 150)
	background_color = (0, 0, 40, 150)
	update_interval = 300 # millis between panel updates

	def __init__(self, enabled=True, *controls):
		"""Create a panel and add it to the panel handler"""
		sprite.Sprite.__init__(self, sprite.layers.ui)
		self.image = self.create_image()
		self.enabled = enabled
		self.controls = pygame.sprite.Group(controls)
		# Map of hotkey => control for binding key presses to controls
		self.control_keymap = dict(
			(control.hotkey, control) for control in controls
			if control.hotkey is not None)
		self.clicked_control = None
		self.slide_end_frame = None
		handler.add_panel(self)
		sprite.layers.ui.to_back(self)
		scheduler.schedule(self.update_panel, sprite=self, recurrance=self.update_interval)
	
	def create_image(self):
		"""Return a panel image that is a background for the panel controls.
		This is intended to be overridden by subclasses
		"""
		return None

	def collides_with(self, pos):
		"""Return true if pos is inside the panel"""
		return self.rect.collidepoint(*pos)

	def control_at(self, pos):
		"""Return the control object at the postion specified or None if none"""
		for control in self.controls.sprites():
			if control.collides_with(pos):
				return control

	def update_panel(self):
		"""Panel-specific updates, called once per update_inteval. 
		This is intended to be overridden by subclasses
		"""

	def update(self):
		if self.slide_end_frame is not None and game.frame_no < self.slide_end_frame:
			self.slide()
		self.controls.update()
	
	def draw(self, surface):
		if self.slide_end_frame is not None and game.frame_no < self.slide_end_frame:
			self.slide()
		if self.image is not None:
			surface.blit(self.image, self.rect)
		self.controls.draw(surface)
		return self.rect
	
	def localize_event(self, event):
		"""Return a new event with coordinates local to the panel"""
		event_info = dict(event.dict) # copy event data into a new dict
		if 'pos' in event_info:
			gx, gy = event_info['pos']
			event_info['pos'] = (gx - self.rect.left, gy - self.rect.top)
		return pygame.event.Event(event.type, event_info)

	def mouse_move(self, event):
		"""Mouse movement. All panels receive all mouse movement events,
		so each panel needs to decide if the event is relevant to it
		"""
		event = self.localize_event(event)
		pos_before = (event.pos[0] - event.rel[0], event.pos[1] - event.rel[1])
		control_before = self.control_at(pos_before)
		control_now = self.control_at(event.pos)
		if control_now != control_before:
			if control_before is not None:
				control_before.mouse_leave(self, event)
			if control_now is not None:
				control_now.mouse_enter(self, event)
	
	def mouse_up(self, event):
		"""Handle mouse button release inside panel, return True if the
		panel handled the event, false if not
		"""
		event = self.localize_event(event)
		if self.clicked_control is not None: 
			if self.clicked_control is self.control_at(event.pos):
				self.clicked_control.mouse_up(self, event)
				self.clicked_control = None
			return True
		else:
			return False
	
	def mouse_down(self, event):
		"""Handle mouse button press inside panel, return True if the
		panel handled the event, false if not
		"""
		event = self.localize_event(event)
		control = self.control_at(event.pos)
		self.clicked_control = control
		if control is not None:
			control.mouse_down(self, event)
			return True
		else:
			return False

	def key_down(self, event):
		"""Key press. All panels receive all keypress events, so each
		panel must decide if the event is relevant to it. If the panel
		processes the key press, return True, else return False
		"""
		if event.key == self.hotkey:
			if not self.enabled:
				self.show()
			else:
				self.hide()
			return True
		elif self.enabled and event.key in self.control_keymap:
			control = self.control_keymap[event.key]
			return control.key_down(event)
		else:
			return False
	
	def show(self):
		self.enabled = True
	
	def hide():
		self.enabled = False
	
	def show_or_hide(self):
		if self.enabled:
			self.hide()
		else:
			self.show()
	
	def slide_to(self, pos):
		self.slide_dest = pygame.Rect(self.rect)
		self.slide_dest.left = pos[0]
		self.slide_dest.right = pos[1]
		frame_count = int(game.fps * self.slide_time)
		self.slide_vel = vector.vector2(
			float(pos[0] - self.rect[0]) / frame_count, float(pos[1] - self.rect[1]) / frame_count)
		self.slide_pos = vector.vector2(float(self.rect[0]), float(self.rect[1]))
		self.slide_end_frame = game.frame_no + frame_count
	
	def slide(self):
		old_pos = self.slide_pos
		self.slide_pos += self.slide_vel
		self.rect.left, self.rect.top = vector.to_tuple(self.slide_pos)
		# Create a simulated mouse move event and send it to the event handler
		# Even though we are moving, not the mouse, the effect is the same
		mm_event = pygame.event.Event(pygame.MOUSEMOTION,
			pos=pygame.mouse.get_pos(), buttons=pygame.mouse.get_pressed(),
			rel=vector.to_tuple(-self.slide_vel))
		self.mouse_move(mm_event)


class BottomPanel(Panel):
	"""Panel at bottom of screen with tabs"""

	border_frac = 0.015 # Fractional border width
	font_name = 'fonts/forgottenfuturist/Forgotti.ttf'
	title = ''
	tab = None

	def __init__(self, width, height, align_right=False, *controls):
		self.rect = pygame.Rect(0, 0, width, height)
		self.rect.top = display.rect.bottom
		self.align_right = align_right
		if align_right:
			self.rect.right = display.rect.right
		self.border_width = (self.rect.width + self.rect.height) / 2 * self.border_frac 
		Panel.__init__(self, enabled=False, *controls)
		
	def show(self):
		if not self.enabled:
			handler.hide_all(self.__class__) # Hide all other panels of this type
			rect = pygame.Rect(self.rect)
			rect.bottom = display.rect.bottom + 1
			self.slide_to(rect)
			self.enabled = True

	def hide(self):
		if self.enabled:
			rect = pygame.Rect(self.rect)
			rect.top = display.rect.bottom
			if self.align_right:
				rect.right = display.rect.right
			self.slide_to(rect)
			self.enabled = False

	def mouse_move(self, event):
		event = self.localize_event(event)
		pos_before = (event.pos[0] - event.rel[0], event.pos[1] - event.rel[1])
		control_before = self.control_at(pos_before)
		control_now = self.control_at(event.pos)
		if control_now != control_before:
			if control_before is not None:
				control_before.mouse_leave(self, event)
			if control_now is not None:
				control_now.mouse_enter(self, event)
	
	def mouse_up(self, event):
		"""Handle mouse button release inside panel, return True if the
		panel handled the event, false if not
		"""
		event = self.localize_event(event)
		if self.clicked_control is not None: 
			if self.clicked_control is self.control_at(event.pos):
				self.clicked_control.mouse_up(self, event)
				self.clicked_control = None
			return True
		else:
			return False
	
	def mouse_down(self, event):
		"""Handle mouse button press inside panel, return True if the
		panel handled the event, false if not
		"""
		event = self.localize_event(event)
		control = self.control_at(event.pos)
		self.clicked_control = control
		if control is not None:
			control.mouse_down(self, event)
			return True
		else:
			return False


class MiniMap(BottomPanel):
	"""Tactical mini-map display"""

	hotkey = K_m
	frac_width = 0.28
	title = 'Map'

	def __init__(self, map):
		width = display.rect.width * self.frac_width
		height = map.rect.height / (map.rect.width / width)
		self.map = map
		border_width = (width + height) / 2 * self.border_frac
		self.map_rect = pygame.Rect(border_width, border_width,
			width - border_width*2, height - border_width*2)
		self.map_scale = self.map_rect.width / float(map.rect.width)
		BottomPanel.__init__(self, width, height, align_right=True)
		self.image = pygame.Surface(self.rect.size, pygame.SRCALPHA, 32)
		self.tab = PanelTab(self, align_right=True)
		self.controls.add(self.tab)
		self.select_rect = None

	def create_image(self):
		image = pygame.Surface(self.rect.size, pygame.SRCALPHA, 32)
		image.fill(self.border_color)
		image.fill(self.background_color, self.map_rect)
		pygame.draw.rect(image, self.border_color, self.map_rect, 1)
		grid_size = int((camera.Camera.virtual_width) * self.map_scale)
		x = self.map_rect.left + grid_size
		while x < self.map_rect.right:
			pygame.draw.line(image, self.border_color, 
				(x, self.map_rect.top), (x, self.map_rect.bottom))
			x += grid_size
		y = self.map_rect.top + grid_size
		while y < self.map_rect.bottom:
			pygame.draw.line(image, self.border_color, 
				(self.map_rect.left, y), (self.map_rect.right, y))
			y += grid_size
		for planet in self.map.planets:
			pos_x, pos_y = vector.to_tuple(planet.position * self.map_scale)
			planet_img = media.image(planet.image_name, self.map_scale * 2, -vector.rightangle)
			planet_rect =  planet_img.get_rect(
				centerx=pos_x + self.map_rect.centerx, centery=pos_y + self.map_rect.centery)
			image.blit(planet_img, planet_rect)
		self.map_points = pygame.Surface(self.rect.size, pygame.SRCALPHA, 32)
		self.map_points.set_clip(
			self.map_points.get_rect().inflate(-self.border_width * 2, -self.border_width * 2))
		self.map_mask = pygame.Surface(self.rect.size, pygame.SRCALPHA, 32)
		self.map_mask.set_clip(
			self.map_mask.get_rect().inflate(-self.border_width * 2, -self.border_width * 2))
		image.set_alpha(0, pygame.RLEACCEL)
		self.background = image
		return image
	
	def update_panel(self):
		if self.enabled:
			self.map_points.fill((0, 0, 0, 0))
			self.map_mask.fill((0, 0, 0, 0))
			self.tab.rect.right = self.rect.right
			for vessel in self.map.vessels:
				if not vessel.incidental:
					if vessel.is_friendly(game.local_player.vessel):
						if vessel is game.local_player.vessel:
							color = (50, 50, 255)
						elif vessel.selected:
							color = (255, 255, 255)
						else:
							color = (0, 255, 0)
					else:
						color = (255, 0, 0)
					pos_x, pos_y = vector.to_tuple(vessel.position * self.map_scale)
					point_rect = pygame.Rect(
						pos_x + self.map_rect.centerx, pos_y + self.map_rect.centery, 2, 2)
					self.map_points.fill(color, point_rect)
					self.map_mask.fill((0, 0, 0, 120), point_rect.inflate(4, 4))
			self.image.fill((0, 0, 0, 0))
			self.image.blit(self.background, (0, 0))
			self.image.blit(self.map_mask, (0, 0))
			self.image.blit(self.map_points, (0, 0))
			self.image.set_alpha(0, pygame.RLEACCEL)
	
	def mouse_drag(self, event, start_pos, end_pos):
		if self.select_rect is None and not self.rect.collidepoint(start_pos):
			return False # Drag did not originate in our panel
		self.select_rect = pygame.Rect(
			min(start_pos[0], end_pos[0]), min(start_pos[1], end_pos[1]),
			abs(start_pos[0] - end_pos[0]), abs(start_pos[1] - end_pos[1]))
		return True

	def mouse_drag_end(self, event, start_pos, end_pos):
		if self.select_rect is not None:
			selection.group.select_none()
			# Translate the select rect to map coordinates
			(left, top), (width, height) = self.select_rect.topleft, self.select_rect.size
			rect = pygame.Rect((left - self.rect.centerx) / self.map_scale, 
				(top - self.rect.centery) / self.map_scale,
				width / self.map_scale, height / self.map_scale)
			selection.group.select_map_rect(game.local_player.category, rect)
			self.select_rect = None
			return True
	
	def mouse_click(self, event, pos):
		if event.button not in (1, 3):
			return False # We only handle left and right clicks
		if self.rect.collidepoint(pos):
			# Translate the position to map coodinates
			map_pos = ((pos[0] - self.rect.centerx) / self.map_scale,
				(pos[1] - self.rect.centery) / self.map_scale)
			planet_rect = pygame.Rect(0, 0, 0, 0)
			for planet in game.map.planets:
				planet_rect.width = planet_rect.height = planet.collision_radius * 4
				planet_rect.center = vector.to_tuple(planet.position)
				if planet_rect.collidepoint(map_pos):
					if event.button == 1:
						# Left-click, select
						game.target.select(planet)
						return True
					elif event.button == 3:
						# Right-click, direct
						selection.group.set_target(planet)
						return True
			if event.button == 3:
				# right-click in space, make a target marker and direct ships there
				marker = selection.Marker(vector.vector2(*map_pos))
				selection.group.set_target(marker)
			return True

	def draw(self, surface):
		old_clip = surface.get_clip()
		surface.set_clip(self.rect)
		surface.blit(self.image, self.rect)
		surface.set_clip(self.rect.inflate(-self.border_width * 2, -self.border_width * 2))
		if self.select_rect is not None:
			pygame.draw.rect(surface, (200, 100, 0), self.select_rect, 1)
		surface.set_clip(old_clip)
		return self.rect


class BasePanel(BottomPanel):
	"""Planetary base management ui"""

	base_keys = [K_1, K_2, K_3, K_4, K_5, K_6, K_7, K_8, K_9]

	frac_width = 0.60
	frac_height = 0.25
	frac_tab_margin = 0.036
	planet = None
	info_image = None
	planet_font_size = 32
	info_font_size = 22

	button_off_color = (0, 0, 0, 50)
	button_highlight_color = (100, 100, 160, 130)
	button_on_color = (100, 100, 160, 130)
	box_border_color = (50, 50, 70)

	vessel_button_order = {
		'fighter': 1,
		'assault transport': 2,
		'warship': 3,
	}

	def __init__(self, planet):
		self._last_builder = None
		self.current_builder_btn = None
		self.build_buttons = pygame.sprite.Group()
		self.queue_buttons = pygame.sprite.Group()
		self.queue_button_list = []
		self.planet_font = pygame.font.Font(
			self.font_name, int(self.planet_font_size * game.camera.base_scale))
		self.info_font = pygame.font.Font(
			self.font_name, int(self.info_font_size * game.camera.base_scale))
		BottomPanel.__init__(self, display.rect.width * self.frac_width,
			display.rect.height * (self.frac_height + self.frac_tab_margin))
		self.hotkey = self.base_keys.pop(0)
		self.set_planet(planet)
		self.buttons_dirty = True
	
	def create_image(self):
		image = pygame.Surface(self.rect.size, pygame.SRCALPHA, 32)
		image.fill(self.border_color)
		self.tab_margin = display.rect.height * self.frac_tab_margin
		self.info_rect = pygame.Rect(self.border_width, self.border_width, 
			self.rect.height - self.tab_margin - self.border_width * 2, 
			self.rect.height - self.tab_margin - self.border_width * 2)
		image.fill(self.background_color, self.info_rect)
		pygame.draw.rect(image, self.box_border_color, self.info_rect.inflate(2, 2), 1)
		self.build_rect = pygame.Rect(self.info_rect)
		self.build_rect.right = self.rect.width - self.border_width
		image.fill(self.background_color, self.build_rect)
		pygame.draw.rect(image, self.box_border_color, self.build_rect.inflate(2, 2), 1)
		self.btn_size = self.build_rect.width / 3
		self.queue_rect = pygame.Rect(self.info_rect.right + self.border_width,
			self.info_rect.top, 
			self.build_rect.left - self.info_rect.right - self.border_width * 2,
			self.info_rect.height)
		self.progress_rect = pygame.Rect(
			self.info_rect.right + self.btn_size + self.border_width * 3,
			self.rect.height / 4 + self.border_width,
			self.build_rect.left - self.info_rect.right - self.btn_size - self.border_width * 5,
			self.rect.height / 15)
		self.builder_rect = pygame.Rect(self.info_rect.right + self.border_width * 2,
			self.info_rect.top,
			self.build_rect.left - self.info_rect.right - self.border_width * 4,
			self.info_rect.height - self.btn_size + self.border_width * 2)
		#image.fill(self.background_color, self.builder_rect)
		#pygame.draw.rect(image, self.box_border_color, self.builder_rect.inflate(2, 2), 1)
		image.set_alpha(0, pygame.RLEACCEL)
		self.building_text = self.planet_font.render("Building...", True, (255, 255, 255))
		self.building_text.set_alpha(0, pygame.RLEACCEL)
		return image
	
	def create_buttons(self):
		"""Create button widgets for building vessels, etc. in the base"""
		self.controls.remove(self.build_buttons)
		self.controls.remove(self.queue_buttons)
		if self.current_builder_btn is not None:
			self.controls.remove(self.current_builder_btn)
		self.build_buttons.empty()
		self.queue_buttons.empty()
		self.queue_button_list = []
		if self.planet.base is not None:
			btn_rect = Rect(self.build_rect.left, self.build_rect.top, 
				self.btn_size, self.btn_size)
			configs = [vessel.get_config(path)
				for path in glob.glob('vessels/%s/*' % game.local_player.race)
				if self.planet.base.can_build_vessel(path)]
			configs.sort(key=lambda c: self.vessel_button_order.get(
				c[1].get('general', 'vessel_type')))
			for config_path, vessel_config in configs:
				button = self.vessel_button(
					btn_rect, vessel_config, self.planet.base.build_vessel, config_path)
				btn_rect = btn_rect.move(btn_rect.width, 0)
				if btn_rect.left > self.build_rect.right:
					btn_rect.left = self.build_rect.left
					btn_rect.top = btn_rect.bottom
				self.build_buttons.add(button)
				self.controls.add(button)
			btn_rect.left = self.builder_rect.left
			btn_rect.centery = self.progress_rect.centery
			self.current_builder_btn = self.vessel_button(
				btn_rect, None, self.planet.base.cancel_builder)
			queue_btn_size = self.progress_rect.width / self.planet.base.max_build_queue
			btn_rect = pygame.Rect(
				self.progress_rect.left, self.builder_rect.bottom - queue_btn_size,
				queue_btn_size - 1, queue_btn_size - 1)
			for i in range(self.planet.base.max_build_queue):
				button = self.vessel_button(
					btn_rect, None, self.planet.base.cancel_builder, i)
				self.queue_button_list.append(button)
				btn_rect = btn_rect.move(queue_btn_size, 0)
		self.render_buttons()
	
	def vessel_icon(self, vessel_config):
		"""Return the path to the vessel icon for the given vessel config"""
		if vessel_config.has_option('general', 'image_name'):
			return vessel_config.get('general', 'image_name')
		else:
			return '%s.png' % vessel_config.get('general', 'vessel_class')
	
	def vessel_button(self, rect, vessel_config, action, *action_args):
		"""Return a button object for the given vessel"""
		if vessel_config is not None:
			icon = self.vessel_icon(vessel_config)
		else:
			icon = None
		return widget.IconButton(icon, rect, 
			off_color=self.background_color,
			highlight_color=self.button_highlight_color,
			on_color=self.button_on_color,
			render_callback=self.render_buttons,
			action=action, action_args=action_args)
	
	def render_buttons(self):
		"""Refresh button sections of panel"""
		self.buttons_dirty = True

	def set_planet(self, planet):
		self.planet = planet
		self.info_image = image = pygame.Surface(self.info_rect.size, pygame.SRCALPHA, 32)
		rect = image.get_rect()
		desired_width = self.info_rect.width * 2.0
		planet_img = media.image(planet.image_name)
		planet_img = pygame.transform.rotozoom(planet_img,
			90, desired_width / max(planet_img.get_rect().size))
		image.blit(planet_img, (0, self.info_rect.height / 4))
		title_img = self.planet_font.render(planet.name, True, (255, 255, 255))
		title_rect = title_img.get_rect(left=4, top=rect.top + 3)
		if planet.base is not None:
			base_info = 'Base Level: %s' % planet.base.level
		else:
			base_info = 'No Base'
		image.blit(title_img, title_rect)
		info_img = self.info_font.render(base_info, True, (255, 255, 255))
		info_rect = info_img.get_rect(left=4, top=title_rect.bottom)
		image.blit(info_img, info_rect)
		info_img = self.info_font.render('Resources: %0.1f' % planet.resources, 
			True, (255, 255, 255))
		info_rect = info_img.get_rect(left=4, top=info_rect.bottom)
		image.blit(info_img, info_rect)
		image.set_alpha(0, pygame.RLEACCEL)
		self.create_buttons()
		self.title = self.planet.name
		self.tab = PanelTab(self)
		self.controls.add(self.tab)
	
	def update_panel(self):
		if self.planet.base is not None:
			if self.planet.base.builder is not None:
				# Planet is building something, update builder buttons
				self.current_builder_btn.set_icon(
					self.vessel_icon(self.planet.base.builder.vessel_config))
				if self.current_builder_btn not in self.queue_buttons:
					self.queue_buttons.add(self.current_builder_btn)
					self.buttons_dirty = True
				queue_size = len(self.planet.base.build_queue)
				for i, button in enumerate(self.queue_button_list):
					if i < queue_size:
						button.set_icon(
							self.vessel_icon(self.planet.base.build_queue[i].vessel_config))
						if button not in self.queue_buttons:
							self.queue_buttons.add(button)
							self.buttons_dirty = True
					elif button in self.controls:
						self.queue_buttons.remove(button)
						self.controls.remove(button)
						self.buttons_dirty = True
				self.controls.add(self.queue_buttons)
			else:
				# planet is not building anything, don't draw buttons
				if self.current_builder_btn in self.controls:
					self.queue_buttons.remove(self.current_builder_btn)
					self.controls.remove(self.current_builder_btn)
					self.buttons_dirty = True
					for button in self.queue_buttons:
						if button in self.controls:
							self.queue_buttons.remove(button)
							self.controls.remove(button)
							self.buttons_dirty = True
		if self.buttons_dirty and self.enabled:
			self.image.fill(self.background_color, self.build_rect) # Erase button area
			self.build_buttons.draw(self.image)
			self.image.fill(self.border_color, self.builder_rect.inflate(2, 2))
			self.queue_buttons.draw(self.image)
			if self.planet.base is not None and self.planet.base.builder is not None:
				self.image.blit(self.building_text, self.building_text.get_rect(
					bottom=self.progress_rect.top - self.progress_rect.height, 
					centerx=self.progress_rect.centerx))
				self.image.fill(self.background_color, self.progress_rect)
				pygame.draw.rect(
					self.image, self.box_border_color, self.progress_rect.inflate(2, 2), 1)
			self.image.set_alpha(0, pygame.RLEACCEL)
			self.buttons_dirty = False
	
	def draw(self, surface):
		surface.blit(self.image, self.rect)
		if self.info_image is not None:
			self.info_rect.top = self.rect.top + self.border_width
			surface.blit(self.info_image, self.info_rect)
		if self.planet.base is not None and self.planet.base.builder is not None:
			bar = pygame.Rect(self.progress_rect)
			bar.top += self.rect.top
			bar.width = int(bar.width * self.planet.base.builder.fraction_complete())
			surface.fill((200, 150, 0), bar)
		return self.rect


class PanelTab(widget.Button):
	"""Panel control tab"""

	tab_height_frac = .036
	panel_left = 0
	hotkey_offset = 3
	hotkey_color = (95, 95, 255, 100)
	hotkey_font = 'fonts/forgottenfuturist/Forgottb.ttf'

	def __init__(self, panel, align_right=False):
		self.panel = panel
		self.tab_font = pygame.font.Font(panel.font_name, 72)
		self.hotkey_font = pygame.font.Font(self.hotkey_font, 72)
		self.render()
		if not align_right:
			rect = self.image.get_rect(left=PanelTab.panel_left)
			PanelTab.panel_left = rect.right + 6
		else:
			rect = self.image.get_rect(right=display.rect.right)
		widget.Button.__init__(self, rect, self.panel.show_or_hide)
		sprite.layers.ui.add(self)

	def render(self):
		"""Create image for panel tab"""
		height = max(int(display.rect.height * self.tab_height_frac), 22)
		title_img = self.tab_font.render(self.panel.title, True, (255, 255, 255))
		title_img = pygame.transform.rotozoom(title_img, 0, 
			float(height - 5) / title_img.get_rect().height)
		width = max(title_img.get_rect().width, height * 2)
		self.image = tab_img = pygame.Surface((width + height + self.hotkey_offset, 
			height + self.hotkey_offset), pygame.SRCALPHA, 32)
		tab_img.fill(self.panel.border_color, 
			tab_img.get_rect().move(self.hotkey_offset, self.hotkey_offset))
		tab_img.blit(title_img, title_img.get_rect(
			centerx=width / 2 - 3 + height + self.hotkey_offset, top=4 + self.hotkey_offset))
		hotkey_rect = pygame.Rect(0, 0, height * 3 / 4, height * 3 / 4)
		hotkey_img = self.hotkey_font.render(chr(self.panel.hotkey), True, (0, 0, 0))
		hotkey_img = pygame.transform.rotozoom(hotkey_img, 0,
			float(hotkey_rect.height) / hotkey_img.get_rect().height)
		pygame.draw.ellipse(tab_img, self.hotkey_color, hotkey_rect)
		tab_img.blit(hotkey_img, hotkey_img.get_rect(centerx=hotkey_rect.centerx - 1))
		pygame.draw.ellipse(tab_img, (0, 0, 0, 0), hotkey_rect.inflate(4, 4), 2)
		tab_img.set_alpha(0, pygame.RLEACCEL)
	
	def draw(self, surface):
		self.rect.bottom = self.panel.rect.top
		surface.blit(self.image, self.rect)
		return self.rect

	def collides_with(self, pos):
		"""Return true if pos is inside the tab"""
		# pos is in panel local coordinates, but our rect is global
		x, y = pos
		return self.rect.collidepoint(x + self.panel.rect.left, y + self.panel.rect.top)


class PanelHandler(event.Handler):
	"""Event handler that delegates to the appropriate panel"""

	def __init__(self, panels=(), next_handler=None):
		self.panels = []
	
	def add_panel(self, panel):
		if panel not in self.panels:
			self.panels.append(panel)

	def hide_all(self, panel_class=None, force=False):
		"""Hide all panels of the specified class"""
		for panel in self.panels:
			if panel_class is None or isinstance(panel, panel_class):
				if force:
					panel.enabled = True
				panel.hide()

	def mouse_move(self, event):
		"""Handle mouse movement"""
		for panel in self.panels:
			panel.mouse_move(event)
	
	def mouse_up(self, event):
		"""Handle mouse button release"""
		for panel in self.panels:
			if panel.mouse_up(event):
				return panel
	
	def mouse_down(self, event):
		"""Handle mouse button press"""
		for panel in self.panels:
			if panel.mouse_down(event):
				return panel
	
	def mouse_click(self, event, pos):
		"""Handle click events"""
		for panel in self.panels:
			if panel.mouse_click(event, pos):
				return panel
	
	def mouse_drag(self, event, start_pos, end_pos):
		"""Handle drag events"""
		for panel in self.panels:
			if panel.mouse_drag(event, start_pos, end_pos):
				return panel

	def mouse_drag_end(self, event, start_pos, end_pos):
		"""Handle drag events"""
		for panel in self.panels:
			if panel.mouse_drag_end(event, start_pos, end_pos):
				return panel

	def key_down(self, event):
		"""Handle key press"""
		for panel in self.panels:
			if event.key == panel.hotkey:
				panel.show_or_hide()
				return panel
			if panel.enabled:
				if panel.key_down(event):
					return panel

handler = PanelHandler() # Singleton
