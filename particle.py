## Eos, Dawn of Light -- A Space Opera
## Copyright (c) 2007 Casey Duncan and contributors
## See LICENSE.txt for licensing details

# Particle Objects
# $Id$

import random
import pygame
from pygame.locals import *
import game
import body	
import vector
import sprite
from vector import halfcircle

max_particles = 500 # Max particles on-screen
particle_count = 0

class Particle(sprite.Sprite):
	key_color = (1, 1, 1)

	_image_cache = {}
	
	def __init__(self, position, velocity, accel_rate, color, 
		growth_rate, time_to_live, opacity=255):
		sprite.Sprite.__init__(self, sprite.layers.effects)
		self.position = position
		self.velocity = velocity / game.fps
		self.accel_rate = float(accel_rate) / game.fps + 1
		self.growth_rate = float(growth_rate) / game.fps
		self.color = color
		self.apparent_size, screen_pos = vector.to_screen(position)
		self.size = 2.0 * self.apparent_size
		self.time_to_live = time_to_live * 1000
		self.timeout = game.time + time_to_live * 1000
		self.opacity = self.image_alpha = opacity
		self.rect = pygame.Rect(0, 0, 0, 0)
		self.image = None
		self.set_image()
		self.offscreen_img = None
		global particle_count
		particle_count += 1
	
	def set_image(self):
		if self.size >= 0.1 or self.image is None:
			img_key = int(self.size), self.color
			if img_key in self._image_cache:
				self.image = self._image_cache[img_key]
			else:
				if self.size < 1.0:
					self.image_alpha = self.opacity * self.size * self.size
				else:
					self.image_alpha = min(self.opacity + self.opacity * (self.size % 1.0), 255)
				size = int(max(self.size, 1))
				self.image = pygame.Surface((size, size))
				self.image.fill(self.key_color)
				self.image.set_colorkey(self.key_color)
				pygame.draw.ellipse(self.image, self.color, self.image.get_rect())
				self._image_cache[img_key] = self.image
		else:
			self.image_alpha = 0
		self.rect = self.image.get_rect(center=self.rect.center)

	def update(self):
		self.position += self.velocity
		self.velocity *= self.accel_rate
		if random.random() <= self.growth_rate:
			self.size += self.apparent_size
			self.set_image()
		self.apparent_size, screen_pos = vector.to_screen(self.position)
		self.rect.center = vector.to_tuple(screen_pos)
		if game.time > self.timeout:
			self.kill()
	
	def draw(self, surface):
		alpha = (self.timeout - game.time) * self.image_alpha / self.time_to_live
		if alpha > 5:
			self.image.set_alpha(alpha)
			return surface.blit(self.image, self.rect)
		else:
			return self.rect
	
	def kill(self):
		global particle_count
		particle_count -= 1
		sprite.Sprite.kill(self)


class ParticleEmitter(sprite.Sprite):
	"""Emits particles from a body at a particular interval for a particular time"""

	def __init__(self, body, accel_rate, color, growth_rate,
		time_to_live, opacity, emit_rate, velocity_error=0.0, initial_size=0):
		sprite.Sprite.__init__(self, sprite.layers.effects)
		self.body = body
		self.velocity_error = velocity_error
		self.accel_rate = accel_rate
		self.color = color
		self.growth_rate = growth_rate
		self.time_to_live = time_to_live
		self.opacity = opacity
		self.emit_rate = float(emit_rate) / game.fps
		self.rect = pygame.Rect(0, 0, initial_size, initial_size)

	def update(self):
		global particle_count, max_particles
		alive = self.body.alive()
		if alive and self.body.on_screen:
			emit_prob = self.emit_rate
			velocity = self.initial_velocity()
			while random.random() < emit_prob and particle_count < max_particles:
				if self.velocity_error:
					velocity += (vector.unit(random.random() * vector.fullcircle) 
						* random.random() * self.velocity_error * vector.length(velocity))
				if not isinstance(self.color, tuple):
					color = self.color()
				else:
					color = self.color
				Particle(self.body.position, velocity, self.accel_rate, 
					color, self.growth_rate, self.time_to_live, self.opacity)
				emit_prob -= 1
		elif not alive:
			self.kill()
	
	def initial_velocity(self):
		return self.body.velocity
	
	def draw(self, surface):
		return self.rect


class ThrustEmitter(ParticleEmitter):
	"""Emits particles as thrust from the rear of the body"""

	def __init__(self, body, accel_rate, color, growth_rate,
		time_to_live, opacity, emit_rate, velocity_error=0.0, 
		initial_size=0, thrust_velocity=0):
		ParticleEmitter.__init__(self, body, accel_rate, color, growth_rate,
			time_to_live, opacity, emit_rate, velocity_error, initial_size)
		self.thrust_velocity = thrust_velocity

	def initial_velocity(self):
		return self.body.velocity + vector.unit(
			self.body.heading + halfcircle) * self.thrust_velocity


class GrayGenerator:
	"""Generates random gray colors"""

	def __init__(self, min_bright, max_bright):
		self.min_bright = min_bright
		self.max_bright = max_bright
	
	def __call__(self):
		bright = random.randint(self.min_bright, self.max_bright)
		return (bright, bright, bright)


class SmokeTrail(ParticleEmitter):
	
	def __init__(self, body):
		"""Smoke trail emitted from body"""
		ParticleEmitter.__init__(self,
			body,
			velocity_error=0.07,
			accel_rate=-0.6,
			color=(60, 60, 60),
			growth_rate=12,
			time_to_live=2.0,
			opacity=130,
			emit_rate=35)


class SparkTrail(ParticleEmitter):

	def __init__(self, body):
		"""Spark trail emitted from body"""
		ParticleEmitter.__init__(self,
			body,
			initial_size=1,
			growth_rate=0,
			velocity_error=0.0,
			accel_rate=-5.5,
			color=(255, 150, 100),
			time_to_live=0.25,
			opacity=255,
			emit_rate=60)

