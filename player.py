## Eos, Dawn of Light -- A Space Opera
## Copyright (c) 2007 Casey Duncan and contributors
## See LICENSE.txt for licensing details

# Player state
# $Id$

import glob
import random
import pygame
import game
import body
from vessel import Vessel, KeyboardControl
import ai
import selection

class Player:
	
	number = None # Player number
	race = None # Player race
	category = None # Player category bitmap
	vessel = None # Current player vessel
	control = None # Player's vessel controls
	bases = None # Bases owned by player


class HumanPlayer(Player):
	"""A player presumably controlled by a hunam"""

	def __init__(self, number, race, vessel_class):
		self.number = number
		self.race = race
		self.category = 2**number
		self.bases = pygame.sprite.Group()
		self.control = KeyboardControl()
		self.create_vessel(vessel_class)
	
	def create_vessel(self, vessel_class=None, net_id=None):
		"""Create a vessel for this player"""
		config = random.choice(glob.glob('vessels/%s/%s' % (self.race or '*', vessel_class or '*')))
		self.race = config.split('/')[1]
		self.vessel = Vessel.load(config, race=self.race, net_id=net_id)
		self.vessel.setup_collision(self.category, body.nothing)
		self.vessel.control = self.control
		return self.vessel
	
	def switch_vessel(self, vessel):
		"""Make the specified vessel the player's current vessel. If the player
		already has a current vessel, it is put under ai control
		"""
		if vessel.category == self.vessel.category and vessel is not self.vessel:
			if isinstance(self.vessel, Vessel):
				# Replace the player control with the appropriate ai control
				self.vessel.control = ai.control(
					self.vessel, target=game.target.selected.sprite, objective=vessel)
				self.vessel.setup_collision(self.category, body.everything & ~body.shot)
			else:
				# We don't currently have a real vessel, just an avatar
				self.vessel.kill()
			vessel.setup_collision(self.category, body.nothing)
			vessel.control = self.control
			vessel.layer.to_front(vessel)
			game.camera.follow(vessel)
			game.target.kill()
			game.target = selection.Target(vessel) # ick, make me better
			self.vessel = vessel

