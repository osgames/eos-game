## Eos, Dawn of Light -- A Space Opera
## Copyright (c) 2007 Casey Duncan and contributors
## See LICENSE.txt for licensing details

# Projectile weapons
# $Id$

import math
import pygame
from pygame.locals import *
import ode
import game
import random
import body
from body import RoundBody, everything
import vector
from vector import fullcircle, halfcircle, rightangle, diagonal
import vessel
import media
import ai
import particle
import sprite


class FullereneCannon(vessel.Weapon):

	name = 'fullerene cannon'
	priority = 0
	mass = 10

	def __init__(self, gunmount, charge_rate, min_charge, max_charge, max_charge_time, efficiency):
		"""Fullerene pulse cannon, a variable charge energy projectile

		gunmount -- body we are mounted to
		charge_rate -- Charge per second
		min_charge -- Minimum charge to fire, controls max fire rate
		max_charge -- Maximum charge
		max_charge_time -- Maximum seconds to hold before auto-fire
		"""
		self.charge_rate = float(charge_rate)
		self.min_charge = float(min_charge)
		self.max_charge = float(max_charge)
		self.max_charge_time = float(max_charge_time)
		self.efficiency = float(efficiency)
		self.charge_start = None
		self.shot = None
		self.gunmount = gunmount
		self.offset = self.gunmount.collision_radius * 1.6
	
	def update_system(self):
		if self.firing and self.shot is None:
			self.charge_start = game.time
			self.shot = Fullerene(self.gunmount, self.max_charge)
		if self.shot is not None:
			self.shot.set_position(self.gunmount.position 
				+ vector.unit(self.gunmount.heading) * self.offset)
			if self.firing:
				old_charge_cost = (self.shot.charge + 1)**2 / self.efficiency
				new_charge = min(self.shot.charge + self.charge_rate / game.fps, self.max_charge)
				new_charge_cost = (new_charge + 1)**2 / self.efficiency
				if self.gunmount.use_energy(new_charge_cost - old_charge_cost):
					self.shot.charge = new_charge
			elif self.shot.charge >= self.min_charge:
				self.shot.launch()
				self.shot = None
			else: # Not charged enough to fire
				self.shot.kill()
				self.shot = None
	
	@property
	def targeted(self):
		if not self.gunmount.control.target:
			return False
		else:
			target = self.gunmount.control.target.sprite
			if (target.category is None 
				or not target.category & everything & ~self.gunmount.category):
				return False
		target_dist = vector.distance(target.position, self.gunmount.position)
		if self.shot is not None:
			if (self.shot.charge >= self.max_charge / 1.5 
				and self.gunmount.bearing(target) < (diagonal / 4.0)
			    and target_dist < Fullerene.range * 2):
				return False
			else:
				return True
		else:
			return target_dist < Fullerene.range * 1.25


class Fullerene(RoundBody):

	speed = 600
	range = 600
	base_damage = 1.0
	layer = sprite.layers.effects
	exploding = False

	def __init__(self, gunmount, max_charge, net_id=None):
		self.radius = 1
		if gunmount is None:
			RoundBody.__init__(self, net_id=net_id)
		else:
			RoundBody.__init__(self, gunmount.position, net_id=net_id)
		self.gunmount = gunmount
		self.charge = 0
		self.max_charge = max_charge
		self.distance = 0
		self.animation = media.FullereneAnimation()

	def get_state(self):
		state = RoundBody.get_state(self)
		state['create_args'] = (self.charge, None)
		return state

	def update(self):
		# Position, velocity and heading are cached because they are frequently accessed
		self.position = vector.vector2(*self.body.getPosition()[:2])
		self.velocity = vector.vector2(*self.body.getLinearVel()[:2])
		self.heading = self.get_heading()
		self.radius = max(self.charge, 1)
		self.partsize = max(self.charge * .45, 1)
		self.apparent_size, screen_pos = vector.to_screen(self.position)

		if self.velocity and vector.distance(self.start_position, self.position) > self.range:
			self.kill()
		if not self.gunmount.alive() and not self.exploding:
			self.exploding = True
			self.animation = media.FullereneExplosion()
		image = self.animation.next_image(self.apparent_size * self.charge / self.max_charge)
		if image is not None:
			self.image = image
			self.rect = self.image.get_rect(center=vector.to_tuple(screen_pos))
		else:
			self.kill()
	
	def launch(self):
		self.set_velocity(vector.unit(self.gunmount.heading) * self.speed + self.gunmount.velocity)
		self.setup_collision(body.nothing, everything & ~self.gunmount.category)
		self.start_position = self.gunmount.position
		media.play_sound('fullerene.wav', position=self.position)

	def collide(self, other, contacts):
		if not self.exploding:
			self.exploding = True
			self.animation = media.FullereneExplosion()
			self.set_velocity(other.velocity)
			other.damage((self.charge + self.base_damage)**2)
			if other.explosion is None:
				media.play_sound('hit.wav', volume=(self.charge/5), position=self.position)


class Cannon(vessel.Weapon):
	
	priority = 0
	base_mass = 2
	mass_factor = 0.1 # Mass per (range / 100) + damage
	
	def __init__(self, gunmount, range, velocity, cool_down, image, damage,
			efficiency=None, radius=None, damage_loss=0.0, sound=None, shot='projectile.Shot',
			fade_range=None, mount_points=1, mount_positions=None, name=None):
		self.gunmount = gunmount
		self.range = float(range)
		self.velocity = float(velocity)
		self.cool_down = float(cool_down)
		self.damage_value = float(damage)
		self.efficiency = efficiency and float(efficiency)
		self.damage_loss = float(damage_loss)
		self.velocity = float(velocity)
		self.mass = self.base_mass + ((self.range / 100) + self.damage_value) * self.mass_factor
		self.last_fire = 0
		self.image_name = image
		if radius is None:
			img_rect = media.image(image).get_rect()
			self.radius = min(img_rect.width, img_rect.height) / 2.0
		else:
			self.radius = float(radius)
		self.shot_factory = vessel.resolve(shot)
		self.shot_sound = sound
		self.shot_fade_range = fade_range
		self.setup_mount_points(mount_points, mount_positions)
		self.ray = None
		if name is not None:
			self.name = name
	
	def setup_mount_points(self, count, positions):
		self.mount_points = int(count)
		if positions is not None:
			self.mount_positions = eval(positions)
			if len(self.mount_positions) != self.mount_points:
				raise vessel.VesselConfigError(
					"Value for mount_points (%s) doesn't match"
					" number of mount_positions configured (%s)"
					% (self.mount_points, len(self.mount_positions)))
		else:
			self.mount_positions = None
		self.next_position = 0

	def update_system(self):
		if self.firing:
			self.fire()
	
	def next_shot_position(self):
		"""Return the position that the next shot is fired from"""
		shot_position = self.gunmount.position
		if self.mount_positions is not None:
			angle, length = self.mount_positions[self.next_position]
			self.next_position = (self.next_position + 1) % self.mount_points
			shot_position += vector.unit(angle + self.gunmount.heading) * length
		return shot_position
	
	def fire(self):
		if (self.enabled and game.time - self.last_fire > self.cool_down
			and self.gunmount.use_energy(self.damage_value / self.efficiency)):
			self.shot_factory(
				shooter=self.gunmount,
				position=self.next_shot_position(),
				image=self.image_name,
				radius=self.radius,
				damage=self.damage_value,
				velocity=vector.unit(self.gunmount.heading) * self.velocity,
				heading=self.gunmount.heading,
				range=self.range,
				fade_range=self.shot_fade_range,
				damage_loss=self.damage_loss)
			self.last_fire = game.time
			if self.shot_sound is not None:
				media.play_sound(self.shot_sound, position=self.gunmount.position)

	@property
	def targeted(self, slop=diagonal / 3):
		if self.ray is None:
			# Create a ray geom to determine beam collision
			self.ray = ode.GeomRay(game.collision_space, self.range * 1.1)
			self.ray.setCategoryBits(body.nothing)
			self.ray.setCollideBits(body.everything & ~self.gunmount.category)
			self.ray.parent = self
			self.ray.targeted = False
		self.ray.set(vector.to_tuple3(self.gunmount.position), 
			vector.to_tuple3(vector.unit(self.gunmount.heading)))
		if self.ray.targeted:
			self.ray.targeted = False
			return True
		if not self.gunmount.control.target:
			return False
		else:
			target = self.gunmount.control.target.sprite
			if (target.category is None 
				or not target.category & everything & ~self.gunmount.category):
				return False
		if vector.distance(target.position, self.gunmount.position) > self.range:
			return False
		heading_diff = self.gunmount.heading - vector.radians(
			target.position - self.gunmount.position)
		if heading_diff > halfcircle:
			heading_diff -= fullcircle
		elif heading_diff < -halfcircle:
			heading_diff += fullcircle
		return abs(heading_diff) < slop
	
	def collide(self, other, contact):
		self.ray.targeted = True


class SpreadCannon(Cannon):
	"""Cannon that shoots multiple shots in a triangular spread"""

	base_mass = 3

	def __init__(self, gunmount, range, velocity, cool_down, image, shot_damage, shot_count,
		spread_angle, capacity, regeneration_time, radius=None, damage_loss=0.0, sound=None, 
		shot='projectile.Shot', fade_range=None, mount_points=1, mount_positions=None, name=None):
		Cannon.__init__(self, gunmount, range, velocity, cool_down, image, shot_damage,
			radius=radius, damage_loss=damage_loss, sound=sound, shot=shot, fade_range=fade_range,
			mount_points=mount_points, mount_positions=mount_positions, name=name)
		self.shot_count = int(shot_count)
		self.spread_angle = float(spread_angle) / 360.0 * vector.fullcircle
		self.max_capacity = self.remaining = int(capacity)
		self.regen_time = float(regeneration_time) * 1000
		self.last_fire = 0
		self.next_regen = None

	def update_system(self):
		if self.remaining < self.max_capacity:
			if self.next_regen is not None:
				if game.time > self.next_regen:
					self.remaining += 1
					self.next_regen = game.time + self.regen_time
			else:
				self.next_regen = game.time + self.regen_time
		else:
			self.next_regen = None
		Cannon.update_system(self)

	def fire(self):
		if self.enabled and self.remaining and game.time - self.last_fire > self.cool_down:
			angle = self.gunmount.heading - (self.spread_angle / 2)
			increment = self.spread_angle / self.shot_count
			for i in range(self.shot_count):
				shot = self.shot_factory(
					shooter=self.gunmount,
					position=self.next_shot_position(),
					heading=angle,
					image=self.image_name,
					radius=self.radius,
					damage=self.damage_value,
					velocity=vector.unit(angle) * self.velocity,
					range=self.range,
					fade_range=self.shot_fade_range,
					damage_loss=self.damage_loss)
				particle.SparkTrail(shot)
				angle += increment
			self.last_fire = game.time
			if self.shot_sound is not None:
				media.play_sound(self.shot_sound, position=self.gunmount.position)


class Shot(RoundBody):
	"""Generic projectile shot"""

	layer = sprite.layers.effects

	def __init__(self, shooter, image, damage, velocity, position=None, heading=None,
		range=None, max_ttl=None, fade_range=None, damage_loss=0, radius=1, mass=0.1, net_id=None):
		"""Generic projectile shot

		shooter -- body that fired the shot
		image -- shot sprite image
		damage -- damage value on hit
		velocity -- velocity vector of shot (not including shooter velocity)
		heading -- initial shot heading
		range -- distance shot travels before disappearing
		max_ttl -- Maximum time before shot is gone (alternative to range)
		fade_range -- range when shot image begins to fade (cosmetic)
		damage_loss -- Reduction in damage at max range
		radius -- shot collision radius
		"""
		if type(shooter) is int:
			shooter_net_id = shooter
			shooter = game.objects.get(shooter_net_id)
			assert shooter is not None, 'Could not find shooter: %d' % shooter_net_id
		self.radius = radius
		self.mass = mass
		RoundBody.__init__(self, 
			position or shooter.position, shooter.velocity + velocity, heading,
			collides_with=everything & ~shooter.category,
			image_name=image, net_id=net_id)
		self.shooter = shooter
		self.start_position = position or shooter.position
		self.damage_value = self.start_damage = damage
		self.range = range
		if max_ttl is not None:
			self.max_time = game.time + max_ttl
		else:
			self.max_time = None
		self.fade_range = fade_range
		if damage_loss:
			# calculate damage loss per frame
			flight_time = range / vector.length(self.velocity)
			self.damage_loss = damage_loss / flight_time / game.fps
		else:
			self.damage_loss = 0

	def get_state(self):
		state = RoundBody.get_state(self)
		state['create_args'] = (self.shooter.net_id, self.image_name, self.start_damage, vector.vector2())
		return state
	
	def update(self):
		RoundBody.update(self)
		self.damage_value -= self.damage_loss
		if self.range is not None:
			distance = vector.distance(self.start_position, self.position)
			if distance > self.range:
				self.out_of_range()
			elif self.fade_range is not None and distance > self.fade_range:
				opacity = 255 * (self.range - distance) / (self.range - self.fade_range)
				self.image.set_alpha(int(opacity))
			else:
				self.image.set_alpha(255)
		if self.max_time is not None and game.time > self.max_time:
			self.out_of_range()

	out_of_range = RoundBody.kill

	def collide(self, other, contacts):
		if self.alive():
			self.kill()
			if self.shooter.is_friendly(other):
				# Reduce damage for friendly fire
				damage = self.damage_value / 3
			else:
				damage = self.damage_value
			if damage > 0:
				other.damage(damage)
				if other.explosion is None:
					media.play_sound('hit.wav', 
						volume=self.damage_value / self.start_damage, 
						position=self.position, min_spacing=0.25)


class ExplodingShot(Shot):
	"""Shot that explodes on collision"""

	xplode_animation = media.SmallExplosion

	def collide(self, other, contacts):
		if self.alive() and self.explosion is None:
			if self.damage_value > 0:
				self.explode('hit.wav')
				self.set_velocity(other.velocity)
				other.damage(self.damage_value)
			else:
				self.kill()


class PhaseDisruptorShot(Shot):
	"""Shot that disrupts shield regeneration"""

	def collide(self, other, contacts):
		if self.alive():
			other.damage(self.damage_value)
			if hasattr(other, 'shield'):
				other.shield.disrupt(0.5)


class FlakBomb(Shot):
	"""Flak bomb that explodes into shrapnel for secondary damage in an area
	"""
	shards = 50
	shard_velocity = 500
	shard_range = 60
	shard_damage = 3.0
	min_time = 0.6

	def __init__(self, shooter, bomb_bay, velocity, max_ttl):
		Shot.__init__(self, shooter,
			image='flak-bomb.png',
			damage=75, velocity=velocity, heading=vector.radians(velocity), range=750)
		self.max_time = game.time + max_ttl
		self.min_time = game.time + self.min_time * 1000
		media.play_sound('flak-bomb.wav', position=shooter.position)
		self.bomb_bay = bomb_bay
		self.detonating = False
		self.target = getattr(self.shooter.control.target, 'sprite', None)

	def get_state(self):
		state = RoundBody.get_state(self)
		state['create_args'] = (self.shooter.net_id, self.start_damage, vector.vector2())
		return state

	def update(self):
		Shot.update(self)
		if (not self.detonating and isinstance(self.shooter, ai.AIVessel) 
			and game.time > self.min_time):
			proximity = self.shard_range / 2
			self.detonating = (not self.target or
				vector.distance(self.position, self.target.position) < proximity
				or vector.distance(self.shooter.position, self.target.position) <
					vector.distance(self.shooter.position, self.position) + proximity)
		if game.time > self.max_time or self.detonating or (
			not self.bomb_bay.firing and game.time > self.min_time):
			self.detonate()
			self.kill()
	
	def collide(self, other, contacts):
		if self.alive():
			self.detonating = True
	
	def detonate(self):
		# denonate into shrapnel
		media.play_sound('explode.wav', position=self.position)
		for i in range(self.shards):
			shard_angle = random.random() * fullcircle
			Shot(self.shooter,
				image='flak-shard.png',
				heading=shard_angle,
				damage=self.shard_damage,
				position = self.position,
				velocity=vector.unit(shard_angle) * random.gauss(
					self.shard_velocity, self.shard_velocity / 3),
				range=self.shard_range,
				damage_loss=-30.0)


class MissileLauncher(Cannon):

	base_mass = 3
	priority = 1
	
	def __init__(self, gunmount, max_ttl, max_velocity, mass, thrust, cool_down, damage, radius,
		capacity, missile_factory, regeneration_time, launch_distance,
		image, sound=None, mount_points=1, mount_positions=None, arc_degrees=0, 
		max_turn_rate=None, length=None, name=None):
		self.gunmount = gunmount
		self.missile_max_ttl = float(max_ttl) * 1000
		self.max_velocity = float(max_velocity)
		self.cool_down = float(cool_down)
		self.missile_factory = vessel.resolve(missile_factory)
		self.damage_value = float(damage)
		self.mass = self.base_mass + (
			(self.missile_max_ttl / 100) + self.damage_value) * self.mass_factor
		self.missile_mass = float(mass)
		self.missile_thrust = float(thrust)
		self.missile_radius = float(radius)
		self.missile_length = length and float(length)
		self.missile_max_turn_rate = max_turn_rate and float(max_turn_rate)
		self.range = int(launch_distance) # for targetting
		self.max_capacity = self.remaining = float(capacity)
		self.last_fire = 0
		self.next_regen = None
		self.regen_time = float(regeneration_time) * 1000
		self.arc = float(arc_degrees) / 360.0 * vector.fullcircle
		self.missile_img = image
		self.sound = sound
		self.setup_mount_points(mount_points, mount_positions)
		self.ray = None
		if name is not None:
			self.name = name


	def update_system(self):
		if self.remaining < self.max_capacity:
			if self.next_regen is not None:
				if game.time > self.next_regen:
					self.remaining += 1
					self.next_regen = game.time + self.regen_time
			else:
				self.next_regen = game.time + self.regen_time
		else:
			self.next_regen = None
		Cannon.update_system(self)

	def fire(self):
		if self.enabled and self.remaining and game.time - self.last_fire > self.cool_down:
			target = self.gunmount.control.target.sprite
			position = self.next_shot_position()
			if self.arc > 0 and target:
				# Track the selected target
				track_angle = vector.radians(target.position - position) - self.gunmount.heading
				if track_angle > halfcircle:
					track_angle -= fullcircle
				elif track_angle < -halfcircle:
					track_angle += fullcircle
				if abs(track_angle) > self.arc:
					# limit angle to maximum arc
					track_angle /= track_angle / self.arc
			else:
				track_angle = 0

			self.missile_factory(
				shooter=self.gunmount,
				heading=self.gunmount.heading + track_angle,
				position=position,
				image=self.missile_img,
				damage=self.damage_value,
				max_velocity=self.max_velocity,
				mass=self.missile_mass,
				length=self.missile_length,
				thrust=self.missile_thrust,
				max_turn_rate=self.missile_max_turn_rate,
				radius=self.missile_radius,
				max_ttl=self.missile_max_ttl,
				target=target)
			self.last_fire = game.time
			self.remaining -= 1
			if self.sound is not None:
				media.play_sound(self.sound, position=self.gunmount.position)


class Rocket(ExplodingShot):
	"""Unguided, dumb self propelled rocket"""

	def __init__(self, shooter, heading, image, damage, mass, length, thrust, max_turn_rate,
		max_velocity, max_ttl, position=None, radius=1, target=None, net_id=None):
		ExplodingShot.__init__(
			self, shooter, image, damage, vector.vector2(), position, heading,
			max_ttl=max_ttl, radius=radius, mass=mass, net_id=net_id)
		self.max_velocity = float(max_velocity)
		self.thrust_vector = vector.unit(heading) * float(thrust) * 100

	def get_state(self):
		state = ExplodingShot.get_state(self)
		thrust = vector.length(self.thrust_vector) / 100
		state['create_args'] = (self.shooter.net_id, self.get_heading(), self.image_name, self.start_damage, self.mass, None, thrust, None, self.max_velocity, None)
		return state
	
	def update(self):
		ExplodingShot.update(self)
		if self.explosion is None and vector.length(self.get_velocity()) < self.max_velocity:
			self.push(self.thrust_vector)
	
	def out_of_range(self):
		self.explode('hit.wav')


class Missile(ExplodingShot):
	"""Guided, self propelled missile"""

	def __init__(self, shooter, heading, image, damage, mass, length, thrust, max_turn_rate,
		max_velocity, max_ttl, position=None, radius=1, target=None, net_id=None):
		ExplodingShot.__init__(
			self, shooter, image, damage, vector.vector2(), position, heading,
			max_ttl=max_ttl, radius=radius, mass=mass, net_id=net_id)
		self.target = pygame.sprite.GroupSingle(target)
		self.max_velocity = float(max_velocity)
		self.max_turn_rate = float(max_turn_rate)
		self.thrust = float(thrust) * 100
		self.tracker = ai.Tracker(target)
		self.exhaust = particle.ThrustEmitter(
			self,
			velocity_error=.25,
			accel_rate=-0.05,
			color=(240, 150, 0),
			initial_size=1,
			growth_rate=0.25,
			time_to_live=1.0,
			opacity=200,
			emit_rate=100,
			thrust_velocity=200.0)

	def get_state(self):
		state = ExplodingShot.get_state(self)
		state['create_args'] = (self.shooter.net_id, self.get_heading(), self.image_name, self.start_damage, self.mass, None, self.thrust / 100, self.max_turn_rate, self.max_velocity, None)
		return state

	def update(self):
		ExplodingShot.update(self)
		if self.explosion is None:
			tracking = self.tracker.update()
			turn = 0
			if tracking:
				target = self.target.sprite
				speed = vector.length(self.velocity)
				if speed:
					travel_time = vector.length(target.position - self.position) / speed
					target_ahead = self.tracker.predict(travel_time)
					self_ahead = self.position + self.velocity * (travel_time / 5)
				else:
					target_ahead = target.position
					self_ahead = self.position
				angle = vector.radians(target_ahead - self_ahead) - self.heading
				if angle > halfcircle:
					angle -= fullcircle
				elif angle < -halfcircle:
					angle += fullcircle
				self.turn_rate = max(min(angle * min(2 / travel_time, 4), self.max_turn_rate), 
					-self.max_turn_rate)
			self.push(vector.unit(self.heading) * self.thrust)
			if vector.length(self.velocity) > self.max_velocity:
				self.set_velocity(vector.clamp(self.velocity, self.max_velocity))

	def out_of_range(self):
		self.explode('hit.wav')
	
	def explode(self, sound):
		self.exhaust.kill()
		ExplodingShot.explode(self, sound)
