## Eos, Dawn of Light -- A Space Opera
## Copyright (c) 2007 Casey Duncan and contributors
## See LICENSE.txt for licensing details

# Periodic task scheduler
# $Id$

import sys
from heapq import heappush, heappop, heapreplace

# Initialize the task queue with a fake task that will always
# be after any scheduled task so we can always assume the task
# queue has at least one item in it.
_taskq = [(sys.maxint,)]

def schedule(callback, time=0, sprite=None, recurrance=None):
	"""Schedule the specified callback to be executed at the specified time.
	If recurrance is specified, the callback will be rescheduled to occur
	periodically every recurrance millis. 
	
	You can specify a time value of 0 if you want the task to be called
	immediately on the next call to dispatch. 

	If sprite is specified, then the callback will only be dispatched
	if the sprite is alive at the scheduled time.
	"""
	heappush(_taskq, (time, callback, sprite, recurrance))

def dispatch(time):
	"""Dispatch all scheduled items that are due to be executed on or
	before time.

	Any exceptions from a scheduled callback will be propagated, halting
	dispatching at that point. Callables not executed are left in the
	task queue.

	Note this method is not currently thread-safe, though it could be
	made so with a small loss in efficiency.

	>>> class Foo:
	...   one_calls = 0
	...   two_calls = 0
	...   def one(self):
	...     self.one_calls += 1
	...   def two(self):
	...     self.two_calls += 1
	>>> class Sprite:
	...   is_alive = True
	...   def alive(self):
	...     return self.is_alive
	>>> _clear()
	>>> o = Foo()
	>>> s = Sprite()
	>>> schedule(o.one, 2)
	>>> schedule(o.two, 3, s, 2)
	>>> dispatch(1)
	>>> o.one_calls
	0
	>>> o.two_calls
	0
	>>> dispatch(2)
	>>> o.one_calls
	1
	>>> o.two_calls
	0
	>>> dispatch(3)
	>>> o.one_calls
	1
	>>> o.two_calls
	1
	>>> dispatch(4)
	>>> o.one_calls
	1
	>>> o.two_calls
	1
	>>> dispatch(6)
	>>> o.one_calls
	1
	>>> o.two_calls
	2
	>>> dispatch(4000)
	>>> o.one_calls
	1
	>>> o.two_calls
	3
	>>> s.is_alive = False
	>>> dispatch(4100)
	>>> o.one_calls
	1
	>>> o.two_calls
	3
	"""
	while _taskq[0][0] <= time:
		nil, callback, sprite, recurrance = _taskq[0]
		if sprite is None or sprite.alive():
			if recurrance is not None:
				heapreplace(_taskq, (time + recurrance, callback, sprite, recurrance))
			else:
				heappop(_taskq)
			callback()
		else:
			heappop(_taskq)

def _clear():
	global _taskq
	_taskq = []

if __name__ == '__main__':
	"""Run tests if executed directly"""
	import sys, doctest
	failed, count = doctest.testmod()
	print 'Ran', count, 'test cases with', failed, 'failures'
	sys.exit(failed)
