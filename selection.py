## Eos, Dawn of Light -- A Space Opera
## Copyright (c) 2007 Casey Duncan and contributors
## See LICENSE.txt for licensing details

# Target and friend selection
# $Id$

import sys
import itertools
import ai
import game
import pygame
import media
import sprite
from vessel import Vessel
import vector
import body


class Target(sprite.Sprite):

	size_factor = 1.5
	color = (255, 255, 255)
	timeout = 2000
	sensor = None

	def __init__(self, source_vessel, range=2000):
		sprite.Sprite.__init__(self, sprite.layers.ui)
		self.selected = pygame.sprite.GroupSingle()
		self._select_timeout = sys.maxint
		self._sensor_off_frame = None
		self.rect = pygame.rect.Rect(0, 0, 0, 0)
		self._nearest_vessel_iter = None
		self._planet_iter = None
		self.sensor = ai.Sensor(source_vessel, range)
		self.sensor.disable()
	
	@property
	def target(self):
		"""Return the object targeted or None"""
		return self.selected.sprite
	
	def select(self, object):
		"""Replace the current targeted object"""
		if object is not self.selected.sprite:
			media.play_sound('select.wav')
			self.selected.add(object)
			self._select_timeout = game.time + self.timeout
	
	def select_nearest(self, category, cycle=True):
		"""Select the nearest body that is in the category."""
		if self._nearest_vessel_iter is not None and self._last_category == category:
			try:
				while 1:
					next = self._nearest_vessel_iter.next()
					if next is not self.selected.sprite and not next.incidental:
						# Ensure we always select a new target
						self.select(next)
						break
				return
			except StopIteration:
				if cycle:
					self._nearest_vessel_iter = (body for body in self.sensor.detected 
						if body.alive())
					nearest = self.select_nearest(category, cycle=False)
				else:
					self._nearest_vessel_iter = None
		self.sensor.setDetect(category)
		self._last_category = category
		# Enable the sensor for the next few frames
		self._sensor_off_frame = game.frame_no + 3
		self.sensor.enable()
	
	def select_next_planet(self):
		def distance(planet):
			return vector.distance(planet.position, game.camera.position)
		if self._planet_iter is None:
			self._planet_iter = itertools.cycle(sorted(game.map.planets, key=distance))
		self.sensor.disable()
		self.select(self._planet_iter.next())
	
	def update(self):
		if game.time > self._select_timeout:
			# forget about all previously selected targets
			self._nearest_vessel_iter = None
			self._planet_iter = None
			self._select_timeout = sys.maxint
		if self.sensor.enabled:
			if self.sensor.closest_vessel:
				# See what our sensor detected
				self._nearest_vessel_iter = (body for body in self.sensor.detected if body.alive())
				self.select_nearest(self._last_category)
				self.sensor.disable() # Stop detection
			elif game.frame_no > self._sensor_off_frame:
				self.sensor.disable() # Stop detection
	
	def draw(self, surface):
		target = self.target
		rect = self.rect
		if target is not None:
			# To avoid lagging behind the target, we update our rect here
			# which guarantees that the target's rect is already updated
			width = target.collision_radius * 3 * target.apparent_size
			if width <= max(target.rect.size):
				# compensate for smaller collision radii
				width *= 2
			width = max(width, 3)
			rect.center = target.rect.center
			rect.size = (width, width)
			line_len = min(max(self.rect.width / 10, 3), 75)
			pygame.draw.line(surface, self.color, 
				(rect.left, rect.centery), (rect.left + line_len, rect.centery))
			pygame.draw.line(surface, self.color, 
				(rect.centerx, rect.top), (rect.centerx, rect.top + line_len))
			pygame.draw.line(surface, self.color, 
				(rect.right, rect.centery), (rect.right - line_len, rect.centery))
			pygame.draw.line(surface, self.color, 
				(rect.centerx, rect.bottom), (rect.centerx, rect.bottom - line_len))
		elif rect:
			rect.size = 0, 0
		return rect


class GroupSelector:

	timeout = 2000
	initial_select_size = 500

	def __init__(self):
		self.selected = pygame.sprite.Group()
		self.by_class_iter = None
		self._select_timeout = 0
		self._last_select_size = 0
	
	def select_none(self, clear_class_iter=True):
		"""De-select all selected vessels"""
		for vessel in self.selected:
			vessel.selected = False
		self.selected.empty()
		if clear_class_iter:
			self.by_class_iter = None
	
	def select(self, vessel):
		"""Select a single vessel"""
		self.select_none()
		self.selected.add(vessel)
		vessel.selected = True
	
	def select_screen_rect(self, category, rect):
		"""Select all vessels matching the category inside the given rect in screen coordinates"""
		in_rect = rect.colliderect
		by_class = {}
		for spr in sprite.layers.vessels:
			if (isinstance(spr, Vessel) and spr.category & category and in_rect(spr.rect)
				and spr is not game.local_player.vessel and not spr.incidental):
				self.selected.add(spr)
				if spr.vessel_class not in by_class:
					by_class[spr.vessel_class] = pygame.sprite.Group()
				by_class[spr.vessel_class].add(spr)
				spr.selected = True
		if by_class:
			self.by_class_iter = itertools.cycle(by_class.iteritems())
		else:
			self.by_class = None
	
	def select_map_rect(self, category, rect):
		"""Select all vessels matching the category inside the given rect in map coordinates"""
		in_rect = rect.collidepoint
		by_class = {}
		for spr in sprite.layers.vessels:
			if (isinstance(spr, Vessel) and spr.category & category 
				and in_rect(vector.to_tuple(spr.position))
				and spr is not game.local_player.vessel and not spr.incidental):
				self.selected.add(spr)
				if spr.vessel_class not in by_class:
					by_class[spr.vessel_class] = pygame.sprite.Group()
				by_class[spr.vessel_class].add(spr)
				spr.selected = True
		if by_class:
			self.by_class_iter = itertools.cycle(by_class.iteritems())
		else:
			self.by_class = None
	
	def select_near_vessel(self, vessel, category):
		"""Select vessels in an expanding area surround the vessel"""
		if game.time > self._select_timeout:
			self._last_select_size = 0
			self._select_timeout = game.time + self.timeout
			self.select_none()
		size = self._last_select_size = self._last_select_size*2 or self.initial_select_size
		size += vessel.collision_radius * 2
		rect = pygame.Rect(0, 0, size, size)
		rect.center = vector.to_tuple(vessel.position)
		self.select_map_rect(category, rect)
	
	def constrain_by_class(self):
		"""Constrain an existing selection to a single vessel class. Repeated
		calls cycle through all currently selected vessel classes
		"""
		if self.by_class_iter is not None:
			vessel_class, vessels = self.by_class_iter.next()
			self.select_none(clear_class_iter=False)
			for vessel in vessels:
				vessel.selected = True
				self.selected.add(vessel)
	
	def set_target(self, target):
		"""Set the target and objective for all selected vessels.
		"""
		for vessel in self.selected:
			vessel.control.set_target(target)
			vessel.control.objective.add(target)

group = GroupSelector() # Singleton


class Marker(body.RoundBody):
	"""Position marker for targetting arbitrary positions in space"""

	rect = pygame.Rect(0, 0, 0, 0)
	velocity = vector.vector2()
	heading = 0
	collision_radius = 0

	def __init__(self, position):
		sprite.Sprite.__init__(self)
		self.position = position

	def draw(self):
		return self.rect

