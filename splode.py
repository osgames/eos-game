import sys
import pygame
from pygame.locals import *
import random
import time
import itertools

pygame.init()
screen = pygame.display.set_mode((800,600))

class Particle:

	def __init__(self, color, pos, vel=(0,0), drag=0, initial_size=1, initial_expand=0.1, accel=1.5, accel_time=8, deccel=1.05, deccel_time=80):
		self.velx, self.vely = vel
		self.x = float(pos[0])
		self.y = float(pos[1])
		self.drag = 1.0 - drag
		self.size = float(initial_size)
		self.expand = initial_expand
		self.rect = pygame.Rect(pos[0], pos[1], initial_size, initial_size)
		self.orig_color = color
		self.accel = accel
		self.accel_time = accel_time
		self.deccel = deccel
		self.deccel_time = deccel_time
	
	def update(self):
		width = min(int(self.size), 500)
		self.image = pygame.Surface((width, width), SRCALPHA, 32)
		self.x += self.velx
		self.y += self.vely
		self.rect = self.image.get_rect(centerx=self.x, centery=self.y)
		self.velx *= self.drag
		self.vely *= self.drag
		color = [max(min(int(c * self.expand), 255), 0) for c in self.orig_color]
		pygame.draw.circle(self.image, color, self.image.get_rect().center, int(width / 2))
		self.size += self.expand
		if self.accel_time:
			self.expand *= self.accel
			self.accel_time -= 1
		elif self.deccel_time:
			self.expand /= self.deccel
			self.deccel_time -= 1

class Particle:

	def __init__(self, color, pos, vel=(0,0), drag=0, initial_size=1, max_size=1, size_interval=2, fade_start=sys.maxint, fade_end=sys.maxint):
		self.velx, self.vely = vel
		self.x = float(pos[0])
		self.y = float(pos[1])
		self.drag = drag
		self.size = float(initial_size)
		self.max_size = float(max_size)
		self.size_interval = float(size_interval)
		self.fade_start = fade_start
		self.fade_end = fade_end
		self.frame = 0
		self.rect = pygame.Rect(pos[0], pos[1], initial_size, initial_size)
		self.color = list(color)
	
	def update(self):
		width = int(self.size)
		self.image = pygame.Surface((width, width), SRCALPHA, 32)
		self.x += self.velx
		self.y += self.vely
		self.rect = self.image.get_rect(centerx=self.x, centery=self.y)
		self.velx -= self.velx * self.drag
		self.vely -= self.vely * self.drag
		if self.frame < self.fade_start:
			color = [max(min(int(c), 255), 0) for c in self.color]
		else:
			fade = float(self.fade_end - self.frame) / float(self.fade_end - self.fade_start)
			color = [max(min(int(c * fade), 255), 0) for c in self.color]
		pygame.draw.circle(self.image, color, self.image.get_rect().center, int(width / 2))
		self.frame += 1
		if self.size < self.max_size:
			self.size += (self.max_size - self.size) / self.size_interval

particles = []
rect = pygame.Rect(0,0,0,0)


for i in range(50):
	red=random.randint(100,200)
	green=random.randint(50, red)
	blue=random.randint(50, green)
	p = Particle(
		color=(red, green, blue, 10),
		pos=(200,200), #(200 + random.gauss(40, 15), 200 + random.gauss(40, 15)),
		vel=(random.gauss(0, 4), random.gauss(0, 4)),
		drag=random.gauss(0.15, .01),
		max_size=random.gauss(150, 10),
		size_interval=random.uniform(35,80),
		fade_start=random.gauss(5,1),
		fade_end=100)
	particles.append(p)
	rect.union_ip(p.rect)

for i in range(250):
	red=random.randint(150,255)
	green=random.randint(20, red)
	blue=random.randint(0, green)
	p = Particle(
		color=(red, green, blue, 50),
		pos=(200,200), #(200 + random.gauss(40, 15), 200 + random.gauss(40, 15)),
		vel=(random.gauss(0, 4), random.gauss(0, 4)),
		drag=random.gauss(0.35, .02),
		max_size=random.gauss(40, 10),
		size_interval=random.uniform(5,70),
		fade_start=random.gauss(5,1),
		fade_end=70)
	particles.append(p)
	rect.union_ip(p.rect)


for i in range(500):
	bright=random.gauss(200,20)
	p = Particle(
		color=(bright, bright, bright, 150),
		pos=(200,200),
		vel=(random.gauss(0, 3), random.gauss(0, 3)),
		drag=0.1,
		initial_size=1,
		max_size=random.gauss(5, 1),
		size_interval=random.uniform(60,100),
		fade_start=random.gauss(15,12),
		fade_end=random.gauss(80,5))
	particles.append(p)
	rect.union_ip(p.rect)

for i in range(250):
	red=random.randint(150,255)
	green=random.randint(20, red)
	blue=random.randint(0, green)
	p = Particle(
		color=(red, green, blue, 50),
		pos=(200,200), #(200 + random.gauss(40, 15), 200 + random.gauss(40, 15)),
		vel=(random.gauss(0, 4), random.gauss(0, 4)),
		drag=random.gauss(0.35, .01),
		max_size=random.gauss(40, 10),
		size_interval=random.uniform(5,70),
		fade_start=random.gauss(5,1),
		fade_end=random.gauss(70, 10))
	particles.append(p)
	rect.union_ip(p.rect)

frames = []
slomo = False
for i in range(200):
	image = pygame.Surface(rect.size, SRCALPHA, 32)
	for p in particles:
		p.update()
		image.blit(p.image, p.rect)
		rect.union_ip(p.rect)
	frames.append(image)
	for event in pygame.event.get():
		if event.type == QUIT or (
			event.type == KEYDOWN and event.key == K_ESCAPE):
			raise SystemExit
screen_center = screen.get_rect().center
for image in itertools.cycle(frames):
	screen.fill((0, 0, 0))
	#image = pygame.transform.rotozoom(image, 0, .5)
	screen.blit(image, image.get_rect())
	pygame.display.flip()
	if slomo:
		time.sleep(0.1)
	for event in pygame.event.get():
		if event.type == QUIT or (
			event.type == KEYDOWN and event.key == K_ESCAPE):
			raise SystemExit
		if event.type == KEYDOWN and event.key == K_SPACE:
			slomo = not slomo
