## Eos, Dawn of Light -- A Space Opera
## Copyright (c) 2007 Casey Duncan and contributors
## See LICENSE.txt for licensing details

# Sprite extensions
# $Id$

import pygame
from pygame.sprite import OrderedUpdates


class Sprite(pygame.sprite.Sprite):
	"""Sprite that supports scaled images referenced by name"""

	draw = None # Override with a custom drawing method


class RenderedGroup(OrderedUpdates):
	"""A modified version of RenderedUpdates that can render sprites that do
	not have an image. If the sprite has no 'image' attribute then it's draw()
	method is called passing it the surface. The draw() method returns the 
	rect affected.

	Sprites added to a RenderedGroup are not drawn until the next full frame,
	thus avoiding bugs where sprites are added and drawn immediately without
	being appropriately updated first.
	"""
	def __init__(self, *sprites):
		OrderedUpdates.__init__(self, *sprites)
		# _activesprites is the list of sprites to draw this frame
		# note this is *never* mutated in place, since it may
		# need to be changed while being iterated.
		self._activesprites = []
		self._have_new = bool(sprites)

	def add_internal(self, sprite):
		OrderedUpdates.add_internal(self, sprite)
		self._have_new = True
	
	def remove_internal(self, sprite):
		OrderedUpdates.remove_internal(self, sprite)
		if not self._have_new or sprite in self._activesprites:
			self._activesprites = list(self._activesprites)
			self._activesprites.remove(sprite)
		
	def sprites(self):
		return self._activesprites

	def draw(self, surface):
		spritedict = self.spritedict
		surface_blit = surface.blit
		dirty = self.lostsprites
		self.lostsprites = []
		dirty_append = dirty.append
		for s in self.sprites():
			r = spritedict[s]
			if s.draw is not None:
				newrect = s.draw(surface)
			else:
				newrect = surface_blit(s.image, s.rect)
			if r is 0:
				dirty_append(newrect)
			else:
				if newrect.colliderect(r):
					dirty_append(newrect.union(r))
				else:
					dirty_append(newrect)
					dirty_append(r)
			spritedict[s] = newrect
		return dirty

	def update(self, *args):
		"""update(*args)
		   call update for all member sprites

		   calls the update method for all sprites in the group.
		   Passes all arguments on to the Sprite update function."""
		if self._have_new:
			# Incorporate new sprites
			#if self is layers.effects:
			#	print 'Incorporate!',  set(self._spritelist) - set(self._activesprites)
			self._activesprites = list(self._spritelist)
			self._have_new = False
		for s in self.sprites(): 
			s.update(*args)
	
	def to_front(self, sprite):
		"""Make the designated sprite the top-most sprite in the group
		(last to render).  sprite must already be in the group
		"""
		self._spritelist.remove(sprite)
		self._spritelist.append(sprite)
		if not self._have_new or sprite in self._activesprites:
			self._activesprites = list(self._activesprites)
			self._activesprites.remove(sprite)
			self._activesprites.append(sprite)
	
	def to_back(self, sprite):
		"""Make the designated sprite the bottom-most sprite in the group
		(first to render).  sprite must already be in the group
		"""
		self._spritelist.remove(sprite)
		self._spritelist.insert(0, sprite)
		if not self._have_new or sprite in self._activesprites:
			self._activesprites = list(self._activesprites)
			self._activesprites.remove(sprite)
			self._activesprites.insert(0, sprite)


class NetGroup(pygame.sprite.AbstractGroup):

	def __init__(self):
		pygame.sprite.AbstractGroup.__init__(self)
		self._by_net_id = {}

	def add_internal(self, sprite):
		pygame.sprite.AbstractGroup.add_internal(self, sprite)
		assert hasattr(sprite, 'net_id'), sprite.__class__.__name__
		self._by_net_id[sprite.net_id] = sprite

	def remove_internal(self, sprite):
		del self._by_net_id[sprite.net_id]
		pygame.sprite.AbstractGroup.remove_internal(self, sprite)

	def get(self, *args, **kw):
		return self._by_net_id.get(*args, **kw)


class SpriteLayers:
	"""Sprite layer manager"""

	def __init__(self):
		self.background = RenderedGroup() # Starfield, planets, etc.
		self.vessels = RenderedGroup() # Vessels
		self.effects = RenderedGroup() # Weapons fire, explosions, goodies
		self.ui = RenderedGroup() # User interface
	
	def update(self):
		"""Update all sprite layers"""
		self.ui.update()
		self.vessels.update()
		self.effects.update()
		self.background.update()
	
	def draw(self, surface):
		"""Draw all sprite layers"""
		self.background.draw(surface)
		self.vessels.draw(surface)
		self.effects.draw(surface)
		self.ui.draw(surface)
	
	def __len__(self):
		return (
			len(self.background) +
			len(self.vessels) +
			len(self.effects) +
			len(self.ui))

layers = SpriteLayers() # Singleton
