## Eos, Dawn of Light -- A Space Opera
## Copyright (c) 2007 Casey Duncan and contributors
## See LICENSE.txt for licensing details

# Starfield background
# $Id$

from math import sin
from random import randint, uniform, choice
import pygame
from pygame.constants import *
import game
import vector
import media
import subpixel
import camera
import sprite


class StarField(sprite.Sprite):
	"""Parallax starfield simulation"""

	debris_count = 20
	star_count = 300
	big_star_count = 30
	parallax_depths = 20
	parallax_factor = 0.95
	last_camera_pos = None

	def __init__(self, rect):
		sprite.Sprite.__init__(self, sprite.layers.background)
		self.set_rect(rect)
	
	def set_rect(self, rect):
		"""Setup starfield for the given rectangle"""
		self.rect = rect
		colors = [
			(200, 200, 255),
			(200, 255, 255),
			(255, 255, 200),
			(255, 255, 255),
			(255, 130, 130),
			(255, 255, 130),
			(130, 130, 255)]
		small_stars = self.make_stars(1, colors)
		self.stars = [
			[uniform(self.rect.left, self.rect.right),
			 uniform(self.rect.top, self.rect.bottom),
			 choice(small_stars)
			] for i in range(self.star_count + self.debris_count)
		]
		big_stars = self.make_stars(2, colors)
		for i in range(self.big_star_count):
			idx = randint(0, self.star_count - 1)
			self.stars[idx][2] = choice(big_stars)
		self.nebula_image = media.load_image('galaxy-milky-way.jpg')
		self.nebula_image = pygame.transform.rotozoom(
			self.nebula_image, 30.0, 
			float(rect.width) / float(camera.Camera.virtual_width))
		self.nebula_image.set_colorkey(self.nebula_image.get_at([0, 0]))
		self.nebula_image.set_alpha(255, pygame.RLEACCEL)
		rect = self.nebula_image.get_rect(center=rect.center)
		self.nebula_pos = [float(rect.left), float(rect.top)]
	
	@staticmethod
	def make_stars(size, colors):
		size = (size, size)
		stars = []
		for color in colors:
			image = pygame.Surface(size, SRCALPHA, 32)
			image.fill(color)
			stars.append(subpixel.SubPixelSurface(image))
		return stars

	def draw(self, surface):
		blit = surface.blit
		fill = surface.fill
		stars_per_layer = self.star_count / self.parallax_depths
		brightness = 255
		debris = self.debris_count
		debris_phase = game.time / 200.0
		left = self.star_count / self.parallax_depths
		surface.blit(self.nebula_image, self.nebula_pos)
		for x, y, star in self.stars:
			if debris > 0:
				brightness = sin(debris_phase + debris) * 100 + 155
				fill((brightness, brightness, brightness), (x, y, 1, 1))
				debris -= 1
			else:
				left -= 1
				if left == 0:
					brightness *= 0.95
					left = stars_per_layer
				img = star.at(x, y)
				img.set_alpha(brightness)
				blit(img, (x, y))
		return self.rect
	
	def move(self, vec):
		leftbound, topbound = self.rect.left, self.rect.top
		rightbound, botbound = self.rect.right, self.rect.bottom
		width, height = self.rect.width, self.rect.height
		speed = 0.25
		debris = self.debris_count
		left = self.star_count / self.parallax_depths
		for star_rect in self.stars:
			if debris > 0:
				star_rect[0] += vec[0] * (debris / 10.0 + 1.1)
				star_rect[1] += vec[1] * (debris / 10.0 + 1.1)
				debris -= 1
			else:
				left -= 1
				if left == 0:
					speed *= self.parallax_factor
					left = self.star_count / self.parallax_depths
				star_rect[0] += vec[0] * speed
				star_rect[1] += vec[1] * speed
			# If star moved offscreen, recycle it
			# into a new star on the opposite side
			if star_rect[0] < leftbound:
				star_rect[0] += width
				star_rect[1] = randint(topbound, botbound)
			elif star_rect[0] > rightbound:
				star_rect[0] -= width
				star_rect[1] = randint(topbound, botbound)
			elif star_rect[1] > botbound:
				star_rect[1] -= height
				star_rect[0] = randint(leftbound, rightbound)
			elif star_rect[1] < topbound:
				star_rect[1] += height
				star_rect[0] = randint(leftbound, rightbound)
		self.nebula_pos[0] += vec[0] * 0.15
		self.nebula_pos[1] += vec[1] * 0.15

	def update(self):
		if self.last_camera_pos is not None:
			star_velocity = game.camera.zoom * (self.last_camera_pos - game.camera.position)
			self.move(vector.to_tuple(star_velocity))
		self.last_camera_pos = game.camera.position
