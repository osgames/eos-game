## Eos, Dawn of Light -- A Space Opera
## Copyright (c) 2007 Casey Duncan and contributors
## See LICENSE.txt for licensing details

# Planets and other static bodies
# $Id$

import math
import pygame
from pygame.constants import *
import game
import display
import media
from body import RoundBody
import random
import vector
import sprite

class Planet(RoundBody):

	mass = 1000000
	base = None # Established planetary base
	layer = sprite.layers.background

	def __init__(self, name, image, x=0, y=0, resources=0, player=None, base_level=None, 
		max_base_level=5, net_id=None):
		RoundBody.__init__(self, position=(int(x), int(y)), heading=-vector.rightangle, 
			net_id=net_id)
		self.body.disable() # immobile
		self.name = name
		self.image_name = random.choice(image.split(','))
		self.collision_radius = self.radius = media.image(self.image_name).get_rect().width / 2
		self.resources = float(resources)
		self.update()
		if player is not None and base_level is not None:
			import base
			# XXX Hack, this needs to be better for real multiplayer
			if game.local_player.number == int(player):
				self.base = base.PlanetaryBase(
					self, game.local_player, int(base_level), int(max_base_level))

	def get_state(self):
		state = RoundBody.get_state(self)
		state['create_args'] = (self.name, self.image_name)
		return state
	
	def update(self):
		RoundBody.update(self)
		if self.base is not None:
			self.base.update()
		if self in game.target.selected:
			self.offscreen_img = 'pointy-planet-white'
		elif self.base is None:
			self.offscreen_img = 'pointy-planet-blue'
		elif self.base.owner.category & game.local_player.category:
			self.offscreen_img = 'pointy-planet-green'
		else:
			self.offscreen_img = 'pointy-planet-red'
			
