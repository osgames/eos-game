## Eos, Dawn of Light -- A Space Opera
## Copyright (c) 2007 Casey Duncan and contributors
## See LICENSE.txt for licensing details

# Space stations
# $Id$

import pygame
import math
import game
import vessel
import body
import staticbody
import vector
import media
import map
import message
import sprite
import ode


class Station(vessel.Vessel):
	"""Space station"""

	planet = None # Planet station is harvesting
	
	disable_factor = 0 # Stations are destroyed once their armor is breached
	build_time = 30
	status_bar_width = 70
	station_altitude = 150 # distance from planet surface

	def __init__(self, planet, orbit_angle, building=False, builder=None,
		vessel_type='station', **kw):
		if isinstance(planet, str):
			self.planet = game.map.get_planet(planet_name)
			if self.planet is None:
				raise map.MapConfigError('Cannot add station, planet "%s" not found in map')
		else:
			self.planet = planet
		vessel.Vessel.__init__(self, vessel_type=vessel_type, **kw)
		self.set_position(self.planet.position + vector.unit(orbit_angle) * (
			self.station_altitude + self.planet.radius))
		self.set_heading(vector.radians(self.planet.position - self.position))
		self.setup_collision(body.friend | body.foe, body.nothing)
		self.collision_radius = sum(self.vessel_img.get_rect().size) / 5
		self.builder = builder
		if building:
			self.build_end = int(game.time + self.build_time * 1000)
			self.defense_level = 0
			self.built = False
		else:
			self.build_end = 0
			self.defense_level = 1.0
			self.built = True
	
	def update(self):
		if game.time < self.build_end:
			build_millis = self.build_time * 1000.0
			self.defense_level = (build_millis - (self.build_end - game.time)) / build_millis
			self.show_status()
		else:
			self.defense_level = 1.0
			if not self.built:
				message.send_status(self, self.builder, 'Station built at %s' % self.planet.name)
				self.built = True
		vessel.Vessel.update(self)

	def use_energy(self, energy):
		"""Stations have unlimited energy"""
		return energy


class Armor(vessel.System):
	"""Station defensive armor
	
	Station armor takes 50% less damage from normal weapons, however it
	takes 150% damage from siege weapons.
	"""
	name = 'armor'

	def __init__(self, station, durability, regeneration):
		self.station = station
		self.durability = float(durability)
		self.damage_level = 0.0
		self.regeneration = float(regeneration)
	
	def update_system(self):
		if self.damage_level > 0:
			self.damage_level = max(self.damage_level - self.regeneration / game.fps, 0)
	
	def damage(self, damage, siege=False):
		if siege:
			damage = damage * 1.5
		else:
			damage = damage * 0.5
		self.damage_level += damage
		durability = self.durability * self.station.defense_level
		if self.damage_level > durability:
			return min(self.damage_level - durability, damage)
		else:
			return 0
	
	@property
	def health(self):
		return (self.durability * self.station.defense_level - self.damage_level) / self.durability


class StationPlacer(sprite.Sprite):
	"""Interface element for choosing a station site"""

	station_spacing = 0 # minimum gap between stations

	def __init__(self, image_name, planet):
		sprite.Sprite.__init__(self)
		self.image_name = image_name
		self.planet = planet
		self.distance = planet.radius + Station.station_altitude
		self.geom = ode.GeomSphere(
			game.collision_space, self.image_sra.get_rect().width / 2 + self.station_spacing)
		self.geom.parent = self # attach ourself for callback purposes
		self.geom.setCategoryBits(body.nothing)
		self.geom.setCollideBits(body.everything)
		self.collision_frame = 0
	
	def update(self):
		if vector.distance(game.local_player.vessel.position, self.planet.position) > self.distance:
			# Player is outside of station "orbit", cancel placement
			self.kill()
			return
		self.position = self.planet.position + vector.unit(
			game.local_player.vessel.heading) * self.distance
		self.geom.setPosition(vector.to_tuple3(self.position))
		self.heading = vector.radians(self.planet.position - self.position)
		apparent_size, screen_pos = vector.to_screen(self.position)
		self.image = media.image(self.image_name, apparent_size, self.heading)
		self.rect = self.image.get_rect(center=vector.to_tuple(screen_pos))

	def collide(self, other, contacts):
		if isinstance(other, Station) or isinstance(other, staticbody.Planet):
			self.collision_frame = game.frame_no + 1
	
	def can_build(self):
		"""Return true if this is a suitable spot for the station"""
		return self.collision_frame < game.frame_no


current_placer = pygame.sprite.GroupSingle()


class Conduit(sprite.Sprite):
	"""Naree resource conduit"""

	min_length = 300
	max_length = 3000
	width = 75

	# Cosmetic settings
	flow_speed = 4 # pixels / second
	arc_length = 8000
	arc_period = 31.0
	max_brightness = 100

	_src_image = None

	def __init__(self, source, target):
		sprite.Sprite.__init__(self)
		self.source = source
		self.target = target
		if self._src_image is None:
			self.create_image()
		self.offset = 0
		self.last_time = game.time
		sx, sy = vector.to_screen(source)
		tx, ty = vector.to_screen(target)
		distance = math.sqrt((tx - sx)**2 + (ty - sy)**2)
		self.w2mage = pygame.Surface((distance, target), self.width)
	
	@staticmethod
	def create_image():
		"""Generate conduit source image"""
		length = self.max_length + self.arc_period
		s = pygame.Surface((length, self.width), 0, 32)
		arc_rect = pygame.Rect(0, 0, self.arc_length, self.width)
		arc_rect.right = 0
		brightness = 0
		while arc_rect.left < self.length:
			brightness += self.max_brightness / self.arc_period
			if brightness > self.max_brightness:
				brightness = 0
			pygame.draw.arc(s, (brightness, brightness, 255), arc_rect, -math.pi/2, math.pi/2, 1)
			arc_rect.move_ip(1, 0)
		src_img = pygame.Surface((length, height), pygame.SRCALPHA, 32)
		r = src_img.get_rect()
		r.height = 1
		i = 0
		# Make a cylinder by blending the outer pixels more than the inner
		while r.top < height:
			s.set_alpha(math.sin(i * (math.pi / height))**2 * 180)
			src_img.blit(s, (0, i), r)
			r.move_ip(0, 1)
			i += 1
		src_img.set_alpha(255)
		# Cache in the class to use for all instances
		Conduit._src_image = src_img

