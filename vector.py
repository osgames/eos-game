## Eos, Dawn of Light -- A Space Opera
## Copyright (c) 2007 Casey Duncan and contributors
## See LICENSE.txt for licensing details

# 2D vectors using python complex numbers
# $Id$

from sys import maxint
import game
from math import atan2, log10, pi
import cmath

vector2 = complex

length = abs

def to_tuple(vector):
	return (vector.real, vector.imag)

def to_tuple3(vector, z=0):
	return (vector.real, vector.imag, z)

def radians(vector):
	return atan2(vector.imag, vector.real)

def unit(radians):
	return cmath.exp(radians * 1j)

def normal(vector):
	L = length(vector)
	if L == 0:
		return vector2()
	else:
		return vector / L

def clamp(vector, max_length):
	L = length(vector)
	if L > max_length:
		return vector * (max_length / L)
	else:
		return vector

def distance(vector1, vector2):
	return length(vector1 - vector2)

def to_screen(position):
	"""Given an absolute position vector in space return a tuple of
	(apparent_size, screen_location) where apparent_size is a
	float value where 0 < apparent_size <= 1.0 and screen_location
	is a vector of the cooresponding location on-screen.
	"""
	camera = game.camera
	from_camera = position - camera.position
	apparent_size = camera.zoom * log10(
		10.0 * camera.zoom_size_factor / (length(from_camera) + camera.zoom_size_factor) + 1.0)
	return apparent_size**2, camera.screen_pos + (
		from_camera * camera.zoom * apparent_size)

def to_map(screen_pos, slop=50, max_iterations=100):
	"""Return a vector of the approximate map coordinate for the given screen position.
	Note that without knowing the apparent size at that screen position, there is
	no unambiguous way to derive the exact map position.
	"""
	approx_pos = game.camera.position
	size, approx_screen_pos = to_screen(approx_pos)
	shift = (screen_pos - approx_screen_pos) * size
	last_error = distance(screen_pos, approx_screen_pos)
	while max_iterations > 0:
		last_pos = approx_pos
		approx_pos += shift
		size, approx_screen_pos = to_screen(approx_pos)
		error = distance(screen_pos, approx_screen_pos)
		if error < slop:
			return last_pos
		if error > last_error:
			shift = (screen_pos - approx_screen_pos) * size
			approx_pos = last_pos
		last_error = error
		max_iterations -= 1
	return last_pos

# angle aliases
fullcircle = pi * 2
halfcircle = pi
rightangle = pi / 2
diagonal = pi / 4

