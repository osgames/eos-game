## Eos, Dawn of Light -- A Space Opera
## Copyright (c) 2007 Casey Duncan and contributors
## See LICENSE.txt for licensing details

# Vessel classes
# $Id$

import os.path
import body
import ConfigParser
import game
import math
import pygame
import random
import sys
from pygame.locals import *
import media
import vector
from vector import fullcircle, halfcircle, rightangle, diagonal
import particle
import message
import sprite
import scheduler

max_weapons = 5
_empty_rect = pygame.rect.Rect(0, 0, 0, 0)

class Control:
	"""Static vessel control state"""

	def __init__(self):
		self.thrust = False
		self.fw_maneuver = False
		self.left_maneuver = False
		self.right_maneuver = False
		self.bw_maneuver = False
		self.turn = 0 # -1 = left, 0 = stop, 1 = right
		self.weapons = [False] * max_weapons
		self.target = None # body being targeted

	def update(self):
		"""Update control state"""
		pass


class KeyboardControl(Control):

	def set_keystate(self, keystate):
		self.thrust = keystate[K_UP]
		self.turn = keystate[K_RIGHT] - keystate[K_LEFT]
		self.fw_maneuver = keystate[K_w]
		self.left_maneuver = keystate[K_a]
		self.right_maneuver = keystate[K_d]
		self.bw_maneuver = keystate[K_s] or keystate[K_DOWN]
		self.weapons[0] = keystate[K_SPACE]
		self.weapons[1] = keystate[K_LSHIFT]
		self.target = game.target.selected

class VesselConfigError(Exception):
	"""Vessel configuration file error"""

_configs = {}

def get_config(config_file):
	"""Return a tuple of (file name, config parser) for a given config file
	which may be a file-path or file-like object
	"""
	global _configs
	if isinstance(config_file, str):
		# Load config file from disk and cache it
		if config_file in _configs:
			return _configs[config_file]
		parser = ConfigParser.SafeConfigParser()
		file = open(config_file)
		parser.readfp(file)
		_configs[config_file] = file.name, parser
		return file.name, parser
	else:
		# Parser file-like object as config, no caching
		parser = ConfigParser.SafeConfigParser()
		parser.readfp(config_file)
		return getattr(config_file, 'name', repr(config_file)), parser


class Vessel(body.RoundBody):
	"""A controlled body
	
	Vessels consist of a hull containing controls and systems which can react
	to the environment and relay information to other systems . The hull
	determines which and how many systems may be present.  Each frame the
	systems read the controls and can affect the vessel in some way or perform
	an action on behalf of the vessel.

	Systems perform functions like below:

	- Damage systems absorb damage. If the damage systems are depleted and
	  more damage is sustained, then other systems may be disabled or the ship
	  may be destroyed.
	- Maneuvering systems allow the ship to turn and move.
	- Weapon systems allow the ship to attack.
	- Cargo systems allow the ship to transport cargo.

	Systems that have a graphical representation should subclass Sprite.
	"""

	hull_damage = 0 # Current damage to hull
	disable_factor = 5 # disabled if damage exceeds this factor * hull_mass
	system_damage = 0 # System damage accumulator
	system_damage_threshold = 10 # How much damage to disable a system
	max_speed = 0
	max_energy = 0
	energy_storage_mass = 0.02 # Mass per energy unit stored
	damage_smoke = None
	status_bar_width = 37 # Width of status bar on-screen
	status_fade_time = 300 # status fade out/in speed
	status_timeout = 5000 # millis status remains visible
	system_repair_time = 15000 # millis to repair a damaged system
	selected = False

	race = None

	layer = sprite.layers.vessels

	control = Control()

	def __init__(self, vessel_name=None, vessel_class='', vessel_type='', description='',
	    image_name=None, hull_mass=1, hull_length=1, crew=0, max_speed=0, max_energy=0, 
		standoff_distance=0, race=None, ai=None, cost=None, config_file=None, net_id=None,
		incidental=False):
		if image_name is None and vessel_class:
			image_name = vessel_class + '.png'
		if image_name is not None:
			img_rect = media.image(image_name).get_rect()
			self.collision_radius = max(img_rect.size) / 3
		else:
			self.collision_radius = 0
		self.config_file = config_file
		self.hull_length = float(hull_length)
		self.radius = self.hull_length
		self.standoff_distance = float(standoff_distance)
		body.RoundBody.__init__(self, image_name=image_name, net_id=net_id)
		self.vessel_name = vessel_name
		self.vessel_class = vessel_class
		self.vessel_type = vessel_type
		self.description = description
		self.hull_mass = float(hull_mass)
		self.crew = int(crew)
		self.energy = self.max_energy = float(max_energy)
		self.last_status = None
		self.status_image = None
		self.status_time = 0
		self.max_speed = float(max_speed)
		self._sys = []
		self._damage_sys = []
		self.weapons = []
		self.set_heading(random.random() * fullcircle)
		self.damage_time = 0
		self.race = race
		self.ai_name = ai
		self.incidental = str(incidental) not in ('0', 'False')
		if game.map is not None:
			game.map.vessels.add(self)
		self._systems_to_repair = []

	@classmethod
	def load(cls, config_file, **kw):
		"""Create a vessel from a config file. config_file is either a readable
		file-like object or a file name. Additional keyword arguments are passed
		to the vessel constructor and override values of the same name in the
		config file.

		The config file is in ConfigParser format and contains a [general]
		section that defines the overall parameters for the vessel. These are 
		passed as arguments to the class constructor. Additional sections
		describe the vessel systems. The section names are in the format
		[modulename.SystemClass] for each system. The values under each
		system section are passed to the constructor for the class specified
		by the section name. Each instantiated system is then added to the
		vessel and the completed vessel is returned. See below for an example.

		>>> config = '''
		... [general]
		... vessel_class: ship
		... description: A test vessel
		... hull_mass: 42
		... hull_length: 3.1415926
		...
		... [vessel.Engine]
		... thrust: 5
		... '''
		>>> import StringIO
		>>> v = Vessel.load(StringIO.StringIO(config))
		>>> v.vessel_class
		'ship'
		>>> v.description
		'A test vessel'
		>>> v.hull_mass
		42.0
		>>> v.hull_length == 3.1415926
		True
		>>> s = list(v)
		>>> len(s)
		1
		>>> s[0].__class__.__name__
		'Engine'
		>>> s[0].thrust
		500.0
		"""
		file_name, config = get_config(config_file)
		params = dict(config.items('general'))
		params.update(kw)
		vessel = cls(config_file=config_file, **params)
		for section in config.sections():
			if section == 'general':
				continue
			if ':' in section:
				# Explicit section priority
				new_section, priority = section.split(':')
				try:
					priority = int(priority)
				except ValueError:
					raise VesselConfigError(
						'Non-integer priority value "%s" in section [%s]'
						' in vessel config file %s' % (priority, section, file_name))
				class_path = new_section.split('.')
			else:
				priority = None
				class_path = section.split('.')
			if len(class_path) != 2:
				raise VesselConfigError(
					'Invalid section name [%s] in vessel config file %s'
					% (section, file_name))
			mod_name, class_name = class_path
			if mod_name not in sys.modules:
				raise VesselConfigError(
					'Cannot find module %s for section [%s] in vessel config file %s. '
					'Check the module name and be sure the vessel.py module imports it.'
					% (mod_name, section, file_name))
			if not hasattr(sys.modules[mod_name], class_name):
				raise VesselConfigError(
					'Cannot find vessel system class %s in module %s for section [%s] in '
					'vessel config file %s' % (class_name, mod_name, section, file_name))
			sys_constructor = getattr(sys.modules[mod_name], class_name)
			try:
				params = dict(config.items(section))
				system = sys_constructor(vessel, **params)
			except Exception, err:
				raise VesselConfigError, (
					'Exception creating system from section [%s] of vessel config file %s: %s'
					% (section, file_name, err)), sys.exc_info()[-1]
			if priority is not None:
				system.priority = priority
			vessel.add_system(system)
		return vessel

	def get_state(self):
		state = body.RoundBody.get_state(self)
		state['create_kw']['config_file'] = self.config_file
		return state
	
	def update(self):
		body.RoundBody.update(self)
		if self.enabled:
			self.control.update()
			for weapon, ctrl_state in zip(self.weapons, self.control.weapons):
				weapon.firing = ctrl_state
			self.update_systems()
		if vector.length(self.velocity) > self.max_speed:
			# If we are overspeed, bleed off a little
			overspeed = vector.length(self.velocity) - self.max_speed
			if overspeed < self.max_speed / 5:
				# not much overspeed, just clamp it
				self.set_velocity(vector.clamp(self.velocity, self.max_speed))
			else:
				# very overspeed, clamp down quick
				self.set_velocity(vector.clamp(self.velocity, self.max_speed + overspeed / 2))
	
	def update_systems(self):
		for system in self._sys:
			system.update_system()
	
	def draw_status(self):
		"""Return an image for the vessel status"""
		bar_width = int(self.status_bar_width * game.camera.base_scale)
		bar_height = 3
		health = self.health
		health_bar_width = int(health * bar_width)
		if self.max_energy:
			energy_bar_width = int(self.energy * bar_width / self.max_energy)
		else:
			energy_bar_width = None
		if self.last_status != (health_bar_width, energy_bar_width):
			image = pygame.Surface((bar_width, bar_height * 2 + 2), 0, 8)
			image.fill((255, 255, 255))
			image.set_colorkey((255, 255, 255))
			# Health bar
			if health > 0.7:
				color = (0, 255, 0)
			elif health > 0.45:
				color = (255, 255, 0)
			elif health > 0.2:
				color = (255, 127, 0)
			else:
				color = (255, 0, 0)
			graph_rect = pygame.Rect(0, 0, bar_width, bar_height)
			graph_bar = pygame.Rect(graph_rect)
			graph_bar.width = health_bar_width
			image.fill((0, 0, 0), graph_rect)
			image.fill(color, graph_bar)
			pygame.draw.rect(image, (50, 50, 50), graph_rect, 1)
			if self.max_energy:
				# Energy Bar
				graph_rect.top = graph_rect.bottom + 1
				graph_bar.top = graph_bar.bottom + 1
				graph_bar.width = energy_bar_width
				image.fill((0, 0, 0), graph_rect)
				image.fill((50, 50, 255), graph_bar)
				pygame.draw.rect(image, (0, 0, 125), graph_rect, 1)
			self.status_image = image
			self.last_status = health_bar_width, energy_bar_width
		return self.status_image
				
	def draw(self, surface):
		rect = body.RoundBody.draw(self, surface)
		if self.selected and self.on_screen and self.apparent_size is not None:
			size = max(self.collision_radius * 4 * self.apparent_size, 10)
			rect = pygame.Rect(0, 0, size, size)
			rect.center = self.rect.center
			white = (255, 255, 255)
			segsize = rect.width / 4
			inset = rect.width / 11
			pygame.draw.aalines(surface, white, 0, 
				[(rect.left, rect.top+segsize), (rect.left+inset, rect.top+inset),
				 (rect.left+segsize, rect.top)], 0)
			pygame.draw.aalines(surface, white, 0, 
				[(rect.left, rect.bottom-segsize), (rect.left+inset, rect.bottom-inset),
				 (rect.left+segsize, rect.bottom)], 0)
			pygame.draw.aalines(surface, white, 0, 
				[(rect.right-segsize, rect.top), (rect.right-inset, rect.top+inset),
				 (rect.right, rect.top+segsize)], 0)
			pygame.draw.aalines(surface, white, 0, 
				[(rect.right-segsize, rect.bottom), (rect.right-inset, rect.bottom-inset),
				 (rect.right-inset-1, rect.bottom-inset+1), (rect.right, rect.bottom-segsize)], 0)
		if game.time <= self.status_time and self.explosion is None:
			status_image = self.draw_status()
			time = self.status_time - game.time
			if time < self.status_fade_time:
				# fading out
				opacity = 255 * time / self.status_fade_time
			elif time - self.status_timeout > self.status_fade_time:
				# fading in
				time -= self.status_timeout + self.status_fade_time
				opacity = 255 * (self.status_fade_time - time) / self.status_fade_time
			else:
				opacity = 255
			status_image.set_alpha(opacity)
			status_rect = status_image.get_rect(
				centerx=self.rect.centerx, top=self.rect.centery + self.collision_radius + 15)
			surface.blit(status_image, status_rect)
			return rect.union(status_rect)
		else:
			return rect
	
	def add_system(self, system):
		"""Add a vessel system

		>>> game.init()
		>>> class TestSys:
		...   mass = 5
		...   attr_name = 'test_sys'
		>>> v = Vessel()
		>>> v.mass = 1
		>>> len(list(v))
		0
		>>> s = TestSys()
		>>> v.add_system(s)
		>>> len(list(v))
		1
		>>> v.mass
		6.0
		>>> v.test_sys is s
		True
		"""
		if self.allow_system(system):
			self._sys.append(system)
			self.calc_mass()
			if hasattr(system, 'damage'):
				self._damage_sys.append(system)
				self._damage_sys.sort(key=lambda s: getattr(s, 'priority', sys.maxint))
			if isinstance(system, Weapon):
				self.weapons.append(system)
				self.weapons.sort(key=lambda s: getattr(s, 'priority', sys.maxint))
			if getattr(system, 'attr_name', None) is not None:
				if not hasattr(self, system.attr_name):
					# Add system as a vessel attribute for convenient access
					setattr(self, system.attr_name, system)
				else:
					assert False, 'Vessel already has attribute %s' % system.attr_name
	
	def allow_system(self, system):
		"""Return true if the hull can accomodate the system"""
		return system not in self
	
	def remove_system(self, system):
		"""Remove a vessel system
		>>> game.init()
		>>> class TestSys: mass = 10
		>>> sys = [TestSys(), TestSys()]
		>>> v = Vessel()
		>>> v.add_system(sys[0]); v.add_system(sys[1])
		>>> len(list(v))
		2
		>>> v.mass
		21.0
		>>> v.remove_system(sys[1])
		>>> len(list(v))
		1
		>>> v.mass
		11.0
		>>> v.remove_system(sys[0])
		>>> len(list(v))
		0
		>>> v.mass 
		1.0
		>>> v.remove_system(sys[0])
		Traceback (most recent call last):
			...
		ValueError: list.remove(x): x not in list
		"""
		self._sys.remove(system)
		self.calc_mass()
		if system in self._damage_sys:
			self._damage_sys.remove(system)
	
	def calc_mass(self):
		"""Calculate the vessel mass

		>>> game.init()
		>>> class TestSys: mass = 7
		>>> v = Vessel()
		>>> v.add_system(TestSys())
		>>> v.hull_mass = 5
		>>> v.calc_mass()
		"""
		self.mass = sum(s.mass for s in self._sys) + self.hull_mass
		self.mass += self.max_energy * self.energy_storage_mass
		massobj = self.body.getMass()
		massobj.adjust(self.mass)
		self.body.setMass(massobj)
	
	def set_scale(self, scale):
		"""Change the vessel scale, adjusting both the collision geom
		radius and image scaling factor
		"""
		self.scale = scale
		self.geom.setRadius(self.collision_radius * scale)
	
	def __iter__(self):
		return iter(self._sys)
	
	def disable(self):
		body.RoundBody.disable(self)
		for system in self:
			system.disable()
	
	def show_status(self):
		time = game.time
		if game.time < self.status_time:
			# already visible, no fade-in necessary
			time -= self.status_fade_time
		self.status_time = max(
			self.status_time, time + self.status_timeout + self.status_fade_time * 2)
	
	@property
	def status_shown(self):
		return game.time < self.status_time
	
	def damage(self, value):
		"""Apply damage to the vessel

		>>> game.init()
		>>> class DamageSys:
		...   mass = 0
		...   level = 0
		...   def damage(self, d):
		...     self.level += d / 2
		...     return d / 2
		...
		>>> d = DamageSys()
		>>> v = Vessel()
		>>> v.add_system(d)
		>>> v.damage(10)
		>>> d.level
		5
		>>> v.hull_damage
		5
		"""
		self.damage_time = game.time
		if not self.incidental:
			self.show_status()
		for s in self._damage_sys:
			value = s.damage(value)
			if not value:
				break
		if value:
			# Apply residual damage to hull
			self.hull_damage += value
			if self.hull_damage > self.disable_factor * self.hull_mass:
				if self.damage_smoke is not None:
					self.damage_smoke.kill()
				self.explode()
			else:
				self.system_damage += value
				while self.system_damage > self.system_damage_threshold:
					self.damage_system(random.choice(self._sys))
					self.system_damage -= self.system_damage_threshold
			if (self.hull_damage > self.disable_factor * self.hull_mass * 0.75 and 
				self.explosion is None):
				message.send_status(self, self, 'Severe Hull Damage', message.critical)
	
	def damage_system(self, system):
		"""Render a system inoperable due to damage and schedule repair"""
		system.disable()
		if not self._systems_to_repair:
			scheduler.schedule(self.repair_system, game.time + self.system_repair_time, self)
		self._systems_to_repair.append(system)
		message.send_status(self, self, '%s Damaged' % system.name.title(), message.important)
		if game.local_player.vessel is self:
			media.play_sound('power_down.wav', min_spacing=2)
		if self.damage_smoke is None and not self.incidental:
			self.damage_smoke = particle.SmokeTrail(self)

	def repair_system(self):
		"""Repair the earliest damaged system"""
		if self._systems_to_repair:
			system = self._systems_to_repair.pop()
			system.enable()
			message.send_status(
				self, self, '%s Repaired' % system.name.title(), message.notice)
			if self._systems_to_repair:
				scheduler.schedule(self.repair_system, game.time + self.system_repair_time, self)
			elif self.damage_smoke is not None:
				self.damage_smoke.kill()

	def use_energy(self, energy, partial=False):
		"""Consume vessel energy. Return the amount of energy that is available.
		If partial is true, return whatever energy is available up to the amount 
		specified, otherwise return all of what was specified or zero if that amount
		is not available.
		"""
		if self.energy >= energy:
			self.energy -= energy
			return energy
		elif partial:
			energy = self.energy
			self.energy = 0
			return energy
		else:
			return 0
	
	@property
	def health(self):
		"""Return our health rating between 0 and 1"""
		if self._damage_sys:
			damage_sys_healths = [system.health for system in self._damage_sys 
				if hasattr(system, 'health')]
			return sum(damage_sys_healths) / len(damage_sys_healths)
		else:
			return 0
	
	def explode(self, sound='explode.wav'):
		if self is game.local_player.vessel:
			game.local_player.vessel = EscapePod(self)
			game.camera.follow(game.local_player.vessel)
			media.play_sound('launch.wav')
		body.RoundBody.explode(self, sound)

	def kill(self):
		for system in self._sys:
			if hasattr(system, 'kill'):
				system.kill()
		body.RoundBody.kill(self)
	
	def bearing(self, other):
		"""Return the clockwise angle between the vessel heading and
		the position of the other vessel. Counter clockwise angles
		are negative

		>>> game.init()
		>>> v1 = Vessel()
		>>> v2 = Vessel()
		>>> v1.heading = 0
		>>> v2.position = Vector2D(1, 0)
		>>> v1.bearing(v2) == rightangle
		True
		>>> v1.heading = diagonal
		>>> round(v1.bearing(v2), 5) == round(diagonal, 5)
		True
		>>> v1.heading = rightangle
		>>> round(abs(v1.bearing(v2)), 5)
		0.0
		>>> v1.heading = halfcircle
		>>> v1.bearing(v2) == -rightangle
		True
		"""
		angle = vector.radians(other.position - self.position) - self.heading
		if angle > halfcircle:
			angle -= fullcircle
		elif angle < -halfcircle:
			angle += fullcircle
		return angle

	def __str__(self):
		return '<%s@%x "%s" at (%d, %d)>' % (
			(self.__class__.__name__, id(self), self.vessel_class) + vector.to_tuple(self.position))


class System:
	"""Vessel system base class"""

	name = 'base system'
	attr_name = None # Vessel attribute
	vessel = None
	mass = 0
	priority = sys.maxint
	enabled = True

	def update_system(self):
		"""Update system state, called once per frame"""
		pass # overridden in subclasses

	def disable(self):
		"""disable system

		>>> s = System()
		>>> s.enabled
		True
		>>> s.disable()
		>>> s.enabled
		False
		"""
		self.enabled = False
	
	def enable(self):
		"""enable system

		>>> s = System()
		>>> s.disable()
		>>> s.enabled
		False
		>>> s.enable()
		>>> s.enabled
		True
		"""
		self.enabled = True


class Weapon(System):
	"""Weapon base class."""

	# True if the weapon is trained on a suitable target
	targeted = False

	# True if weapon is being fired
	firing = False


class Shield(System, sprite.Sprite):
	"""Basic vessel shield system
	
	Shields have a current level, maximum level and regeneration rate.
	When shields are damaged, the damage value is deducted from the shield
	level. When the level reached zero, damage is no longer absorbed.
	The shield level recovers at the regeneration rate (per sec) as long
	as the shield system is not disabled.
	"""

	name = 'energy shield'
	attr_name = 'shield'
	mass = 0
	priority = 1

	regen_recovery = 5.0 # Time regeneration recovers after disruption

	# Cosmetic	
	opacity = 70 # 0-255
	flicker = 12 # variance in opacity
	fadeout = 200 # millis shields fade when turned off
	timeout = 1500 # millis shields stay active

	def __init__(self, vessel, max_level, regeneration, image=None):
		sprite.Sprite.__init__(self, vessel.layer)
		self.vessel = vessel
		self.max_level = self.level = float(max_level)
		self.regeneration = self.max_regeneration = float(regeneration)
		if image is not None:
			self.image_name = image
		else:
			self.image_name = None
		self.time = 0
		self.enabled = True
	
	def disable(self):
		self.level = 0
		self.enabled = False

	def show_shield(self):
		"""Show shields for timeout secs

		>>> game.init()
		>>> s = Shield(None, 0, 0)
		>>> s.time > game.time
		False
		>>> s.show_shield()
		>>> s.time > game.time
		True
		"""
		self.time = game.time + self.timeout

	def update_system(self):
		"""Update shield status and regenerate

		>>> game.init()
		>>> s = Shield(Vessel(), 10.0, 30.0)
		>>> s.level == s.max_level
		True
		>>> s.update_system()
		>>> s.level == s.max_level
		True

		>>> s.level = 0
		>>> s.update_system()
		>>> s.level
		1.0
		"""
		if self.vessel.status_shown:
			self.show_shield()
		if self.level < self.max_level and self.regeneration and self.enabled:
			# Regenerate shields
			new_level = min(
				self.level + self.regeneration / game.fps, self.max_level)
			energy = self.vessel.use_energy(new_level - self.level, partial=True)
			self.level += energy
		if self.regeneration < self.max_regeneration:
			# Recover regeneration rate if disrupted
			self.regeneration = min(
				self.regeneration + (self.max_regeneration / (game.fps * self.regen_recovery)), 
				self.max_regeneration)

	def draw(self, surface):
		if self.time >= game.time and self.level and self.image_name is not None:
			apparent_size, pos = vector.to_screen(self.vessel.position)
			image = media.image(self.image_name, apparent_size, math.radians(game.time / 30), 
				colorkey=True)
			flicker = int(self.flicker + self.flicker * (self.max_level / self.level))
			flicker = random.randint(0, flicker)
			opacity = self.opacity * (self.level / self.max_level / 2) + self.opacity
			if self.time - game.time <= self.fadeout:
				opacity *= (self.time - game.time) / self.fadeout
			image.set_alpha(max(opacity - flicker, 10))
			shield_rect = image.get_rect(center=self.vessel.rect.center)
			dirty = surface.blit(image, shield_rect)
			return dirty
		else:
			return _empty_rect

	def damage(self, value):
		"""Damage shields, return damage not absorbed
		
		>>> game.init()
		>>> s = Shield(None, 5, 0)
		>>> s.level
		5.0
		>>> s.damage(3)
		0
		>>> s.level
		2.0
		>>> s.damage(3)
		1.0
		>>> s.level
		0
		"""
		if value < self.level:
			self.level -= value
			self.show_shield()
			return 0
		else:
			value -= self.level
			self.level = 0
			return value
	
	@property
	def health(self):
		if self.max_level:
			return self.level / self.max_level
		else:
			return 0
	
	def disrupt(self, multiplier):
		"""Disrupt shield regeneration rate by multiplier"""
		self.regeneration *= multiplier


class DeflectorScreen(System):
	"""Deflector screen defensive system

	Deflector screens reduce damage to other systems in proportion to the
	energy level of the vessel. Once activated, the deflector draws 
	a steady amount of energy. Over time, as the energy level depletes,
	the effectiveness of the deflector screen decreases. The deflector
	also has an maximum damage protection per time over which it does
	not reduce damage further.
	"""

	name = 'deflector screen'
	attr_name = 'shield'
	efficiency_recovery = 4.0
	priority = 0 # Screens get hit first
	
	def __init__(self, vessel, energy_drain, max_protection, deactivation_time, image=None):
		self.vessel = vessel
		self.energy_drain = float(energy_drain)
		self.efficiency = 1.0
		self.max_protection = self.protection = float(max_protection)
		self.deactivation_time = float(deactivation_time) * 1000
		self.last_activation = 0
		self.activated = False
		self.no_deflector_img = self.vessel.image_name
		if image is not None:
			self.deflector_img = image
		else:
			self.deflector_img = None
	
	def update_system(self):
		if self.enabled:
			self.activated = self.activated and (
				game.time - self.last_activation < self.deactivation_time)
			if self.activated:
				self.vessel.use_energy(
					self.energy_drain / (self.efficiency * game.fps), partial=True)
				if self.deflector_img is not None:
					self.vessel.image_name = self.deflector_img
			else:
				self.vessel.image_name = self.no_deflector_img
			if self.efficiency < 1.0:
				self.efficiency += min(
					self.efficiency + (1.0 / (game.fps * self.efficiency_recovery)), 1.0)
		else:
			self.vessel.vessel_img = self.no_deflector_img
		if self.protection < self.max_protection:
			self.protection = min(self.protection + self.max_protection / game.fps, 
				self.max_protection)
		
	def disable(self):
		self.protection = 0
		self.enabled = False

	def damage(self, value):
		"""Absorb damage, return value not absorbed"""
		if self.enabled:
			self.activated = True
			self.last_activation = game.time
			if value > self.protection:
				undeflected = value - self.protection
				deflected = self.protection
			else:
				undeflected = 0
				deflected = value
			self.protection = max(self.protection - deflected, 0)
			return undeflected + deflected - (
				deflected * self.vessel.energy / self.vessel.max_energy)
		else:
			return value

	def disrupt(self, multiplier):
		"""Disrupt deflector efficiency rate by multiplier"""
		self.efficiency *= multiplier


class Armor(System, sprite.Sprite):
	"""Armor defensive system

	Armor has a durability, heat level and dissipation rate per second. As
	armor takes damage, it's heat level is increased. Every second, it's heat
	level decreases by the dissipation rate. If the heat level of the armor
	reaches it's durability level, then additional damage pierces the armor,
	damaging the ship's hull. The durability of the armor is also reduced by
	the damage amount until eventually the armor is disabled. Armor is passive
	system, thus cannot be disabled until its durability reaches zero. 

	Reactive armor will regenerate lost durability over time, but the rate of
	recovery is typically quite slow.
	"""

	name = 'armor'
	mass_factor = 0.005 # Mass per durability unit per ship length unit

	def __init__(self, vessel, durability, dissipation=0, image=None, regeneration=0):
		sprite.Sprite.__init__(self, vessel.layer)
		if image is not None:
			self.armor_img = image
		else:
			self.armor_img = None
		self.vessel = vessel
		self.durability = float(durability)
		self.dissipation = self.max_dissipate = float(dissipation)
		self.regeneration = float(regeneration)
		self.mass = self.durability * vessel.hull_length * self.mass_factor
		self.heat = 0.0
	
	def draw(self, surface):
		if self.armor_img is not None and self.heat > 0 and self.vessel.explosion is None:
			apparent_size, pos = vector.to_screen(self.vessel.position)
			# Get the image and rect here to ensure the vessel has been updated first
			image = media.image(self.armor_img, apparent_size, self.vessel.heading, colorkey=True)
			rect = image.get_rect(center=self.vessel.rect.center)
			if self.durability:
				level = min(self.heat / self.durability, 1.0)
			else:
				level = 1.0
			period = math.pi * game.time / 1000
			opacity = 1 - math.cos(period)**2 / (math.sin(period)**2 + 1)
			opacity *= (level * 40) + (level * 175)
			image.set_alpha(opacity)
			surface.blit(image, rect)
			return rect
		else:
			return _empty_rect

	def update_system(self):
		"""Update heat level

		>>> game.init()
		>>> a = Armor(Vessel(), 1, 1)
		>>> a.heat
		0.0
		>>> a.update_system()
		>>> a.heat
		0.0
		>>> a.heat = 2
		>>> a.update_system()
		>>> new = a.heat
		>>> 0 < a.heat < 2
		True
		>>> a.update_system()
		>>> 0 < a.heat < new
		True
		>>> a.heat = 0.0
		>>> a.update_system()
		>>> a.heat
		0.0
		"""
		if self.heat > 0:
			self.heat = max(self.heat - self.dissipation / game.fps, 0.0)
		if self.regeneration and self.dissipation < self.max_dissipate and self.enabled:
			self.dissipation = min(
				self.dissipation + self.regeneration, self.max_dissipate)
	
	def damage(self, value):
		"""Apply damage to armor, return damage not absorbed
		
		>>> game.init()
		>>> a = Armor(Vessel(), 1, 1)
		>>> a.heat, a.durability
		(0.0, 1.0)
		>>> a.damage(1.0)
		0
		>>> a.heat
		1.0
		>>> a.damage(1.0)
		1.0
		>>> a.heat
		2.0
		>>> a.damage(2.0)
		2.0
		>>> a.heat
		4.0
		"""
		self.heat += value
		if self.heat > self.durability:
			damage = min(self.heat - self.durability, value)
			self.durability = max(self.durability - damage, 0)
			return damage
		else:
			return 0
	
	@property
	def health(self):
		if self.durability and self.heat <= self.durability:
			return max(self.durability - self.heat, 0) / self.durability
		else:
			return 0


class DirectionalThrusters(System):
	"""Simple directional thrust system"""

	name = 'directional thrusters'
	attr_name = 'directional_thrusters'
	mass = 0.5
	
	def __init__(self, vessel, thrust, max_turn_rate):
		"""thrust is in radians per second per second for a one ton ship
		max_turn_rate is in radians per second
		"""
		self.vessel = vessel
		self.thrust = self.full_thrust = float(thrust) * 1000
		self.max_turn_rate = self.full_turn_rate = float(max_turn_rate)
		self.enabled = True
	
	def disable(self):
		self.thrust /= 2
		self.max_turn_rate /= 2
		self.enabled = False
	
	def enable(self):
		self.thrust = self.full_thrust
		self.max_turn_rate = self.full_turn_rate
		self.enabled = True
	
	def update_system(self):
		"""Update vessel turning based on control input
		
		>>> game.init()
		>>> v = Vessel()
		>>> dt = DirectionalThrusters(v, 10, 2)
		>>> v.add_system(dt)
		>>> v.control.turn = 1
		>>> dt.update_system()
		>>> v.torque == dt.thrust
		True
		>>> v.control.turn = -1
		>>> dt.update_system()
		>>> v.torque == 0
		True
		>>> dt.update_system()
		>>> v.torque == -dt.thrust
		True
		"""
		turn_rate = self.vessel.turn_rate
		if self.vessel.control.turn and abs(turn_rate) < self.max_turn_rate:
			self.vessel.body.addTorque((0, 0, self.vessel.control.turn * self.thrust))
		else:
			# slow or stop turning with no input
			if abs(turn_rate) < 0.15:
				self.vessel.turn_rate = 0
			elif turn_rate > 0:
				self.vessel.body.addTorque((0,0,-self.thrust))
			elif turn_rate < 0:
				self.vessel.body.addTorque((0,0,self.thrust))


class ManeuveringThrusters(System):
	
	name = 'maneuvering thrusters'
	attr_name = 'maneuvering_thrusters'
	mass_factor = 0.07 # mass per unit thrust

	def __init__(self, vessel, thrust):
		self.vessel = vessel
		self.thrust = float(thrust) * 100
		self.mass = float(thrust) * self.mass_factor
		self.enabled = True
	
	def disable(self):
		self.enabled = False
	
	def enable(self):
		self.enabled = True

	def update_system(self):
		"""Translate control state into thrusting actions

		>>> game.init()
		>>> v = Vessel()
		>>> v.heading = 0
		>>> mt = ManeuveringThrusters(v, 1)
		>>> v.control.fw_maneuver = True
		>>> mt.update_system()
		>>> v.force.length > 0
		True
		>>> v.force.radians == v.heading
		True

		>>> v = Vessel()
		>>> v.heading = 0
		>>> mt = ManeuveringThrusters(v, 1)
		>>> v.control.fw_maneuver = False
		>>> v.control.right_maneuver = True
		>>> mt.update_system()
		>>> v.force.length > 0
		True
		>>> v.force.radians == v.heading + rightangle
		True

		>>> v = Vessel()
		>>> v.heading = 0
		>>> mt = ManeuveringThrusters(v, 1)
		>>> v.control.right_maneuver = False
		>>> v.control.left_maneuver = True
		>>> mt.update_system()
		>>> v.force.length > 0
		True
		>>> v.force.radians == v.heading + halfcircle + rightangle
		True

		>>> v = Vessel()
		>>> v.heading = 0
		>>> mt = ManeuveringThrusters(v, 1)
		>>> v.control.left_maneuver = False
		>>> v.control.bw_maneuver = True
		>>> mt.update_system()
		>>> v.force.length > 0
		True
		>>> v.force.radians == v.heading + halfcircle
		True
		"""
		if self.enabled:
			if self.vessel.control.fw_maneuver:
				self.vessel.push(vector.unit(self.vessel.heading) * self.thrust)
			if self.vessel.control.bw_maneuver:
				self.vessel.push(vector.unit(self.vessel.heading + halfcircle) * self.thrust)
			if self.vessel.control.left_maneuver:
				self.vessel.push(vector.unit(self.vessel.heading - rightangle) * self.thrust)
			if self.vessel.control.right_maneuver:
				self.vessel.push(vector.unit(self.vessel.heading + rightangle) * self.thrust)


class Engine(System):
	"""Basic thrust engine and generator"""

	name = 'engines'
	attr_name = 'engine'
	base_mass = 1
	mass_factor = 0.03 # mass per unit thrust
	energy_factor = 0.8 # energy generation per unit thrust
	speed_boost = 1.6

	def __init__(self, vessel, thrust):
		self.vessel = vessel
		self.thrust = self.full_thrust = float(thrust) * 100
		self.generation = self.full_generation = float(thrust) * self.energy_factor
		self.mass = self.base_mass + float(thrust) * self.mass_factor
		self.max_speed = self.vessel.max_speed
		self.enabled = True
	
	def disable(self):
		self.thrust /= 2
		self.generation /= 2
		self.enabled = False
	
	def enable(self):
		self.thrust = self.full_thrust
		self.generation = self.full_generation
		self.enabled = True

	def update_system(self):
		"""Translate control state into thrust actions

		>>> game.init()
		>>> v = Vessel()
		>>> v.heading = 0
		>>> e = Engine(v, 10)
		
		>>> v.control.thrust = False
		>>> e.update_system()
		>>> v.force.length
		0.0

		>>> v.control.thrust = True
		>>> v.max_speed = 10
		>>> v.heading = 0
		>>> e.update_system()
		>>> v.force.length > 0
		True
		>>> v.force.radians == v.heading
		True
		"""
		boost = False
		if self.vessel.control.thrust:
			if self.vessel.control.fw_maneuver:
				# Thrust + forward maneuver == boost
				boost = True
				self.vessel.max_speed = self.max_speed * self.speed_boost
			elif self.vessel.max_speed > self.max_speed:
				# overspeed, bleed off a little
				# this avoids a sudden decceleration when we let off boost
				self.vessel.max_speed = max(self.vessel.max_speed * 0.98, self.max_speed)
			self.vessel.push(vector.unit(self.vessel.heading) * self.thrust)
		elif self.vessel.max_speed > self.max_speed:
			# overspeed, bleed off a little
			# this avoids a sudden decceleration when we let off boost
			self.vessel.max_speed = max(self.vessel.max_speed * 0.99, self.max_speed)
		if not boost:
			self.vessel.energy = min(
				self.vessel.energy + self.generation / game.fps, self.vessel.max_energy)


class EnergyCoupler(System):
	"""Enables energy transfer to vessels docked to a mothership"""

	name = 'energy coupler'

	def __init__(self, vessel, energy_flow):
		self.vessel = vessel
		self.energy_flow = float(energy_flow)
	
	def update_system(self):
		if self.vessel.control.objective:
			mothership = self.vessel.control.objective.sprite
			if (self.vessel.energy < self.vessel.max_energy 
				and hasattr(mothership, 'fighter_bay')
				and self.vessel in mothership.fighter_bay):
				energy = min(
					self.energy_flow / game.fps, self.vessel.max_energy - self.vessel.energy)
				self.vessel.energy += mothership.use_energy(energy, partial=True)


class PlanetaryLander(Weapon):
	"""Land on a planet and make a base"""

	name = 'landing system'
	priority = 1
	mass = 100
	landing_time = 3000 # Milliseconds it takes to descend

	def __init__(self, vessel):
		self.vessel = vessel
		self.planet = None
		self.landing_end = None
	
	def update_system(self):
		if (self.enabled and self.firing and self.targeted and self.landing_end is None):
			# Start landing cycle
			self.planet = self.vessel.control.target.sprite
			if self.planet.resources:
				self.landing_end = game.time + self.landing_time
				media.play_sound('land.wav', position=self.vessel.position, min_spacing=2.0)
				# restrict movement
				self.vessel.max_speed = 0
				self.vessel.engine.max_speed = 0
			else:
				message.send_status(self.vessel, game.local_player,
					'Cannot land on %s, planet is inhospitable' % self.planet.name)
				self.vessel.control.target.empty()
		elif self.landing_end is not None:
			# We are landing
			if game.time < self.landing_end:
				self.vessel.set_scale(float(self.landing_end - game.time) / self.landing_time)
			else:
				import base # Avoid circular imports
				# Landing cycle complete, build a base

				self.planet.base = base.PlanetaryBase(self.planet, game.local_player,
					level=1 + (self.planet.resources >= 5))
				message.send_status(self.planet, game.local_player, 
					'Base esablished on %s' % self.planet.name)
				self.vessel.kill()
	
	@property
	def targeted(self):
		import staticbody # Avoid circular import
		target = self.vessel.control.target.sprite
		return (isinstance(target, staticbody.Planet) and
			(target.base is None or target.base.owner.category & self.vessel.category == 0) and
			vector.distance(target.position, self.vessel.position) < target.collision_radius / 2)


class EscapePod(body.RoundBody):

	max_speed = 25
	layer = sprite.layers.vessels
	collision_radius = 0
	
	def __init__(self, launch_vessel):
		body.RoundBody.__init__(self, 
			position=launch_vessel.position, 
			velocity=(launch_vessel.velocity 
				- vector.normal(-launch_vessel.velocity) * self.max_speed),
			heading=launch_vessel.heading + vector.halfcircle,
			image_name='astronaut.png')
		self.turn_rate = random.gauss(0, 0.4)
		media.play_sound('launch.wav')
		self.category = launch_vessel.category

	def update(self):
		if vector.length(self.velocity) > self.max_speed:
			# If we are overspeed, bleed off a little
			self.set_velocity(self.velocity * 0.98)
		body.RoundBody.update(self)


def resolve(dotted_name):
	"""Return the object corresponding to the dotted module/object name
	pair specified.
	
	>>> game.init()
	>>> import beam
	>>> resolve('beam.BeamWeapon') is beam.BeamWeapon
	"""
	splitname = dotted_name.split('.')
	if len(splitname) != 2:
		raise VesselConfigError(
			'Could not resolve "%s": not in format "module_name.object_name"'
			% dotted_name)
	modname, objname = splitname
	if modname not in sys.modules:
		raise VesselConfigError(
			'Could not resolve "%s": could not find module "%s", '
			'make sure the module exists and is imported in vessel.py'
			% (dotted_name, modname))
	module = sys.modules[modname]
	if not hasattr(module, objname):
		raise VesselConfigError(
			'Could not resolve "%s": no object named "%s" '
			'found in module "%s"' % (dotted_name, objname, modname))
	return getattr(module, objname)


# Imports for use by vessel config and resolve()
import projectile
import beam
import bay
import station

if __name__ == '__main__':
	"""Run tests if executed directly"""
	import sys, doctest
	failed, count = doctest.testmod()
	print 'Ran', count, 'test cases with', failed, 'failures'
	sys.exit(failed)
