## Eos, Dawn of Light -- A Space Opera
## Copyright (c) 2007 Casey Duncan and contributors
## See LICENSE.txt for licensing details

# User interface widgets
# $Id$

import pygame
import game
import media
import sprite


class Button(sprite.Sprite):
	"""Basic button widget base class"""

	def __init__(self, rect, action=None, *action_args):
		sprite.Sprite.__init__(self)
		self.rect = rect
		self.action = action
		self.action_args = action_args
		self.highlighted = False
		self.pressed = False
		self.render()
	
	def render(self):
		"""Render the composed button image"""
		raise NotImplementedError
	
	def collides_with(self, pos):
		"""Return true if pos is inside the button"""
		return self.rect.collidepoint(*pos)
	
	## Event Handlers ##

	def mouse_enter(self, ui, event):
		if not event.buttons[0]:
			self.highlighted = True
			self.render()
		elif ui.clicked_control is self:
			self.highlighted = True
			self.pressed = True
			self.render()
	
	def mouse_leave(self, ui, event):
		self.highlighted = False
		self.pressed = False
		self.render()
	
	def mouse_down(self, ui, event):
		self.highlighted = True
		self.pressed = True
		self.render()
	
	def mouse_up(self, ui, event):
		self.pressed = False
		self.render()
		if self.action is not None:
			self.action(*self.action_args)


class ImageButton(Button):
	"""Button displayed using static images"""

	def __init__(self, rect, off_image, highlight_image=None, pressed_image=None,
		action=None, action_args=()):
		self.image = self.off_image = off_image
		self.highlight_image = highlight_image or off_image
		self.pressed_image = pressed_image or highlight_image or off_image
		Button.__init__(self, rect, action, *action_args)
	
	def render():
		if self.pressed:
			self.image = self.pressed_image
		elif self.highlighted:
			self.image = self.highlight_image
		else:
			self.image = self.off_image


class IconButton(Button):
	"""Button labeled with an icon"""

	icon_padding = 0.15
	icon_img = None

	def __init__(self, icon, rect, off_color=(40, 40, 40), 
		on_color=(255, 255, 255, 100), highlight_color=(150, 150, 150),
		text='', render_callback=None, action=None, action_args=()):
		self.padding = max(int((sum(rect.size) / 2) * (self.icon_padding / 2)), 1)
		self.icon_rect = rect.inflate((-self.padding * 2, -self.padding * 2))
		self.text = text
		self.on_color = on_color
		self.off_color = off_color
		self.highlight_color = highlight_color
		self.render_callback = render_callback
		Button.__init__(self, rect, action, *action_args)
		if icon is not None:
			self.set_icon(icon)
	
	def set_icon(self, icon):
		icon = media.fit_image(icon, self.icon_rect)
		if icon is not self.icon_img:
			self.icon_img = icon
			self.render()
	
	def render(self):
		self.image = pygame.Surface(self.rect.size, pygame.SRCALPHA, 32)
		#self.image.fill(self.off_color)
		rect = [(0,0), self.rect.size]
		if self.icon_img is not None:
			self.image.blit(self.icon_img, self.icon_img.get_rect(
				center=self.image.get_rect().center))
			if self.highlighted and not self.pressed:
				pygame.draw.rect(self.image, self.highlight_color, 
					rect, max(self.padding * 3 / 4, 1))
			if self.pressed:
				shader = pygame.Surface(self.rect.size, pygame.SRCALPHA, 32)
				shader.fill(self.on_color)
				self.image.blit(shader, rect)
			if self.render_callback is not None:
				self.render_callback()
		
if __name__ == '__main__':
	# widget tests
	import event
	pygame.init()
	screen = pygame.display.set_mode((1024, 768))
	button = IconButton('outpost.png', pygame.Rect(200, 200, 47, 47))
	event_handler = event.MainHandler()
	controls = pygame.sprite.OrderedUpdates([button])
	while event_handler.handle_events():
		screen.fill((0, 0, 0))
		controls.draw(screen)
		pygame.display.flip()

	

