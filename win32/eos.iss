; Inno setup script
; $Id$

[Setup]
AppName=Eos -- Dawn of Light
AppVerName=Eos -- Dawn of Light version - 0.2
DefaultDirName={pf}\EOS Dawn of Light
DefaultGroupName=EOS Dawn of Light
SetupIconFile=eos.ico

[Dirs]
Name: "{app}\content\maps"
Name: "{app}\content\naree"
Name: "{app}\content\rone"
Name: "{app}\data\proton-grenade"
Name: "{app}\data\splode"
Name: "{app}\fonts\forgottenfuturist"
Name: "{app}\lib"
Name: "{app}\music\human"
Name: "{app}\music\naree"
Name: "{app}\music\rone"
Name: "{app}\sounds"
Name: "{app}\vessels\naree"
Name: "{app}\vessels\rone"

[Files]
Source: "eos.exe"; DestDir: "{app}"
Source: "python25.dll"; DestDir: "{app}"
Source: "msvcr71.dll"; DestDir: "{app}"
Source: "content\maps\*" ; DestDir:"{app}\content\maps"
Source: "content\naree\*" ; DestDir:"{app}\content\naree"
Source: "content\rone\*" ; DestDir:"{app}\content\rone"
Source: "data\proton-grenade\*" ; DestDir:"{app}\data\proton-grenade"
Source: "data\splode\*" ; DestDir:"{app}\data\splode"
Source: "data\*" ; DestDir:"{app}\data"
Source: "fonts\forgottenfuturist\*" ; DestDir:"{app}\fonts\forgottenfuturist"
Source: "lib\shared.zip" ; DestDir:"{app}\lib"
Source: "music\human\*" ; DestDir:"{app}\music\human"
Source: "music\naree\*" ; DestDir:"{app}\music\naree"
Source: "music\rone\*" ; DestDir:"{app}\music\rone"
Source: "sounds\*" ; DestDir:"{app}\sounds"
Source: "vessels\naree\*" ; DestDir:"{app}\vessels\naree"
Source: "vessels\rone\*" ; DestDir:"{app}\vessels\rone"

[Icons]
Name: "{group}\Play EOS"; Filename: "{app}\eos.exe"; WorkingDir: "{app}"
Name: "{group}\Uninstall EOS"; Filename: "{uninstallexe}"; WorkingDir: "{app}"

