# $Id$
#
# This is an example setup.py file
# run it from the windows command line like so:
# > C:\Python24\python.exe -OO setup.py py2exe --bundle 2
# or see the makeExe.bat file in this directory
 
from distutils.core import setup
 
import py2exe, glob
 
opts = { 
 "py2exe": { 
   # if you import .py files from subfolders of your project, then those are
   # submodules.  You'll want to declare those in the "includes"
   #'includes':['data',
   #            'data.glarf',
   #            'data.glarf.chardef',
   #           ],
    
    #'excludes':['OpenGL'],
    'optimize':2,
 } 
} 
 
setup(
 
  #this is the file that is run when you start the game from the command line.  
  windows=[{'script':'eos.py',
            'icon_resources':[(1, 'eos.ico')],
            'dest_base':'eos',
           }],
  
 
  #options as defined above
  options=opts,
 
  #data files - these are the non-python files, like images and sounds
  #the glob module comes in handy here.
  data_files = [
    ("content\\maps", glob.glob("content\\maps\\*")),
    ("content\\naree", glob.glob("content\\naree\\*.txt")),
    ("content\\rone", glob.glob("content\\rone\\*.txt")),    
    ("data", glob.glob("data\\*.png") + glob.glob("data\\*.jpg") + glob.glob("data\\*.txt") + glob.glob("data\\*.gif")),
    ("data\\proton-grenade", glob.glob("data\\proton-grenade\\*.png")),
    ("data\\splode", glob.glob("data\\splode\\*.jpg")),
    ("fonts\\forgottenfuturist", glob.glob("fonts\\forgottenfuturist\\*.ttf") +
     glob.glob("fonts\\forgottenfuturist\\*.html")),
    ("music\\human",glob.glob("music\\human\\*")),
    ("music\\naree",glob.glob("music\\naree\\*")),
    ("music\\rone",glob.glob("music\\rone\\*")),
    ("sounds",glob.glob("sounds\\*.wav")),
    ("vessels\\naree", glob.glob("\\vessels\\naree\\*")),
    ("vessels\\rone", glob.glob("\\vessels\\rone\\*"))
  ],
 
  #this will pack up a zipfile instead of having a glut of files sitting
  #in a folder.
  # this is if you specified bundle 2 on the command line
  zipfile="lib/shared.zip",

  # this is if you specified bundle 1 on the command line
  #zipfile=None,

  
  name="Eos -- Dawn of light",
  version="0.2"
)
